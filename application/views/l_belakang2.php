<!doctype html> 
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Belakang SPD</title>
      <!-- bootstrap -->
      <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	
	<style type="text/css">
/*	html, body{
		width: 150mm;
		height: 297mm;
		position: relative;
	}
	body{
		padding: 0 25.4mm;
		border: 0.5px solid black;
		font: 11pt Segoe UI;
	}
*/	#header{
		font: 10pt Segoe UI;
		margin-left: 0px;
		height: 10mm;
	}
	#header h1{
		font: 14pt Segoe UI;
		margin-bottom: 0;
	}
	#header hr{
		margin: 2pt auto;
		width: 90%;
		border: 1px double black;
	}
	table{
		margin-left: -10px;
		margin-right: -20px;
		width: 100%;
	}
	#konten{
		margin-left: 0px;
		width: 100%;
		text-align: justify;
	}
	#ttd{
		float: right;
		text-align: center;
		margin-right: 150px;
	}
	tr td{
		margin-left: 0px;
		padding: 5 5 5 5;
		text-align: justify;
		vertical-align: top;
	}
	p.ganti{
		page-break-after: always;
	}
	tr td:first-child{
		margin-left: 0px;
	}
	tr td:last-child{
		margin-left: 0px;
		width: 50%;
	}
	footer{
		clear: both;
		margin-top: 30mm;
		text-align: justify;
		font-size: 10px;
/*		position: absolute;
		height: 25.4mm;
		bottom: 5.4mm;
*/		float: left; 
		margin-left: 10mm;
	}
	
	</style>

   </head>
   <body> 
	<div id="header">
		<div class="col-xs-12" style="font-family: Sans-serif;text-align:left;">
			<p style="margin-left:-25px;">Lampiran SPD Tgl .................................. 20 ............................No. : ......................./ ..................../KPU/20.........</p>
		</div>
	</div>
	<?php $detailbelakang = explode('##', $belakang); ?>
	<table border="0" style="pading:5 5 5 5;width:100%;margin-top:-20px">
		<tr>
            <td width="50%">
            </td>
            <td width="50%">
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"></div>
            	</div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[0];?></div>
		        </div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[1];?></div>
		        </div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[2];?></div>
		        </div>
		        <br><br>
		        <?php echo $detailbelakang[3];?>
            </td>
        </tr>
		<tr>
            <td width="50%">
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[5];?></div>
            	</div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[6];?></div>
		        </div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[7];?></div>
		        </div>
		        <br><br>
		        <?php echo $detailbelakang[8];?>
            </td>
            <td width="50%">
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[10];?></div>
            	</div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[11];?></div>
		        </div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[12];?></div>
		        </div>
		        <br><br>
		        <?php echo $detailbelakang[14];?>
            </td>
        </tr>
		<tr>
            <td width="50%">
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[16];?></div>
            	</div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[17];?></div>
		        </div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[18];?></div>
		        </div>
		        <br><br>
		        <?php echo $detailbelakang[19];?>
            </td>
            <td width="50%">
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[21];?></div>
            	</div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[22];?></div>
		        </div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[23];?></div>
		        </div>
		        <br><br>
		        <?php echo $detailbelakang[25];?>
            </td>
        </tr>
		<tr>
            <td width="50%">
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[27];?></div>
            	</div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[28];?></div>
		        </div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[29];?></div>
		        </div>
		        <br><br>
		        <?php echo $detailbelakang[30];?>
            </td>
            <td width="50%">
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[32];?></div>
            	</div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[33];?></div>
		        </div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[34];?></div>
		        </div>
		        <br><br>
		        <?php echo $detailbelakang[36];?>
            </td>
        </tr>
		<tr>
            <td width="50%">
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[38];?></div>
            	</div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[39];?></div>
		        </div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"><?php echo $detailbelakang[40];?></div>
		        </div>
		        <br><br>
		        <?php echo $detailbelakang[41];?>
            </td>
            <td width="50%">
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;">&nbsp;</div>
            	</div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;">&nbsp;</div>
		        </div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;">&nbsp;</div>
		        </div>
		        <br><br>
            </td>
        </tr>
		<tr>
            <td width="50%">
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;">&nbsp;</div>
            	</div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;">&nbsp;</div>
		        </div>
            	<div class="row">
		            	<div class="col-xs-4" style="text-align:left;">
		            	&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</div>
		            	<div class="col-xs-1"></div>
		            	<div class="col-xs-6" style="text-align:left;"></div>
		        </div>
		        <br><br>
            </td>
            <td width="50%">
            	<div class="row">
		            	<div class="col-xs-12" style="text-align:justify;">
		            	&nbsp;</div>
            	</div>
		        <br><br><br><br><br><br><br>
		        <?php echo $detailbelakang[43];?>
            </td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
        	<td></td>
        </tr>
	</table>
   </body>
</html>