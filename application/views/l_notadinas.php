<!doctype html> 
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Nota Dinas</title>
      <!-- bootstrap -->
      <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	
	<style type="text/css">
/*	html, body{
		width: 150mm;
		height: 297mm;
		position: relative;
	}
	body{
		padding: 0 25.4mm;
		border: 0.5px solid black;
		font: 11pt Segoe UI;
	}
*/	#header{
		font: 10pt Segoe UI;
		height: 20mm;
	}
	#header h1{
		font: 14pt Segoe UI;
		margin-bottom: 0;
	}
	#header hr{
		margin: 2pt auto;
		width: 90%;
		border: 1px double black;
	}
	#header .left img{
		height: 32.8mm;
		width: 26.2mm;
		margin-top: 14pt;
	}
	#header .right img{
		height: 21.2mm;
		width: 24.1mm;
		margin-top: 24pt;
	}
	#konten{
		margin-left: 33mm;
		text-align: justify;
	}
	#ttd{
		float: right;
		text-align: center;
		margin-right: 150px;
	}
	tr td{
		text-align: justify;
		vertical-align: top;
	}
	p.ganti{
		page-break-after: always;
	}
	tr td:first-child{
		width: 20mm;
	}
	tr td:last-child{
		width: 10mm;
	}
	footer{
		clear: both;
		margin-top: 30mm;
		text-align: justify;
		font-size: 10px;
/*		position: absolute;
		height: 25.4mm;
		bottom: 5.4mm;
*/		float: left; 
		margin-left: 10mm;
	}
	
	</style>

   </head>
   <body> 
	<div id="header">
		<div class="col-md-12 right" style="margin-top:-20px; font-family: Tahoma, Geneva, sans-serif;font-size: 17;" align="center">
			KOMISI PEMILIHAN UMUM REPUBLIK INDONESIA</br><p style="margin-top:10px;font-family: Tahoma, Geneva, sans-serif;font-size: 14;font-weight: normal;">Jalan Imam Bonjol No. 29 Jakarta Pusat</p>
		</div>
	</div>
	<div class="row" style="margin-top: -20px">
		<div class="col-md-6" style="text-align: left;margin-left: 25px;font-family: Tahoma, Geneva, sans-serif;font-size: 11">Telp. 021 3193 7223</div>
		<div class="col-md-6" style="text-align: right;margin-right: 20px; margin-top:-40px;font-family: Tahoma, Geneva, sans-serif;font-size: 11">Telp. 021 3193 7223</div>
	<hr style="margin: 0pt auto;
	width: 90%;
	border: 2px double black;"/>
	</div>

	<div id="judul" class="row" style="font-family: Arial, Helvetica, sans-serif;font-size: 11">
		<div class="col-md-12" align="center">
			<br>
			<div style="text-align:center;text-decoration: underline;"><b>NOTA DINAS</b></div>
			<br>
			<table width="80%" style="margin-left: 20mm">
				<tr><td style="width:40mm;font-size:11;">Kepada </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm">Yth. Ibu Kepala Biro Umum.</td></tr>
				<tr><td style="width:40mm;font-size:11;">Dari </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm">Kepala Bagian Keamanan.</td></tr>
				<tr><td style="width:40mm;font-size:11;">Tembusan</td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm"> - </td></tr>
				<tr><td style="width:40mm;font-size:11;">Nomor </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm"> <?php echo $no; ?> </td></tr>
				<tr><td style="width:40mm;font-size:11;">Tanggal </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm"> <?php echo $dari; ?> </td></tr>
				<tr><td style="width:40mm;font-size:11;">Sifat </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm"> - </td></tr>
				<tr><td style="width:40mm;font-size:11;">Perihal </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm"> <?php echo $perihal; ?> </td></tr>
			</table>
		</div>
			<hr style="height:1px;border:none;color:#333;background-color:#333;width:90%;" />
			<div>
				<div class="col-xs-12" style="text-align: justify;width:130mm;margin-left: 48mm"><?php echo $isian; ?><br><br>Sehubungan dengan hal tersebut, bersama ini disampaikan Net Konsep Surat Tugas dimaksud. <br><br>Demikian dilaporkan untuk periksa dan mohon petunjuk.</div>
			</div>
		</div>
	</div>
	<br><br><br>
	<div id="ttd" class="row">
		<div class="col-md-12" style="margin-left:40mm;">
			<?php echo $jabpenye; ?>,
			<p><br><br><br></p>
			<?php echo $penye; ?>
		</div>
	</div>
   </body>
</html>