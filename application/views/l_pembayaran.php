<!doctype html> 
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Pembayaran</title>
      <!-- bootstrap -->
      <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	
	<style type="text/css">
/*	html, body{
		width: 150mm;
		height: 297mm;
		position: relative;
	}
	body{
		padding: 0 25.4mm;
		border: 0.5px solid black;
		font: 11pt Segoe UI;
	}
*/	#header{
		font: 10pt Segoe UI;
		height: 20mm;
	}
	#header h1{
		font: 14pt Segoe UI;
		margin-bottom: 0;
	}
	#header hr{
		margin: 2pt auto;
		width: 90%;
		border: 1px double black;
	}
	#header .left img{
		height: 32.8mm;
		width: 26.2mm;
		margin-top: 14pt;
	}
	#header .right img{
		height: 21.2mm;
		width: 24.1mm;
		margin-top: 24pt;
	}
	#konten{
		margin-left: 33mm;
		text-align: justify;
	}
	#ttd{
		float: right;
		text-align: center;
		margin-right: 150px;
	}
	tr td{
		text-align: justify;
		vertical-align: top;
	}
	p.ganti{
		page-break-after: always;
	}
	tr td:first-child{
		width: 20mm;
	}
	tr td:last-child{
		width: 10mm;
	}
	footer{
		clear: both;
		margin-top: 30mm;
		text-align: justify;
		font-size: 10px;
/*		position: absolute;
		height: 25.4mm;
		bottom: 5.4mm;
*/		float: left; 
		margin-left: 10mm;
	}
	
	</style>

   </head>
   <body> 
   	<table>
   		<tr><td style="width:70mm;text-align:center"><b>KOMISI PEMILIHAN UMUM</b></td><td style="width:50mm;"></td><td style="width:80mm">Lembar : .................</td></tr>
   		<tr><td style="width:70mm;text-align:center">Jl. Imam Bonjol Bo.29</td></tr>
   		<tr><td style="width:70mm;text-align:center">Jakarta</td></tr>
   		<tr><td colspan=3 style="text-align:center"><b>KWITANSI</b></td></tr>
   	</table>
   	<table>
   		<tr><td style="width:20mm;text-align:left;">Nomor</td><td style="width:10mm;text-align:left;">:</td><td  style="width:150mm;text-align:left;">.................</td></tr>
   		<tr><td style="width:20mm;text-align:left;">m.a</td><td style="width:10mm;text-align:left;">:</td><td  style="width:150mm;text-align:left;">.................</td></tr>
   		<tr><td style="width:20mm;text-align:left;"></td><td style="width:10mm;text-align:left;"></td><td  style="width:150mm;text-align:left;">Sudah terima dari Komisi Pemilihan Umum di Jakarta</td></tr>
   		<tr><td style="width:20mm;text-align:left;"></td><td style="width:10mm;text-align:left;"></td><td  style="width:150mm;text-align:left;">Uang sebesar :  Rp. <?php echo $totalall; ?>,- ( <?php echo $ttotalall; ?> rupiah )</td></tr>
   		<tr><td style="width:20mm;text-align:left;"></td><td style="width:10mm;text-align:left;"></td><td  style="width:150mm;text-align:left;">
   			<table>
   				<tr><td rowspan=2 style="vertical-align:middle;width:40mm;">Guna Pembayaran</td><td style="border-bottom: 1px solid black;margin-left:10mm;width:40mm;">Biaya perjalanan dinas (lumpsum)</td><td rowspan=2 style="vertical-align:middle;margin-left:150px;">menurut :</td></tr>
   				<tr><td style="margin-left:10mm;width:40mm;">Kekurangan biaya perjalan dinas</td></tr>
   			</table>
   		</td></tr>
   		<tr><td style="width:20mm;text-align:left;"></td><td style="width:10mm;text-align:left;"></td><td  style="width:150mm;text-align:left;">Surat Perintah Perjalanan Dinas dari KEPALA BIRO UMUM KOMISI PEMILIHAN UMUM tanggal <?php echo $dari; ?> Nomor <?php echo $no; ?> untuk perjalanan dinas PP dari Jakarta ke <?php echo $daerah; ?></td></tr>
   		<tr><td style="width:20mm;text-align:left;">Terbilang</td><td style="width:10mm;text-align:left;">:</td><td  style="width:150mm;text-align:left;">Rp. <?php echo $totalall; ?>,- </td></tr>
   	</table><br><br>
   	<div class="row">
   		<div class="col-md-6" style="text-align:center;float:left">
   			<?php echo $jabpenyetuju; ?><br><br><br>(<?php echo $penyetuju; ?>)<br>NIP. <?php echo $nippenyetuju; ?>
   		</div>
   		<div class="col-md-6" style="text-align:center;float:right">
   			<br>Yang bepergian,<br><br><br><br>( <?php echo $nama; ?> )
   		</div>
   	</div>
   	<p class="ganti"></p>
	<div id="header">
		<div class="col-md-12 right" style="margin-top:-20px; font-family: Sans-serif;font-size: 20px;font-weight: bold;" align="center">
			KOMISI PEMILIHAN UMUM REPUBLIK INDONESIA</br><p style="font-size: 15px;font-weight: normal;">Jalan Imam Bonjol No. 29</p></br><p style="font-size: 15px;font-weight: normal; margin-top:-10px;">Jakarta</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6" style="margin-top:-70px;font-size: 10px;">
			Telp. 021 3193 7223
		</div>
		<div class="col-md-6" style="text-align: right; margin-top:-40px;font-size: 10px;">
			Fax. 021 3157 759
		</div>
		<hr style="margin: 0pt auto;
		width: 100%;
		border: 2px double black;"/>
	</div>

	<div id="judul" class="row">
		<div class="col-md-12" align="center" style="text-decoration: underline;">
			RINCIAN PERTANGGUNG JAWABAN<br>BIAYA PERJALANAN DINAS
		</div>
		<div class="col-md-12" style="margin-left: 10mm;">
			<table>
				<tr><td style="width:40mm;font-size:12px;">Nomor Surat Tugas </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm"><?php echo $no; ?></td></tr>
				<tr><td style="width:40mm;font-size:12px;">Tanggal Surat Tugas </td><td style="width: 10mm;">:</td><td ><?php echo $dari; ?><p style="float:right">Tanggal PM/SP2D</p></td></tr>
				<tr><td style="width:40mm;font-size:12px;">Penanggung Jawab Kegiatan </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm"></td></tr>
				<tr style="text-align: center;"><td colspan=3 style="text-align: center;">
					<table class="table table-bordered">
						<tr><td style="width:5mm;">No</td><td colspan=2 style="width:100mm;">URAIAN BIAYA</td><td>JUMLAH</td><td>KETERANGAN</td></tr>
						<tr><td style="width:5mm;">1</td><td colspan=2 style="width:100mm;">Harian (Uang makan, uang saku, transportasi lokal) selama <?php echo $lama; ?> hari x Rp. <?php echo $mharian; ?>,-</td><td style="width:30mm;text-align:right;">Rp. <?php echo $harian; ?>,-</td><td>-</td></tr>
						<tr><td style="width:5mm;">2</td><td colspan=2>Transport Pegawai:<br><ul><li>Tiket Pesawat/ kereta api/ kapal laut/ bus/ transportasi lainnya (airport tax, boarding pass, tiket kendaraan, karcis tol)</li><li>Transport dari tempat kedudukan ke</li><li>Transport dari.... ke.....</li></ul></td><td style="width:30mm;text-align:right;">Rp. <?php echo $tiket; ?>,-</td><td>-</td></tr>
						<tr><td style="width:5mm;">3</td><td colspan=2 style="width:100mm;">Penginapan/ Hotel</td><td style="width:30mm;text-align:right;">Rp. <?php echo $hotel; ?>,-</td><td>-</td></tr>
						<tr><td style="width:5mm;"></td><td colspan=2 style="width:100mm;">Jumlah</td><td style="width:30mm;text-align:right;">Rp. <?php echo $total; ?>,-</td><td>-</td></tr>
						<tr><td style="width:5mm;"></td><td colspan=3>Terbilang : <?php echo $ttotal; ?> rupiah</td><td>-</td></tr>
					</table>
				</td></tr>
			</table>
			<div class="row">
		   		<div class="col-md-6" style="text-align:center;float:left">
		   			Telah dibayarkan sejumlah ( Rp. <?php echo $totalall; ?>,- )<br>Bendahara<br><br><br>(<?php echo $bendahara; ?>)
		   		</div>
		   		<div class="col-md-6" style="text-align:center;float:right;">
		   			Telah menerima jumlah uang ( Rp. <?php echo $totalall; ?>,- )<br>Yang menerima<br><br><br>(<?php echo $nama; ?>)
		   		</div>
			</div>
			<hr style="margin: 0pt auto;
			width: 100%;
			border: 2px double black;"/>
			<div class="col-md-12" style="text-align:center"><b>PERHITUNGAN SPPD RAMPUNG</b></div>
			<table>
				<tr><td style="width:40mm;font-size:12px;">Ditetapkan sejumlah </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm">Rp. <?php echo $totalall; ?>,- ( <?php echo $ttotalall; ?> rupiah )</td></tr>
				<tr><td style="width:40mm;font-size:12px;">Yang di bayarkan semula </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm"> - </td></tr>
				<tr><td style="width:40mm;font-size:12px;">Sisa Kurang/lebih </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm"> - </td></tr>
			</table>
			<p><br></p>
		</div>
	</div>

	<div id="ttd" class="row">
		<div class="col-md-12">
		   			<?php echo $jabpenyetuju; ?><br><br><br>(<?php echo $penyetuju; ?>)<br>NIP. <?php echo $nippenyetuju; ?>
		</div>
	</div>
	<p class="ganti"></p>
	<div id="header">
		<div class="col-md-12 right" style="margin-top:-20px; font-family: Sans-serif;font-size: 20px;font-weight: bold;" align="center">
			KOMISI PEMILIHAN UMUM REPUBLIK INDONESIA</br><p style="font-size: 15px;font-weight: normal;">Jalan Imam Bonjol No. 29</p></br><p style="font-size: 15px;font-weight: normal; margin-top:-10px;">Jakarta</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6" style="margin-top:-70px;font-size: 10px;">
			Telp. 021 3193 7223
		</div>
		<div class="col-md-6" style="text-align: right; margin-top:-40px;font-size: 10px;">
			Fax. 021 3157 759
		</div>
		<hr style="margin: 0pt auto;
		width: 100%;
		border: 2px double black;"/>
	</div>

	<div id="judul" class="row">
		<div class="col-md-12" align="center" style="text-decoration: underline;">
			SURAT PERNYATAAN TANGGUNG JAWAB<br>PENGELUARAN RIII
		</div>
		<div class="col-md-12">
			<table>
				<tr><td style="width:40mm;font-size:12px;">Nama</td><td style="width: 10mm;"> : </td><td style="text-align: left; width:140mm;"><?php echo $nama; ?></td></tr>
				<tr><td style="width:40mm;font-size:12px;">NIP</td><td style="width: 10mm;">:</td><td tyle="text-align: left; width:140mm;"><?php echo $nip; ?></td></tr>
				<tr><td style="width:40mm;font-size:12px;">Jabatan</td><td style="width: 10mm;">:</td><td style="text-align: left; width:140mm"><?php echo $jabatan; ?></td></tr>
				<tr style="text-align: center;"><td colspan=3 style="text-align: justify;">
					Berdasarkan Surat Perintah Perjalanan Dinas (SPPD) tanggal <?php echo $dari; ?> Nomor <?php echo $no; ?> dengan ini menyatakan dengan sesungguhnya bahwa :</td>
				</tr>
				<tr style="text-align: center;"><td colspan=3 style="text-align: justify;">
					1. Biaya Transport pegawai dibawah ini yang tidak dapat diperoleh bukti-bukti pengeluarannya meliputi:</td>
				</tr>
				<tr style="text-align: center;"><td colspan=3 style="text-align: center;">
					<table class="table table-bordered">
						<tr><td style="width:5mm;">No</td><td colspan=2 style="width:100mm;">URAIAN BIAYA</td><td>JUMLAH</td></tr>
						<tr><td style="width:5mm;">1</td><td colspan=2 style="width:100mm;">Biaya Transport dari tempat kedudukan ke Bandara Soekarno Hatta PP berdasarkan peraturan Menteri Keuangan Nomor 49/PMK.02/2017 tentang standart biaya Umum Tahun anggaran 2018</td><td style="width:30mm;text-align:right;">Rp. <?php echo $bandara; ?>,-</td></tr>
						<tr><td style="width:5mm;">2</td><td colspan=2>Biaya Transport dari Bandara ke kota Palangkaraya berdasarkan sda</td><td style="width:30mm;text-align:right;">Rp. <?php echo $local; ?>,-</td></tr>
						<tr><td style="width:5mm;">3</td><td colspan=2 style="width:100mm;">Uang Representasi</td><td style="width:30mm;text-align:right;">Rp. <?php echo $res; ?>,-</td></tr>
						<tr><td style="width:5mm;"></td><td colspan=2 style="width:100mm;">Jumlah</td><td style="width:30mm;text-align:right;">Rp. <?php echo $total1; ?>,-</td></tr>
					</table>
				</td></tr>
				<tr style="text-align: center;"><td colspan=3 style="text-align: justify;">
					Jumlah uang di atas benar-benar di keluarkan untuk pelaksanaan perjalanan dinas dimaksud dan apabila dikemudian hari terdapat kelebihan atas pembayaran kami bersedia untuk menyetorkan kelebihan tersebut.</td>
				</tr>
				<tr style="text-align: center;height:50px;"><td colspan=3 style="text-align: justify;"></td>
				</tr>
				<tr style="text-align: center;margin-top:15px;"><td colspan=3 style="text-align: justify;text-indent: 25px;margin: 50px 15px 70px 15px;">
					Demikian Pernyataan ini kami buat dengan sebenarnya dan penuh tanggung jawab, untuk dapat digunakan sebagaimana mestinya.</td>
				</tr>
			</table><br>
			<div class="row">
		   		<div class="col-md-6" style="text-align:center;float:left">
		   			MENGETAHUI/MENYETUJUI<br><?php echo $jabpenyetuju; ?><br><br><br>(<?php echo $penyetuju; ?>)<br>NIP. <?php echo $nippenyetuju; ?>
		   		</div>
		   		<div class="col-md-6" style="text-align:center;float:right;">
		   			Yang melakukan perjalanan dinas<br><br><br><br><br>(<?php echo $nama; ?>)
		   		</div>
			</div>
		</div>
	</div>
   </body>
</html>