<!doctype html> 
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Surat Tugas</title>
      <!-- bootstrap -->
      <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	
	<style type="text/css">
/*	html, body{
		width: 150mm;
		height: 297mm;
		position: relative;
	}
	body{
		padding: 0 25.4mm;
		border: 0.5px solid black;
		font: 11pt Segoe UI;
	}
*/	#header{
		font: 14pt Tahoma;
		height: 20mm;
	}
	#header h1{
		font: 17pt Tahoma;
		margin-bottom: 0;
	}
	#header hr{
		margin: 2pt auto;
		width: 90%;
		border: 1px double black;
	}
	#header .left img{
		height: 36.8mm;
		width: 26.2mm;
		margin-top: 14pt;
	}
	#header .right img{
		height: 21.2mm;
		width: 24.1mm;
		margin-top: 24pt;
	}
	#konten{
		margin-left: 33mm;
		text-align: justify;
	}
	#ttd{
		float: right;
		text-align: center;
		margin-right: 250px;
	}
	tr td{
		text-align: justify;
		vertical-align: top;
	}
	p.ganti{
		page-break-after: always;
	}
	tr td:first-child{
		width: 20mm;
	}
	tr td:last-child{
		width: 10mm;
	}
	footer{
		clear: both;
		margin-top: 30mm;
		text-align: justify;
		font-size: 10px;
/*		position: absolute;
		height: 25.4mm;
		bottom: 5.4mm;
*/		float: left; 
		margin-left: 10mm;
	}
	
	</style>

   </head>
   <body width=80%> 
	<div id="header">
		<div class="col-md-2 left" style="float: left; margin-left: 3mm; margin-top:-40px">
		<img src="assets/images/logo_perhunungan.png" class='img-responsive' style='width:80px;height:100px;'>
		</div>
		<div class="col-md-10 right" style="margin-top:-20px;font-family: Tahoma, Geneva, sans-serif;font-size: 17;" align="center">
			KOMISI PEMILIHAN UMUM REPUBLIK INDONESIA</br><p style="margin-top:10px;font-family: Tahoma, Geneva, sans-serif;font-size: 14;font-weight: normal;">Jalan Imam Bonjol No. 29</p></br><p style="font-size: 14;font-family: Tahoma, Geneva, sans-serif;font-weight: normal; margin-top:10px;">Jakarta</p>
		</div>
	</div>
	</div>
	<div id="judul" class="row">
		<!-- <div class="col-md-6" style="text-align: left; font-size: 11px; margin-top: -40px;">
			Telp. 021 3193 7223
		</div>
		<div class="col-md-6" style="font-size: 11px; text-align: right; margin-top:-40px;">
			Fax. 021 3157 759
		</div> -->
	</div>
	<div class="row">

		<div class="col-md-6" style="text-align: left;margin-left: 25px;font-family: Tahoma, Geneva, sans-serif;font-size: 11">Telp. 021 3193 7223</div>
		<div class="col-md-6" style="text-align: right;margin-right: 20px; margin-top:-40px;font-family: Tahoma, Geneva, sans-serif;font-size: 11">Telp. 021 3193 7223</div>
	<hr style="margin: 0pt auto;
	width: 90%;
	border: 2px double black;"/>

	<div id="judul" class="row" style="font-family: Arial, Helvetica, sans-serif;font-size: 11">
		<br>
		<div class="col-md-12" align="center">
			SURAT TUGAS</br><p>No : <?php echo $no; ?></p>
		</div>
		<div class="col-md-12" style="margin-left: 10mm;">
			<p><br></p>
			<table width="80%" style="">
				<tr><td style="width:25mm;">Menimbang </td><td style="width: 5mm;">:</td><td style="text-align: justify;width:135mm;"><?php $data = explode('##', $menimbang); ?>
					<ul type="a">
						<?php foreach($data as $key2) {  ?>  
						    <?php if($key2 != ''){?>
						<li></li><div style="margin-left: 20px;"><?php echo $key2; ?></div>
						<?php } } ?>
					</ul>
				</td></tr>
				<tr ><td style="width:25mm;">Dasar </td><td style="width: 5mm;">:</td><td style="width:135mm;"><?php $data1 = explode('##', $dasar); ?>
					<ul type="1">
						<?php foreach($data1 as $key1) {  ?>  
						    <?php if($key1 != ''){?>
						<li></li><div style="margin-left: 20px;"><?php echo $key1; ?></div>
						<?php } } ?>
					</ul>
					<br>
				</td></tr>
				<tr style="text-align: center;"><td colspan=3 style="text-align: center;font-size: 12">Memberi Tugas</td></tr>
				<tr><td style="width:25mm;">Kepada </td><td style="width: 5mm;">:</td><td style="text-align: justify;width:135mm;"> <?php $detailnama = explode(',',$nama); ?>
					<ul type="1" style="margin-left:-20px;">
						<?php foreach($detailnama as $key) {  ?>  
						    <?php if($key != ''){?>
						<li></li><div style="margin-left: 20px;"><?php echo $key; ?></div>
						<?php } } ?>
					</ul>
				</td></tr>
				<tr><td style="width:25mm;">Untuk </td><td style="width: 5mm;">:</td><td style="text-align: justify;width:135mm;"> <?php echo $untuk; ?><br><br>Demikian Surat Tugas ini dibuat untuk dapat dilaksanakan dengan penuh tanggung jawab</td></tr>
			</table>
			<p><br></p>
		</div>
	</div>
	<div id="ttd" class="row">
		<div class="col-md-12">
			Jakarta, <?php echo date('d F Y');?><br>
			Kepala Biro Umum,
			<p><br><br><br></p>
			<?php echo $penyetuju; ?><br>
			NIP. <?php echo $nippenyetuju; ?>
		</div>
	</div>

   </body>
</html>