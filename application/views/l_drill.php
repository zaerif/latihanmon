<!doctype html> 
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Pembayaran</title>
      <!-- bootstrap -->
      <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	
	<style type="text/css">
/*	html, body{
		width: 150mm;
		height: 297mm;
		position: relative;
	}
	body{
		padding: 0 25.4mm;
		border: 0.5px solid black;
		font: 11pt Segoe UI;
	}
*/	#header{
		font: 10pt Segoe UI;
		height: 20mm;
	}
	#header h1{
		font: 14pt Segoe UI;
		margin-bottom: 0;
	}
	#header hr{
		margin: 2pt auto;
		width: 90%;
		border: 1px double black;
	}
	#header .left img{
		height: 32.8mm;
		width: 26.2mm;
		margin-top: 14pt;
	}
	#header .right img{
		height: 21.2mm;
		width: 24.1mm;
		margin-top: 24pt;
	}
	#konten{
		margin-left: 33mm;
		text-align: justify;
	}
	#ttd{
		float: right;
		text-align: center;
		margin-right: 150px;
	}
	tr td{
		text-align: justify;
		vertical-align: top;
	}
	p.ganti{
		page-break-after: always;
	}
	tr td:first-child{
		width: 20mm;
	}
	tr td:last-child{
		width: 10mm;
	}
	footer{
		clear: both;
		margin-top: 30mm;
		text-align: justify;
		font-size: 10px;
/*		position: absolute;
		height: 25.4mm;
		bottom: 5.4mm;
*/		float: left; 
		margin-left: 10mm;
	}
	
	</style>

   </head>
   <body> 
	<div id="header">
		<div class="col-md-12 right" style="margin-top:-20px; font-family: Sans-serif;font-size: 20px;font-weight: bold;" align="center">
			KOMISI PEMILIHAN UMUM REPUBLIK INDONESIA</br><p style="font-size: 15px;font-weight: normal;">Jalan Imam Bonjol No. 29</p></br><p style="font-size: 15px;font-weight: normal; margin-top:-10px;">Jakarta</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6" style="margin-top:-70px;font-size: 10px;">
			Telp. 021 3193 7223
		</div>
		<div class="col-md-6" style="text-align: right; margin-top:-40px;font-size: 10px;">
			Fax. 021 3157 759
		</div>
		<hr style="margin: 0pt auto;
		width: 100%;
		border: 2px double black;"/>
	</div>

	<div id="judul" class="row">
		<div class="col-md-12" align="center" style="text-decoration: underline;">
			SURAT PERNYATAAN TANGGUNG JAWAB<br>PENGELUARAN RIII
		</div>
		<div class="col-md-12">
			<table>
				<tr><td style="width:40mm;font-size:12px;">Nama</td><td style="width: 10mm;"> : </td><td style="text-align: left; width:140mm;"><?php echo $nama; ?></td></tr>
				<tr><td style="width:40mm;font-size:12px;">NIP</td><td style="width: 10mm;">:</td><td tyle="text-align: left; width:140mm;"><?php echo $nip; ?></td></tr>
				<tr><td style="width:40mm;font-size:12px;">Jabatan</td><td style="width: 10mm;">:</td><td style="text-align: left; width:140mm"><?php echo $jabatan; ?></td></tr>
				<tr style="text-align: center;"><td colspan=3 style="text-align: justify;">
					Berdasarkan Surat Perintah Perjalanan Dinas (SPPD) tanggal <?php echo $dari; ?> Nomor <?php echo $no; ?> dengan ini menyatakan dengan sesungguhnya bahwa :</td>
				</tr>
				<tr style="text-align: center;"><td colspan=3 style="text-align: justify;">
					1. Biaya Transport pegawai dibawah ini yang tidak dapat diperoleh bukti-bukti pengeluarannya meliputi:</td>
				</tr>
				<tr style="text-align: center;"><td colspan=3 style="text-align: center;">
					<table class="table table-bordered">
						<tr><td style="width:5mm;">No</td><td colspan=2 style="width:100mm;">URAIAN BIAYA</td><td>JUMLAH</td></tr>
						<tr><td style="width:5mm;">1</td><td colspan=2 style="width:100mm;">Biaya Transport dari tempat kedudukan ke Bandara Soekarno Hatta PP berdasarkan peraturan Menteri Keuangan Nomor 49/PMK.02/2017 tentang standart biaya Umum Tahun anggaran 2018</td><td style="width:30mm;text-align:right;">Rp. <?php echo $bandara; ?>,-</td></tr>
						<tr><td style="width:5mm;">2</td><td colspan=2>Biaya Transport dari Bandara ke kota Palangkaraya berdasarkan sda</td><td style="width:30mm;text-align:right;">Rp. <?php echo $local; ?>,-</td></tr>
						<tr><td style="width:5mm;">3</td><td colspan=2 style="width:100mm;">Uang Representasi</td><td style="width:30mm;text-align:right;">Rp. <?php echo $res; ?>,-</td></tr>
						<tr><td style="width:5mm;"></td><td colspan=2 style="width:100mm;">Jumlah</td><td style="width:30mm;text-align:right;">Rp. <?php echo $total1; ?>,-</td></tr>
					</table>
				</td></tr>
				<tr style="text-align: center;"><td colspan=3 style="text-align: justify;">
					Jumlah uang di atas benar-benar di keluarkan untuk pelaksanaan perjalanan dinas dimaksud dan apabila dikemudian hari terdapat kelebihan atas pembayaran kami bersedia untuk menyetorkan kelebihan tersebut.</td>
				</tr>
				<tr style="text-align: center;height:50px;"><td colspan=3 style="text-align: justify;"></td>
				</tr>
				<tr style="text-align: center;margin-top:15px;"><td colspan=3 style="text-align: justify;text-indent: 25px;margin: 50px 15px 70px 15px;">
					Demikian Pernyataan ini kami buat dengan sebenarnya dan penuh tanggung jawab, untuk dapat digunakan sebagaimana mestinya.</td>
				</tr>
			</table><br>
			<div class="row">
		   		<div class="col-md-6" style="text-align:center;float:left">
		   			MENGETAHUI/MENYETUJUI<br><?php echo $jabpenyetuju; ?><br><br><br>(<?php echo $penyetuju; ?>)<br>NIP. <?php echo $nippenyetuju; ?>
		   		</div>
		   		<div class="col-md-6" style="text-align:center;float:right;">
		   			Yang melakukan perjalanan dinas<br><br><br><br><br>(<?php echo $nama; ?>)
		   		</div>
			</div>
		</div>
	</div>
   </body>
</html>