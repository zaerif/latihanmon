<!doctype html> 
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Surat Tugas</title>
      <!-- bootstrap -->
      <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	
	<style type="text/css">
/*	html, body{
		width: 150mm;
		height: 297mm;
		position: relative;
	}
	body{
		padding: 0 25.4mm;
		border: 0.5px solid black;
		font: 11pt Segoe UI;
	}
*/	#header{
		font: 10pt Segoe UI;
		height: 20mm;
	}
	#header h1{
		font: 14pt Segoe UI;
		margin-bottom: 0;
	}
	#header hr{
		margin: 2pt auto;
		width: 90%;
		border: 1px double black;
	}
	#header .left img{
		height: 36.8mm;
		width: 26.2mm;
		margin-top: 14pt;
	}
	#header .right img{
		height: 21.2mm;
		width: 24.1mm;
		margin-top: 24pt;
	}
	table{
		margin-left: -20px;
		margin-right: -20px;
		width: 100%;
	}
	#konten{
		margin-left: 0px;
		width: 100%;
		text-align: justify;
	}
	#ttd{
		float: right;
		text-align: center;
		margin-right: 150px;
	}
	tr td{
		margin-left: 0px;
		padding: 5 5 5 5;
		text-align: justify;
		vertical-align: top;
	}
	p.ganti{
		page-break-after: always;
	}
	tr td:first-child{
		margin-left: 0px;
	}
	tr td:last-child{
		margin-left: 0px;
		width: 50%;
	}
	footer{
		clear: both;
		margin-top: 30mm;
		text-align: justify;
		font-size: 10px;
/*		position: absolute;
		height: 25.4mm;
		bottom: 5.4mm;
*/		float: left; 
		margin-left: 10mm;
	}
	
	</style>

   </head>
   <body> 
	<div id="header">
		<div class="col-md-2 left" style="float: left; margin-left: 10mm; margin-top:-60px">
		<img src="assets/images/logo_perhunungan.png" class='img-responsive' style='width:100px;height:100px;'>
		</div>
		<div class="col-md-10 right" style="margin-top:-20px; font-family: Sans-serif;font-size: 20px;font-weight: bold;" align="center">
			KOMISI PEMILIHAN UMUM REPUBLIK INDONESIA</br><p style="font-size: 15px;font-weight: normal;">Jalan Imam Bonjol No. 29</p></br><p style="font-size: 15px;font-weight: normal; margin-top:-10px;">Jakarta</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6" style="margin-top:-70px;font-size: 10px;">
			Telp. 021 3193 7223
		</div>
		<div class="col-md-6" style="text-align: right; margin-top:-40px;font-size: 10px;">
			Fax. 021 3157 759
		</div>
		<hr style="margin: 0pt auto;
		width: 100%;
		border: 2px double black;"/>
	</div>
	<div class="row"><br>
		<div class="col-md-12" align="center">
			<u>LAPORAN GLOBAL PENGELUARAN SPPD</u></br>
		</div>
		<br>
		<div class="col-md-12" style="margin-left: 10mm;">
		</div>
	</div>

	<table border="1" style="width:98%;margin-right:-50px;">
		<thead>
			<tr>
			<th style="text-align:center;">No</th>
			<th style="text-align:center;">Nama</th>
			<th style="text-align:center;">Daerah</th>
			<th style="text-align:center;">Tranport</th>
			<th style="text-align:center;">Hotel</th>
			<th style="text-align:center;">Total</th>
			</tr>
		</thead>
		<tbody>
			<?php $no = 1;
                foreach($laporan->result_array() as $me)
                { ?>
            	<tr><td><?php echo $no;?></td><td><?php echo $me['nama'];?></td><td><?php echo '('.$me['lama'].' hari) '.$me['daerah'];?></td><td style="text-align:right;"><?php echo $me['bandara'].' + '.$me['local'];?></td><td style="text-align:right;"><?php echo $me['hotel'];?></td><td style="text-align:right;"><?php echo $me['total'];?></td></tr>
            	<?php $no = $no+1;} ?>
		</tbody>
	</table>

   </body>
</html>