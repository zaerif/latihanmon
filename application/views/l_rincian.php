<!doctype html> 
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Pembayaran</title>
      <!-- bootstrap -->
      <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	
	<style type="text/css">
/*	html, body{
		width: 150mm;
		height: 297mm;
		position: relative;
	}
	body{
		padding: 0 25.4mm;
		border: 0.5px solid black;
		font: 11pt Segoe UI;
	}
*/	#header{
		font: 10pt Segoe UI;
		height: 20mm;
	}
	#header h1{
		font: 14pt Segoe UI;
		margin-bottom: 0;
	}
	#header hr{
		margin: 2pt auto;
		width: 90%;
		border: 1px double black;
	}
	#header .left img{
		height: 32.8mm;
		width: 26.2mm;
		margin-top: 14pt;
	}
	#header .right img{
		height: 21.2mm;
		width: 24.1mm;
		margin-top: 24pt;
	}
	#konten{
		margin-left: 33mm;
		text-align: justify;
	}
	#ttd{
		float: right;
		text-align: center;
		margin-right: 150px;
	}
	tr td{
		text-align: justify;
		vertical-align: top;
	}
	p.ganti{
		page-break-after: always;
	}
	tr td:first-child{
		width: 20mm;
	}
	tr td:last-child{
		width: 10mm;
	}
	footer{
		clear: both;
		margin-top: 30mm;
		text-align: justify;
		font-size: 10px;
/*		position: absolute;
		height: 25.4mm;
		bottom: 5.4mm;
*/		float: left; 
		margin-left: 10mm;
	}
	
	</style>

   </head>
   <body> 
	<div id="header">
		<div class="col-md-12 right" style="margin-top:-20px; font-family: Sans-serif;font-size: 20px;font-weight: bold;" align="center">
			KOMISI PEMILIHAN UMUM REPUBLIK INDONESIA</br><p style="font-size: 15px;font-weight: normal;">Jalan Imam Bonjol No. 29</p></br><p style="font-size: 15px;font-weight: normal; margin-top:-10px;">Jakarta</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6" style="margin-top:-70px;font-size: 10px;">
			Telp. 021 3193 7223
		</div>
		<div class="col-md-6" style="text-align: right; margin-top:-40px;font-size: 10px;">
			Fax. 021 3157 759
		</div>
		<hr style="margin: 0pt auto;
		width: 100%;
		border: 2px double black;"/>
	</div>

	<div id="judul" class="row">
		<div class="col-md-12" align="center" style="text-decoration: underline;">
			RINCIAN PERTANGGUNG JAWABAN<br>BIAYA PERJALANAN DINAS
		</div>
		<div class="col-md-12" style="margin-left: 10mm;">
			<table>
				<tr><td style="width:40mm;font-size:12px;">Nomor Surat Tugas </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm"><?php echo $no; ?></td></tr>
				<tr><td style="width:40mm;font-size:12px;">Tanggal Surat Tugas </td><td style="width: 10mm;">:</td><td ><?php echo $dari; ?><p style="float:right">Tanggal PM/SP2D</p></td></tr>
				<tr><td style="width:40mm;font-size:12px;">Penanggung Jawab Kegiatan </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm"></td></tr>
				<tr style="text-align: center;"><td colspan=3 style="text-align: center;">
					<table class="table table-bordered">
						<tr><td style="width:5mm;">No</td><td colspan=2 style="width:100mm;">URAIAN BIAYA</td><td>JUMLAH</td><td>KETERANGAN</td></tr>
						<tr><td style="width:5mm;">1</td><td colspan=2 style="width:100mm;">Harian (Uang makan, uang saku, transportasi lokal) selama <?php echo $lama; ?> hari x Rp. <?php echo $mharian; ?>,-</td><td style="width:30mm;text-align:right;">Rp. <?php echo $harian; ?>,-</td><td>-</td></tr>
						<tr><td style="width:5mm;">2</td><td colspan=2>Transport Pegawai:<br><ul><li>Tiket Pesawat/ kereta api/ kapal laut/ bus/ transportasi lainnya (airport tax, boarding pass, tiket kendaraan, karcis tol)</li><li>Transport dari tempat kedudukan ke</li><li>Transport dari.... ke.....</li></ul></td><td style="width:30mm;text-align:right;">Rp. <?php echo $tiket; ?>,-</td><td>-</td></tr>
						<tr><td style="width:5mm;">3</td><td colspan=2 style="width:100mm;">Penginapan/ Hotel</td><td style="width:30mm;text-align:right;">Rp. <?php echo $hotel; ?>,-</td><td>-</td></tr>
						<tr><td style="width:5mm;"></td><td colspan=2 style="width:100mm;">Jumlah</td><td style="width:30mm;text-align:right;">Rp. <?php echo $total; ?>,-</td><td>-</td></tr>
						<tr><td style="width:5mm;"></td><td colspan=3>Terbilang : <?php echo $ttotal; ?> rupiah</td><td>-</td></tr>
					</table>
				</td></tr>
			</table>
			<div class="row">
		   		<div class="col-md-6" style="text-align:center;float:left">
		   			Telah dibayarkan sejumlah ( Rp. <?php echo $totalall; ?>,- )<br>Bendahara<br><br><br>(<?php echo $bendahara; ?>)
		   		</div>
		   		<div class="col-md-6" style="text-align:center;float:right;">
		   			Telah menerima jumlah uang ( Rp. <?php echo $totalall; ?>,- )<br>Yang menerima<br><br><br>(<?php echo $nama; ?>)
		   		</div>
			</div>
			<hr style="margin: 0pt auto;
			width: 100%;
			border: 2px double black;"/>
			<div class="col-md-12" style="text-align:center"><b>PERHITUNGAN SPPD RAMPUNG</b></div>
			<table>
				<tr><td style="width:40mm;font-size:12px;">Ditetapkan sejumlah </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm">Rp. <?php echo $totalall; ?>,- ( <?php echo $ttotalall; ?> rupiah )</td></tr>
				<tr><td style="width:40mm;font-size:12px;">Yang di bayarkan semula </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm"> - </td></tr>
				<tr><td style="width:40mm;font-size:12px;">Sisa Kurang/lebih </td><td style="width: 10mm;">:</td><td style="text-align: justify; width:100mm"> - </td></tr>
			</table>
			<p><br></p>
		</div>
	</div>

	<div id="ttd" class="row">
		<div class="col-md-12">
		   			<?php echo $jabpenyetuju; ?><br><br><br>(<?php echo $penyetuju; ?>)<br>NIP. <?php echo $nippenyetuju; ?>
		</div>
	</div>
   </body>
</html>