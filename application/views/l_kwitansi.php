<!doctype html> 
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Pembayaran</title>
      <!-- bootstrap -->
      <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	
	<style type="text/css">
/*	html, body{
		width: 150mm;
		height: 297mm;
		position: relative;
	}
	body{
		padding: 0 25.4mm;
		border: 0.5px solid black;
		font: 11pt Segoe UI;
	}
*/	#header{
		font: 10pt Segoe UI;
		height: 20mm;
	}
	#header h1{
		font: 14pt Segoe UI;
		margin-bottom: 0;
	}
	#header hr{
		margin: 2pt auto;
		width: 90%;
		border: 1px double black;
	}
	#header .left img{
		height: 32.8mm;
		width: 26.2mm;
		margin-top: 14pt;
	}
	#header .right img{
		height: 21.2mm;
		width: 24.1mm;
		margin-top: 24pt;
	}
	#konten{
		margin-left: 33mm;
		text-align: justify;
	}
	#ttd{
		float: right;
		text-align: center;
		margin-right: 150px;
	}
	tr td{
		text-align: justify;
		vertical-align: top;
	}
	p.ganti{
		page-break-after: always;
	}
	tr td:first-child{
		width: 20mm;
	}
	tr td:last-child{
		width: 10mm;
	}
	footer{
		clear: both;
		margin-top: 30mm;
		text-align: justify;
		font-size: 10px;
/*		position: absolute;
		height: 25.4mm;
		bottom: 5.4mm;
*/		float: left; 
		margin-left: 10mm;
	}
	
	</style>

   </head>
   <body> 
   	<table>
   		<tr><td style="width:70mm;text-align:center"><b>KOMISI PEMILIHAN UMUM</b></td><td style="width:50mm;"></td><td style="width:80mm">Lembar : .................</td></tr>
   		<tr><td style="width:70mm;text-align:center">Jl. Imam Bonjol Bo.29</td></tr>
   		<tr><td style="width:70mm;text-align:center">Jakarta</td></tr>
   		<tr><td colspan=3 style="text-align:center"><b>KWITANSI</b></td></tr>
   	</table>
   	<table>
   		<tr><td style="width:20mm;text-align:left;">Nomor</td><td style="width:10mm;text-align:left;">:</td><td  style="width:150mm;text-align:left;">.................</td></tr>
   		<tr><td style="width:20mm;text-align:left;">m.a</td><td style="width:10mm;text-align:left;">:</td><td  style="width:150mm;text-align:left;">.................</td></tr>
   		<tr><td style="width:20mm;text-align:left;"></td><td style="width:10mm;text-align:left;"></td><td  style="width:150mm;text-align:left;">Sudah terima dari Komisi Pemilihan Umum di Jakarta</td></tr>
   		<tr><td style="width:20mm;text-align:left;"></td><td style="width:10mm;text-align:left;"></td><td  style="width:150mm;text-align:left;">Uang sebesar :  Rp. <?php echo $totalall; ?>,- ( <?php echo $ttotalall; ?> rupiah )</td></tr>
   		<tr><td style="width:20mm;text-align:left;"></td><td style="width:10mm;text-align:left;"></td><td  style="width:150mm;text-align:left;">
   			<table>
   				<tr><td rowspan=2 style="vertical-align:middle;width:40mm;">Guna Pembayaran</td><td style="border-bottom: 1px solid black;margin-left:10mm;width:40mm;">Biaya perjalanan dinas (lumpsum)</td><td rowspan=2 style="vertical-align:middle;margin-left:150px;">menurut :</td></tr>
   				<tr><td style="margin-left:10mm;width:40mm;">Kekurangan biaya perjalan dinas</td></tr>
   			</table>
   		</td></tr>
   		<tr><td style="width:20mm;text-align:left;"></td><td style="width:10mm;text-align:left;"></td><td  style="width:150mm;text-align:left;">Surat Perintah Perjalanan Dinas dari KEPALA BIRO UMUM KOMISI PEMILIHAN UMUM tanggal <?php echo $dari; ?> Nomor <?php echo $no; ?> untuk perjalanan dinas PP dari Jakarta ke <?php echo $daerah; ?></td></tr>
   		<tr><td style="width:20mm;text-align:left;">Terbilang</td><td style="width:10mm;text-align:left;">:</td><td  style="width:150mm;text-align:left;">Rp. <?php echo $totalall; ?>,- </td></tr>
   	</table><br><br>
   	<div class="row">
   		<div class="col-md-6" style="text-align:center;float:left">
   			<?php echo $jabpenyetuju; ?><br><br><br>(<?php echo $penyetuju; ?>)<br>NIP. <?php echo $nippenyetuju; ?>
   		</div>
   		<div class="col-md-6" style="text-align:center;float:right">
   			<br>Yang bepergian,<br><br><br><br>( <?php echo $nama; ?> )
   		</div>
   	</div>
   </body>
</html>