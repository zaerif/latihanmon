<?php $this->load->view('layouts/admin'); ?>
            <!-- top navigation -->
            <div class="top_nav navbar-fixed-top">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="assets/images/users.png" alt=""><?php echo $this->session->userdata('username'); ?>                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    
                                    <li><a href="Welcome/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>
						</ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
                <div class="right_col" role="main">           


                	<div class="clearfix"></div>

                    <div class="row headofhtml">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><i class="fa fa-dashboard"></i> Detail Pegawai <small>Sistem Informasi Surat Perjalanan Dinas</small></h2><br>
                                    <div class="col-md-12">
                                        <div class="row" style="margin-top:10px;">
                                            <div class="form-group"> 
                                                <label for="field-2" class="control-label col-md-2" style="text-align:right;">Nama</label> 
                                                <div class="col-md-8">
                                                    <input type="text" placeholder="Nama" value="<?php echo $nama; ?>" data-a-sign="" class="form-control" disabled>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="modal fade bs-example-modal-lg-ppk" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">Form Belakang SPD</h4>
                                            </div>
                                            <table class="tabels" border="1px" style="margin:10px;width:98%;">
                
                        <input type="hidden" value="88" name="id">
                <tbody><tr>
                    <td width="48%">
                        <table>
                            <tbody><tr>
                                <td class="cel1"></td>
                            </tr>
                        </tbody></table>
                    </td>
                    <td>
                        <?php $detailbelakang = explode('##',$belakang); ?>
                        <table class="tbl_isi">
                            <tbody>
                                <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Berangkat dari</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-1" value="<?php echo $detailbelakang[0]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Ke</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-2" value="<?php echo $detailbelakang[1]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Pada Tanggal</p> 
                                        <div class="col-md-8">
                                            <input class="tangg" type="text" id="fb-3" value="<?php echo $detailbelakang[2]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div style="margin-top:200px;"></div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nama</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-4" value="<?php echo $detailbelakang[3]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nip</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-5" value="<?php echo $detailbelakang[4]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
                <tr>
                    <td class="cel1">
                        <table class="tbl_isi" border="0">
                            <tbody>
                                <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Tiba di</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-6" value="<?php echo $detailbelakang[5]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Pada Tanggal</p> 
                                        <div class="col-md-8">
                                            <input class="tangg" type="text" id="fb-7" value="<?php echo $detailbelakang[6]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Kepala</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-8" value="<?php echo $detailbelakang[7]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div style="margin-top:200px;"></div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nama</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-9" value="<?php echo $detailbelakang[8]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nip</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-10" value="<?php echo $detailbelakang[9]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                        </tbody></table>
                    </td>
                    <td class="cel2">
                        <table class="tbl_isi" border="0">
                            <tbody>
                                <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Berangkat dari</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-11" value="<?php echo $detailbelakang[10]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Ke</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-12" value="<?php echo $detailbelakang[11]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Pada Tanggal</p> 
                                        <div class="col-md-8">
                                            <input type="text" class="tangg" id="fb-13" value="<?php echo $detailbelakang[12]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Kepala</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-14" value="<?php echo $detailbelakang[13]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div style="margin-top:200px;"></div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nama</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-15" value="<?php echo $detailbelakang[14]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nip</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-16" value="<?php echo $detailbelakang[15]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
                <tr>
                    <td class="cel1">
                        <table class="tbl_isi" border="0">
                            <tbody>
                                <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Tiba di</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-17" value="<?php echo $detailbelakang[16]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Pada Tanggal</p> 
                                        <div class="col-md-8">
                                            <input type="text" class="tangg" id="fb-18" value="<?php echo $detailbelakang[17]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Kepala</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-19" value="<?php echo $detailbelakang[18]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div style="margin-top:200px;"></div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nama</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-20" value="<?php echo $detailbelakang[19]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nip</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-21" value="<?php echo $detailbelakang[20]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                        </tbody></table>
                    </td>
                    <td class="cel2">
                        <table class="tbl_isi" border="0">
                            <tbody>
                                <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Berangkat dari</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-22" value="<?php echo $detailbelakang[21]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Ke</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-23" value="<?php echo $detailbelakang[22]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Pada Tanggal</p> 
                                        <div class="col-md-8">
                                            <input type="text" class="tangg" id="fb-24" value="<?php echo $detailbelakang[23]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Kepala</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-25" value="<?php echo $detailbelakang[24]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div style="margin-top:200px;"></div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nama</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-26" value="<?php echo $detailbelakang[25]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nip</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-27" value="<?php echo $detailbelakang[26]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
                <tr>
                    <td class="cel1">
                        <table class="tbl_isi" border="0">
                            <tbody>
                                <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Tiba di</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-28" value="<?php echo $detailbelakang[27]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Pada Tanggal</p> 
                                        <div class="col-md-8">
                                            <input type="text" class="tangg" id="fb-29" value="<?php echo $detailbelakang[28]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Kepala</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-30" value="<?php echo $detailbelakang[29]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div style="margin-top:200px;"></div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nama</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-31" value="<?php echo $detailbelakang[30]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nip</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-32" value="<?php echo $detailbelakang[31]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                        </tbody></table>
                    </td>
                    <td class="cel2">
                        <table class="tbl_isi" border="0">
                            <tbody>
                                <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Berangkat dari</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-33" value="<?php echo $detailbelakang[32]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Ke</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-34" value="<?php echo $detailbelakang[33]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Pada Tanggal</p> 
                                        <div class="col-md-8">
                                            <input type="text" class="tangg" id="fb-35" value="<?php echo $detailbelakang[34]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Kepala</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-36" value="<?php echo $detailbelakang[35]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div style="margin-top:200px;"></div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nama</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-37" value="<?php echo $detailbelakang[36]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nip</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-38" value="<?php echo $detailbelakang[37]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
                <tr>
                    <td class="cel1">
                        <table class="tbl_isi" border="0">
                            <tbody>
                                <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Tiba di</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-39" value="<?php echo $detailbelakang[38]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">(Tempat Kedudukan)</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-40" value="<?php echo $detailbelakang[39]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Pada Tanggal</p> 
                                        <div class="col-md-8">
                                            <input type="text" class="tangg" id="fb-41" value="<?php echo $detailbelakang[40]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-8" style="text-align:left;">Pejabat yang berwenang/ Pejabat lainnya yang di tunjuk</p> 
                                        <div class="col-md-8">
                                        </div>
                                    </div>
                            </tr>
                            <tr>
                                    <div ></div>
                                    <div class="col-md-12" style="margin-top:100px;">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nama</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-42" value="<?php echo $detailbelakang[41]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nip</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-43" value="<?php echo $detailbelakang[42]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                            </tr>
                        </tbody></table>
                    </td>
                    <td class="cel2">
                        <table class="tbl_isi" border="0">
                            <tbody><tr>
                                <td class="cel2" colspan="2">
                                Telah diperiksa dengan keterangan bahwa perjalanan<br>
                                tersebut atas perintahnya semata-mata untuk<br>
                                kepentingan jabatan dalam waktu yang sesingkat-<br>
                                singkatnya.</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="margin-top:200px;"></div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nama</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-44" value="<?php echo $detailbelakang[43]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <p for="field-2" class="col-md-4" style="text-align:left;">Nip</p> 
                                        <div class="col-md-8">
                                            <input type="text" id="fb-45" value="<?php echo $detailbelakang[44]; ?>" data-a-sign="">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
                
                
            </tbody></table>
                                            <div class="modal-footer">
                                                <button id="update" class="btn btn-primary">Update</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <h2>Surat SPD</h2>
                                    <div class="bs-glyphicons">
                                        <ul class="bs-glyphicons-list">                                     
                                            <a target="_blank" href="laporan/notadinas/<?php echo $kodeid; ?>" style="cursor:pointer;">
                                                <li>
                                                    <span class="badge bg-orange"><i class="fa fa-exclamation"></i></span>
                                                    <span class="glyphicon glyphicon-print"></span>
                                                    <span class="glyphicon-class">Cetak Nota Dinas</span>   
                                                </li>
                                            </a>       
                                            <a target="_blank" href="surattugas/prints/<?php echo $kodeid; ?>" style="cursor:pointer;">
                                                <li>
                                                    <span class="badge bg-orange"><i class="fa fa-exclamation"></i></span>
                                                    <span class="glyphicon glyphicon-print"></span>
                                                    <span class="glyphicon-class">Cetak Surat Tugas</span>   
                                                </li>
                                            </a>
                                        </ul>
                                    </div>
                                    <div class="bs-glyphicons">
                                        <ul class="bs-glyphicons-list">
                                            <a target="_blank" href="laporan/belakang/<?php echo $kodeid; ?>" style="cursor:pointer;">
                                                <li>
                                                    <span class="badge bg-orange"><i class="fa fa-exclamation"></i></span>
                                                    <span class="glyphicon glyphicon-print"></span>
                                                    <span class="glyphicon-class">Cetak Belakang SPD</span> 
                                                </li>
                                            </a>    
                                            <a data-toggle="modal" data-target=".bs-example-modal-lg-ppk" style="cursor:pointer;">
                                                <li>
                                                    <span class="badge bg-orange"><i class="fa fa-exclamation"></i></span>
                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                    <span class="glyphicon-class">Form Belakang SPD</span>  
                                                </li>
                                            </a>
                                            <a target="_blank" href="laporan/belakang2/<?php echo $kodeid; ?>" style="cursor:pointer;">
                                                <li>
                                                    <span class="badge bg-orange"><i class="fa fa-exclamation"></i></span>
                                                    <span class="glyphicon glyphicon-print"></span>
                                                    <span class="glyphicon-class">Cetak Hasil Belakang SPD</span>   
                                                </li>
                                            </a>    
                                        </ul>
                                    </div>
                                    <h2>Penyerahan Berkas</h2>
                                    <div class="row" style="margin-top:10px;">
                                        <div class="form-group"> 
                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Nomor</label> 
                                            <div class="col-md-6">
                                                <input type="text" id="field-1" placeholder="Nomor" value="<?php echo $no; ?>" data-a-sign="" class="form-control" disabled>
                                                <input type="text" id="transaksi" value="tambah" data-a-sign="" class="form-control hidden">
                                                <input type="text" id="textkode" value="" data-a-sign="" class="form-control hidden">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row" style="margin-top:10px;">
                                        <div class="form-group"> 
                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Tiket Pesawat/ Kereta/ Transportasi Lainnya</label> 
                                            <div class="col-md-3">
                                                <input type="text" id="field-3" placeholder="Nominal" value="<?php echo $tiket; ?>" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                            </div> 
                                            <div class="col-md-3">
                                                <input type="file" id="field-4" placeholder="Nomor" value="" data-a-sign="" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <?php if($dtiket != ""){?>
                                                <button class="btn btn-primary view1" onclick="lihatdata()"><i class="fa fa-eye"></i></button>
                                                <?php }?>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row" style="margin-top:10px;">
                                        <div class="form-group"> 
                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Transportasi</label> 
                                            <div class="col-md-3">
                                                <input type="text" id="field-5" placeholder="Nominal" value="<?php echo $transport; ?>" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                            </div> 
                                            <div class="col-md-3">
                                                <input type="file" id="field-6" placeholder="Nomor" value="" data-a-sign="" class="form-control" >
                                            </div>
                                            <div class="col-md-3">
                                                <?php if($dtransport != ""){?>
                                                <button onclick="lihatdata1()" class="btn btn-primary view2"><i class="fa fa-eye"></i></button>
                                                <?php }?>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row" style="margin-top:10px;">
                                        <div class="form-group"> 
                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Hotel</label> 
                                            <div class="col-md-3">
                                                <input type="text" id="field-7" placeholder="Nominal" value="<?php echo $hotel; ?>" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                            </div> 
                                            <div class="col-md-3">
                                                <input type="file" id="field-8" placeholder="Nomor" value="" data-a-sign="" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <?php if($dhotel != ""){?>
                                                <button onclick="lihatdata2()"  class="btn btn-primary view3"><i class="fa fa-eye"></i></button>
                                                <?php }?>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row" style="margin-top:10px;">
                                        <div class="form-group"> 
                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Dokumen Kerja</label> 
                                            <div class="col-md-6">
                                                <input type="file" id="field-9" placeholder="Nomor" value="" data-a-sign="" class="form-control" style="text-align:right;">
                                            </div>
                                            <div class="col-md-3">
                                                <?php if($dkerja != ""){?>
                                                <button onclick="lihatdata3()"  class="btn btn-primary view4"><i class="fa fa-eye"></i></button>
                                                <?php }?>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row" style="margin-top:10px;">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-success" id="simpan"><i class="fa fa-disket"></i> Simpan</button>
                                        </div>
                                    </div>
                                    <h2>Check Biaya</h2>
                                    <div class="bs-glyphicons">
                                            <ul class="bs-glyphicons-list">
                                                <a href="pembayaran" style="cursor:pointer;">
                                                    <li>
                                                        <span class="badge bg-orange"><i class="fa fa-exclamation"></i></span>
                                                        <span class="glyphicon glyphicon-print"></span>
                                                        <span class="glyphicon-class">Check Rincian Biaya</span>    
                                                    </li>
                                                </a> 
                                                <a target="_blank" href="laporan/surat/<?php echo $kodeid; ?>" style="cursor:pointer;">
                                                    <li>
                                                        <span class="badge bg-orange"><i class="fa fa-exclamation"></i></span>
                                                        <span class="glyphicon glyphicon-print"></span>
                                                        <span class="glyphicon-class">Cetak Surat Perjalanan Dinas</span>   
                                                    </li>
                                                </a>       
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            <!-- end content -->
            	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                Konfirmasi Hapus
				            </div>
				            <div class="modal-body">
				                Anda yakin menghapus data?
				            </div>
				            <div class="modal-footer">
				                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				                <a class="btn btn-danger btn-ok">Delete</a>
				            </div>
				        </div>
				    </div>
				</div>
                </div>

                <!-- footer content -->

           
            </div>
            <!-- /page content -->

        

    

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="assets/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="assets/js/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="assets/js/bootstrap-progressbar.min.js"></script>
    <script src="assets/js/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="assets/js/icheck.min.js"></script>
    <script src="assets/js/custom.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="assets/js/moment.min2.js"></script>
    <script type="text/javascript" src="assets/js/daterangepicker.js"></script>
    <!-- input mask -->
    <script src="assets/js/jquery.inputmask.js"></script>
    <!-- knob -->
    <script src="assets/js/jquery.knob.min.js"></script>
    <!-- range slider -->
    <script src="assets/js/ion.rangeSlider.min.js"></script>
    <!-- color picker -->
    <script src="assets/js/bootstrap-colorpicker.js"></script>
    <script src="assets/js/docs.js"></script>
	<!-- select2 -->
        <script src="assets/js/select2.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="assets/js/parsley.min.js"></script>
    <!-- image cropping -->
    <script src="assets/js/cropper.min.js"></script>
    <script src="assets/js/main2.js"></script>

	<script src="assets/js/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="assets/js/countries.js"></script>
        <script src="assets/js/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.bootstrap.js"></script>

		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/jszip.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/pdfmake.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/vfs_fonts.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.html5.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.print.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedHeader.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.keyTable.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.responsive.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/responsive.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.scroller.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.colVis.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedColumns.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/autoNumeric/autoNumeric.js" type="text/javascript"></script>
    <!-- input_mask -->
    <!-- /input mask -->
    <script type="text/javascript">
        $('.gol2').select2({ width: '100%' });
        $('.autonumber').autoNumeric('init');
        jQuery('.tangg').datepicker({
            toggleActive: true,
            format: "dd/mm/yyyy"
        });
    	$('#add').on('click',function(){
            $(this).addClass('hidden');
            $('.editroles').css('display','block');
            $('#batal').removeClass('hidden');
            $('.editrole').removeClass('hidden');
            $('#transaksi').val('tambah');
            $('#simpan').removeClass('hidden');
            bukatutup(false);
            clearForm();
        });
    	$('#batal').on('click',function(){
            bukatutup(false);
            $(this).addClass('hidden');
            $('#add').removeClass('hidden');
            $('.editrole').addClass('hidden');
        });


                var doclihat = '<?php echo $dtiket;?>', doclihat1 = '<?php echo $dtransport;?>',doclihat2 = '<?php echo $dhotel;?>', doclihat3 = '<?php echo $dkerja;?>';
                function lihatdata(){
                    window.open("<?php echo base_url('assets/sppd/"+doclihat+"')?>","Ratting","width=550,height=170,left=150,top=200,toolbar=0,status=0,");
                }

                function lihatdata1(){
                    window.open("<?php echo base_url('assets/sppd/"+doclihat1+"')?>","Ratting","width=550,height=170,left=150,top=200,toolbar=0,status=0,");
                }

                function lihatdata2(){
                    window.open("<?php echo base_url('assets/sppd/"+doclihat2+"')?>","Ratting","width=550,height=170,left=150,top=200,toolbar=0,status=0,");
                }

                function lihatdata3(){
                    window.open("<?php echo base_url('assets/sppd/"+doclihat3+"')?>","Ratting","width=550,height=170,left=150,top=200,toolbar=0,status=0,");
                }

                $('#simpan').on('click',function(){
                    var filename = $("#field-4").get(0).files[0];
                    var filename1 = $("#field-6").get(0).files[0];
                    var filename2 = $("#field-8").get(0).files[0];
                    var filename3 = $("#field-9").get(0).files[0];
                    var kode=$('#textkode').val();
                    var transaksi=$('#transaksi').val();
                    $.ajax({
                        type : "POST",
                        contentType: false,
                        processData: false,
                        url  : "<?php echo base_url('index.php/kelengkapantugas/simpan_surat')?>",
                        data: function() {
                            var data = new FormData();
                            data.append("a1", $("#field-1").val());
                            data.append("b1", $("#field-3").autoNumeric('get'));
                            data.append("c1", $("#field-5").autoNumeric('get'));
                            data.append("d1", $("#field-7").autoNumeric('get'));
                            data.append("e1", filename);
                            data.append("f1", filename1);
                            data.append("g1", filename2);
                            data.append("h1", filename3);
                            data.append("i1", '<?php echo $kodepeg;?>');
                            return data;
                            // Or simply return new FormData(jQuery("form")[0]);
                        }(),
                        success: function(data){
                            table.ajax.reload();
                            $( "#batal" ).trigger( "click" );
                        }
                    });
                    return false;
                });

                $('#update').on('click',function(){
                    var kumpul = $('#fb-1').val()+'##'+$('#fb-2').val()+'##'+$('#fb-3').val()+'##'+$('#fb-4').val()+'##'+$('#fb-5').val()+'##'+$('#fb-6').val()+'##'+$('#fb-7').val()+'##'+$('#fb-8').val()+'##'+$('#fb-9').val()+'##'+$('#fb-10').val()+'##'+$('#fb-11').val()+'##'+$('#fb-12').val()+'##'+$('#fb-13').val()+'##'+$('#fb-14').val()+'##'+$('#fb-15').val()+'##'+$('#fb-16').val()+'##'+$('#fb-17').val()+'##'+$('#fb-18').val()+'##'+$('#fb-19').val()+'##'+$('#fb-20').val()+'##'+$('#fb-21').val()+'##'+$('#fb-22').val()+'##'+$('#fb-23').val()+'##'+$('#fb-24').val()+'##'+$('#fb-25').val()+'##'+$('#fb-26').val()+'##'+$('#fb-27').val()+'##'+$('#fb-28').val()+'##'+$('#fb-29').val()+'##'+$('#fb-30').val()+'##'+$('#fb-31').val()+'##'+$('#fb-32').val()+'##'+$('#fb-33').val()+'##'+$('#fb-34').val()+'##'+$('#fb-35').val()+'##'+$('#fb-36').val()+'##'+$('#fb-37').val()+'##'+$('#fb-38').val()+'##'+$('#fb-39').val()+'##'+$('#fb-40').val()+'##'+$('#fb-41').val()+'##'+$('#fb-42').val()+'##'+$('#fb-43').val()+'##'+$('#fb-44').val();
                    $.ajax({
                        type : "POST",
                        contentType: false,
                        processData: false,
                        url  : "<?php echo base_url('index.php/kelengkapantugas/simpan_surat2')?>",
                        data: function() {
                            var data = new FormData();
                            data.append("a1", $("#field-1").val());
                            data.append("b1", kumpul);
                            data.append("i1", '<?php echo $kodepeg;?>');
                            return data;
                            // Or simply return new FormData(jQuery("form")[0]);
                        }(),
                        success: function(data){
                            table.ajax.reload();
                            $( "#batal" ).trigger( "click" );
                        }
                    });
                    return false;
                });
    </script>
	
	<script type="text/javascript" src="assets/js/jquery.smartWizard.js"></script>
</body>
</html>