
<?php 
if($this->session->userdata('stts')=="1"){
$this->load->view('layouts/admin');
} else {

$this->load->view('layouts/adminpegawai');
} ?>
            <!-- top navigation -->
            <div class="top_nav navbar-fixed-top">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="assets/images/users.png" alt=""><?php echo $this->session->userdata('username'); ?>                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    
                                    <li><a href="Welcome/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
                <div class="right_col" role="main">           


                	<div class="clearfix"></div>

                    <div class="row headofhtml">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><i class="fa fa-dashboard"></i> Kelengkapan <small>Sistem Informasi Surat Perjalanan Dinas</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                	<div class="col-md-2 col-sm-2 col-xs-12 hidden">
										<button type="button" class="btn btn-success" id="add"><i class="fa fa-plus-circle"></i> Tambah Surat Tugas</button>
										<button type="button" class="btn btn-danger hidden" id="batal"><i class="fa fa-close"></i> Batal</button>
									</div>
									<div class="row editrole hidden">
				                        <div class="col-md-12">
				                            <div class="x_panel">
				                                <div class="x_title">
				                                    <h2 id="titleedit"><i class="fa fa-dashboard"></i> Edit Kelengkapan <small></small></h2>
				                                    <ul class="nav navbar-right panel_toolbox">
				                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				                                        </li>
				                                    </ul>
				                                    <div class="clearfix"></div>
				                                </div>
				                                <div class="x_content editroles">
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Nomor</label> 
                                                            <div class="col-md-6">
                                                                <input type="text" id="transaksi" value="tambah" data-a-sign="" class="form-control hidden">
                                                                <input type="text" id="textkode" value="" data-a-sign="" class="form-control hidden">
                                                                <input type="text" id="field-1" placeholder="Nomor" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Tiket Pesawat/ Kereta/ Transportasi Lainnya</label> 
                                                            <div class="col-md-3">
                                                                <input type="text" id="field-2" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div> 
                                                            <div class="col-md-3">
                                                                <input type="file" id="field-3" placeholder="Nomor" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <button class="btn btn-primary view1" onclick="lihatdata()"><i class="fa fa-eye"></i></button>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Transportasi</label> 
                                                            <div class="col-md-3">
                                                                <input type="text" id="field-4" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div> 
                                                            <div class="col-md-3">
                                                                <input type="file" id="field-5" placeholder="Nomor" value="" data-a-sign="" class="form-control" >
                                                            </div>
                                                            <div class="col-md-3">
                                                                <button onclick="lihatdata1()" class="btn btn-primary view2"><i class="fa fa-eye"></i></button>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Hotel</label> 
                                                            <div class="col-md-3">
                                                                <input type="text" id="field-6" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div> 
                                                            <div class="col-md-3">
                                                                <input type="file" id="field-7" placeholder="Nomor" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <button onclick="lihatdata2()"  class="btn btn-primary view3"><i class="fa fa-eye"></i></button>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Dokumen Kerja</label> 
                                                            <div class="col-md-6">
                                                                <input type="file" id="field-8" placeholder="Nomor" value="" data-a-sign="" class="form-control" style="text-align:right;">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <button onclick="lihatdata3()"  class="btn btn-primary view4"><i class="fa fa-eye"></i></button>
                                                            </div>
                                                        </div> 
                                                    </div>
				                                	<div class="row" style="margin-top:10px;">
				                                		<div class="col-md-2">
				                                		</div>
				                                		<div class="col-md-6">
                                                            <button type="button" class="btn btn-danger" id="cancel"><i class="fa fa-disket"></i> Batal</button>
				                                			<button type="button" class="btn btn-success" id="simpan"><i class="fa fa-disket"></i> Simpan</button>
				                                		</div>
				                                	</div>
				                                    <div class="clearfix"></div>
				                                </div>
				                            </div>
				                        </div>
				                    </div>

		                            <table id="datatable-buttons" class="table table-striped table-bordered">
		                                <thead>
		                                <tr>
		                                    <th>Action</th>
                                            <th>Nomor</th>
		                                    <th>Nominal Tiket</th>
		                                    <th>Dokumen Tiket</th>
		                                    <th>Dokumen Kerja</th>
		                                </tr>
		                                </thead>
		                                <tbody>
		                                </tbody>
		                            </table>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            <!-- end content -->
            	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                Konfirmasi Hapus
				            </div>
				            <div class="modal-body">
				                Anda yakin menghapus data?
				            </div>
				            <div class="modal-footer">
				                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				                <a class="btn btn-danger btn-ok">Delete</a>
				            </div>
				        </div>
				    </div>
				</div>
                </div>

                <!-- footer content -->

           
            </div>
            <!-- /page content -->

        

    

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="assets/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="assets/js/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="assets/js/bootstrap-progressbar.min.js"></script>
    <script src="assets/js/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="assets/js/icheck.min.js"></script>
    <script src="assets/js/custom.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="assets/js/moment.min2.js"></script>
    <script type="text/javascript" src="assets/js/daterangepicker.js"></script>
    <!-- input mask -->
    <script src="assets/js/jquery.inputmask.js"></script>
    <!-- knob -->
    <script src="assets/js/jquery.knob.min.js"></script>
    <!-- range slider -->
    <script src="assets/js/ion.rangeSlider.min.js"></script>
    <!-- color picker -->
    <script src="assets/js/bootstrap-colorpicker.js"></script>
    <script src="assets/js/docs.js"></script>
	<!-- select2 -->
        <script src="assets/js/select2.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="assets/js/parsley.min.js"></script>
    <!-- image cropping -->
    <script src="assets/js/cropper.min.js"></script>
    <script src="assets/js/main2.js"></script>

	<script src="assets/js/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="assets/js/countries.js"></script>
        <script src="assets/js/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.bootstrap.js"></script>

		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/jszip.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/pdfmake.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/vfs_fonts.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.html5.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.print.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedHeader.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.keyTable.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.responsive.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/responsive.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.scroller.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.colVis.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedColumns.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/autoNumeric/autoNumeric.js" type="text/javascript"></script>
    <!-- input_mask -->
    <!-- /input mask -->
    <script type="text/javascript">
    	$('.gol2').select2({ width: '100%' });
        $('.autonumber').autoNumeric('init');
    	$('#add').on('click',function(){
            $(this).addClass('hidden');
            $('.editroles').css('display','block');
            $('#batal').removeClass('hidden');
            $('.editrole').removeClass('hidden');
            $('#transaksi').val('tambah');
            $('#simpan').removeClass('hidden');
            bukatutup(false);
            clearForm();
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: "dd/mm/yyyy"
        });
    	$('#batal').on('click',function(){
            bukatutup(false);
            $(this).addClass('hidden');
            $('#add').removeClass('hidden');
            $('.editrole').addClass('hidden');
        });
        $('#cancel').on('click',function(){
            bukatutup(false);
            $('.editrole').addClass('hidden');
        });
        var table = $("#datatable-buttons").DataTable({
	          processing: true,
	          serverSide: true,
	                  ajax: {"url": "<?php echo base_url().'index.php/kelengkapantugas/get_surat_json'?>", "type": "POST"},
	                        columns: [
	                            {"data": "view"},
	                            {"data": "no"},
                                {"data": "nom_tiket", render: $.fn.dataTable.render.number(',', '.', ''), className: "text-right"},
                                {"data": "doc_tiket"},
                                {"data": "doc_kerja"}
	                      ],
	                    order: [[2, 'asc']],
	            dom: "Blfrtip",
	            buttons: [{
	                extend: "copy",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2 ]
                    }
	            }, {
	                extend: "csv",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2 ]
                    }
	            }, {
	                extend: "excel",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2 ]
                    }
	            }, {
	                extend: "pdf",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2 ]
                    }
	            }, {
	                extend: "print",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2 ]
                    }
	            }]
	        });

				function clearForm(){
                    $('#transaksi').val('tambah');
                    $('#field-1').val('');
                    $('#field-2').val('');
                    $('#field-13').val('');
                    $('#field-14').val('');
                }

                function bukatutup(status){
                    $('#field-1').prop("readonly", status);
                    $('#field-2').prop("readonly", status);
                    $('#field-3').prop("readonly", status);
                    $('#field-4').prop("readonly", status);
                    $('#field-5').prop("readonly", status);
                    $('#field-6').prop("readonly", status);
                    $('#field-7').prop("readonly", status);
                }
                var doclihat = '', doclihat1 = '',doclihat2 = '', doclihat3 = '';
                $('#datatable-buttons').on('click','.edit-row',function(){
                    var id=$(this).attr('data');
                    $.ajax({
                        type : "GET",
                        url  : "<?php echo base_url('index.php/kelengkapantugas/get_surat')?>",
                        dataType : "JSON",
                        data : {id:id},
                        success: function(data){
                            bukatutup(false);
                            $('.editaja').removeClass('hidden');
                            $( "#add" ).trigger( "click" );
                            $('#simpan').removeClass('hidden');
                            $('#transaksi').val('edit');
                            $('.view1').addClass('hidden');
                            $('.view2').addClass('hidden');
                            $('.view3').addClass('hidden');
                            $('.view4').addClass('hidden');
                            $.each(data,function(){
                                $('#textkode').val(id);
                                $('#field-1').val(data.a1);
                                $('#field-2').autoNumeric('set',data.b1);
                                $('#field-4').autoNumeric('set',data.c1);
                                $('#field-6').autoNumeric('set',data.d1);
                                if(data.e1 != 'Belum'){
                                    $('.view1').removeClass('hidden');
                                    doclihat = data.e1;
                                }
                                if(data.f1 != 'Belum'){
                                    $('.view2').removeClass('hidden');
                                    doclihat1 = data.f1;
                                }
                                if(data.g1 != 'Belum'){
                                    $('.view3').removeClass('hidden');
                                    doclihat2 = data.g1;
                                }
                                if(data.h1 != 'Belum'){
                                    $('.view4').removeClass('hidden');
                                    doclihat3 = data.h1;
                                }
                            });
                        }
                    });
                    return false;
                });


                function lihatdata(){
                    window.open("<?php echo base_url('assets/sppd/"+doclihat+"')?>","Ratting","width=550,height=170,left=150,top=200,toolbar=0,status=0,");
                }

                function lihatdata1(){
                    window.open("<?php echo base_url('assets/sppd/"+doclihat1+"')?>","Ratting","width=550,height=170,left=150,top=200,toolbar=0,status=0,");
                }

                function lihatdata2(){
                    window.open("<?php echo base_url('assets/sppd/"+doclihat2+"')?>","Ratting","width=550,height=170,left=150,top=200,toolbar=0,status=0,");
                }

                function lihatdata3(){
                    window.open("<?php echo base_url('assets/sppd/"+doclihat3+"')?>","Ratting","width=550,height=170,left=150,top=200,toolbar=0,status=0,");
                }
                $('#datatable-buttons').on('click','.hapus-row',function(){
                    var id=$(this).attr('data');
                    $('#confirm-delete').modal('show');
                    $('.btn-ok').on('click',function(){
	                    $.ajax({
	                    type : "POST",
	                    url  : "<?php echo base_url('index.php/surattugas/hapus_surat')?>",
	                    dataType : "JSON",
	                            data : {kode: id},
	                            success: function(data){
                    				$('#confirm-delete').modal('hide');
	                                table.ajax.reload();
                            		$( "#batal" ).trigger( "click" );
	                            }
	                        });
	                        return false;
	                });
                });

                $('#simpan').on('click',function(){
                    var filename = $("#field-3").get(0).files[0];
                    var filename1 = $("#field-5").get(0).files[0];
                    var filename2 = $("#field-7").get(0).files[0];
                    var filename3 = $("#field-8").get(0).files[0];
                    var kode=$('#textkode').val();
                    var transaksi=$('#transaksi').val();
                    $.ajax({
                        type : "POST",
                        contentType: false,
                        processData: false,
                        url  : "<?php echo base_url('index.php/kelengkapantugas/simpan_surat')?>",
                        data: function() {
                            var data = new FormData();
                            data.append("a1", $("#field-1").val());
                            data.append("b1", $("#field-2").autoNumeric('get'));
                            data.append("c1", $("#field-4").autoNumeric('get'));
                            data.append("d1", $("#field-6").autoNumeric('get'));
                            data.append("e1", filename);
                            data.append("f1", filename1);
                            data.append("g1", filename2);
                            data.append("h1", filename3);
                            return data;
                            // Or simply return new FormData(jQuery("form")[0]);
                        }(),
                        success: function(data){
                            table.ajax.reload();
                            $( "#batal" ).trigger( "click" );
                        }
                    });
                    return false;
                });
    </script>
	
	<script type="text/javascript" src="assets/js/jquery.smartWizard.js"></script>
</body>
</html>