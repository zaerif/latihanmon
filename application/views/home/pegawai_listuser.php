<?php $this->load->view('layouts/adminpegawai'); ?>
            <!-- top navigation -->
            <div class="top_nav navbar-fixed-top">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="assets/images/users.png" alt=""><?php echo $this->session->userdata('username'); ?>                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    
                                    <li><a href="Welcome/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>
						</ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
                <div class="right_col" role="main">           


                	<div class="clearfix"></div>

                    <div class="row headofhtml">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><i class="fa fa-dashboard"></i> Staff Kantor <small>Sistem Informasi Surat Perjalanan Dinas</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                	<div class="col-md-2 col-sm-2 col-xs-12">
										<button type="button" class="btn btn-success hidden" id="add"><i class="fa fa-plus-circle"></i> Tambah Pegawai</button>
										<button type="button" class="btn btn-danger hidden" id="batal"><i class="fa fa-close"></i> Batal</button>
									</div>
									<div class="row editrole hidden">
				                        <div class="col-md-12">
				                            <div class="x_panel">
				                                <div class="x_title">
				                                    <h2 id="titleedit"><i class="fa fa-dashboard"></i> Tambah Pegawai <small></small></h2>
				                                    <ul class="nav navbar-right panel_toolbox">
				                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				                                        </li>
				                                    </ul>
				                                    <div class="clearfix"></div>
				                                </div>
				                                <div class="x_content editroles">
				                                	<div class="row">
					                                    <div class="form-group"> 
					                                        <label for="field-1" class="control-label col-md-2" style="text-align:right;">Surat Tugas</label> 
					                                        <div class="col-md-6">
					                                        	<input type="text" id="transaksi" value="tambah" data-a-sign="" class="form-control hidden">
					                                        	<input type="text" id="textkode" value="" data-a-sign="" class="form-control hidden">
                                                                <select class="gol2" id="field-1">
                                                                    <option value="">Pilih Surat Tugas</option>
                                                                    <?php
                                                                        foreach($mst_st->result_array() as $me)
                                                                        {
                                                                            
                                                                    ?>
                                                                        <option value="<?php echo $me['no']; ?>"><?php echo $me['no']; ?></option>
                                                                    <?php
                                                                        }
                                                                    ?>
                                                                </select>
					                                        </div>
					                                    </div> 
					                                </div>
				                                	<div class="row" style="margin-top:10px;">
					                                    <div class="form-group"> 
					                                        <label for="field-4" class="control-label col-md-2" style="text-align:right;">Pegawai</label> 
					                                        <div class="col-md-6">
					                                        	<select class="gol2" id="field-2">
					                                        		<option value="">Pilih Pegawai</option>
                                                                    <?php
                                                                        foreach($mst_pegawai->result_array() as $me)
                                                                        {
                                                                            
                                                                    ?>
                                                                        <option value="<?php echo $me['id']; ?>"><?php echo $me['nama']; ?></option>
                                                                    <?php
                                                                        }
                                                                    ?>
					                                        	</select>
					                                        </div>
					                                    </div> 
				                                	</div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">Tanggal</label> 
                                                            <div class="col-md-6">
                                                                <div class="input-daterange input-group" id="date-range">
                                                                    <input type="text" placeholder="Awal SPPD" class="form-control" id="field-13" />
                                                                    <span class="input-group-addon bg-custom b-0 text-white">to</span>
                                                                    <input type="text" placeholder="Akhir SPPD" class="form-control" id="field-14" />
                                                                </div>
                                                            </div>
                                                        </div> 
                                                    </div>
				                                	<div class="row" style="margin-top:10px;">
				                                		<div class="col-md-2">
				                                		</div>
				                                		<div class="col-md-6">
				                                			<button type="button" class="btn btn-success" id="simpan"><i class="fa fa-disket"></i> Simpan</button>
				                                		</div>
				                                	</div>
				                                    <div class="clearfix"></div>
				                                </div>
				                            </div>
				                        </div>
				                    </div>

		                            <table id="datatable-buttons" class="table table-striped table-bordered">
		                                <thead>
		                                <tr>
                                            <th>Action</th>
		                                    <th>Nama</th>
		                                    <th>ST</th>
                                            <th>Dari</th>
		                                </tr>
		                                </thead><tbody></tbody>
		                            </table>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            <!-- end content -->
            	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                Konfirmasi Hapus
				            </div>
				            <div class="modal-body">
				                Anda yakin menghapus data?
				            </div>
				            <div class="modal-footer">
				                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				                <a class="btn btn-danger btn-ok">Delete</a>
				            </div>
				        </div>
				    </div>
				</div>
                </div>

                <!-- footer content -->

           
            </div>
            <!-- /page content -->

        

    

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="assets/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="assets/js/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="assets/js/bootstrap-progressbar.min.js"></script>
    <script src="assets/js/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="assets/js/icheck.min.js"></script>
    <script src="assets/js/custom.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="assets/js/moment.min2.js"></script>
    <script type="text/javascript" src="assets/js/daterangepicker.js"></script>
    <!-- input mask -->
    <script src="assets/js/jquery.inputmask.js"></script>
    <!-- knob -->
    <script src="assets/js/jquery.knob.min.js"></script>
    <!-- range slider -->
    <script src="assets/js/ion.rangeSlider.min.js"></script>
    <!-- color picker -->
    <script src="assets/js/bootstrap-colorpicker.js"></script>
    <script src="assets/js/docs.js"></script>
	<!-- select2 -->
        <script src="assets/js/select2.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="assets/js/parsley.min.js"></script>
    <!-- image cropping -->
    <script src="assets/js/cropper.min.js"></script>
    <script src="assets/js/main2.js"></script>

	<script src="assets/js/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="assets/js/countries.js"></script>
        <script src="assets/js/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.bootstrap.js"></script>

		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/jszip.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/pdfmake.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/vfs_fonts.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.html5.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.print.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedHeader.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.keyTable.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.responsive.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/responsive.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.scroller.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.colVis.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedColumns.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <!-- input_mask -->
    <!-- /input mask -->
    <script type="text/javascript">
    	$('.gol2').select2({ width: '100%' });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: "dd/mm/yyyy"
        });
    	$('#add').on('click',function(){
            $(this).addClass('hidden');
            $('.editroles').css('display','block');
            $('#batal').removeClass('hidden');
            $('.editrole').removeClass('hidden');
            $('#transaksi').val('tambah');
            $('#simpan').removeClass('hidden');
            bukatutup(false);
            clearForm();
        });
    	$('#batal').on('click',function(){
            bukatutup(false);
            $(this).addClass('hidden');
            $('#add').removeClass('hidden');
            $('.editrole').addClass('hidden');
        });
        function getTanggal(data){
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('index.php/Demo/get_pegawai2')?>",
                dataType : "JSON",
                data : {id:data},
                success: function(data){
                    $.each(data,function(){
                        $('#field-13').datepicker('setDate', new Date(data.dari));
                        $('#field-14').datepicker('setDate', new Date(data.ke));
                    });
                }
            });
        }
        function getTanggal2(data){
            $('#field-2').select2().empty();
            var html = '<option value="">Pilih Pegawai</option>';
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('index.php/Demo/get_pegawai3')?>",
                dataType : "JSON",
                data : {id:data},
                success: function(data){
                    /*html +='<option value="'+data.a1+'">'+data.b1+'</option>';*/
                    $.each(data,function(e){
                        html +='<option value="'+data[e].id+'">'+data[e].nama+'</option>';
                    });  
                    $('#field-2').append(html);
                    $('.gol2').select2({ width: '100%' });
                }
            });
        }
        /*$("#datatable-buttons").DataTable({
            dom: "Bfrtip",
                buttons: [{
                    extend: "copy",
                    className: "btn-sm"
                }, {
                    extend: "csv",
                    className: "btn-sm"
                }, {
                    extend: "excel",
                    className: "btn-sm"
                }, {
                    extend: "pdf",
                    className: "btn-sm"
                }, {
                    extend: "print",
                    className: "btn-sm"
                }]
        });*/
        var table = $("#datatable-buttons").DataTable({
	          processing: true,
	          serverSide: true,
	                  ajax: {"url": "<?php echo base_url().'index.php/Demo/get_pegawai_json'?>", "type": "POST"},
                            fixedColumns:   {
                                heightMatch: 'none'
                            },
	                        columns: [
	                            {"data": "view"},
	                            {"data": "nama", "width": "30%", "height":"10px",
                                "render": function(data, type, row) {
                                    var split = data.split('##');
                                    /*return '<a class="btn btn-info pull-left profile_thumb" href="Demo/'+split[0]+'">'+
                                                        '<i class="fa fa-hand-o-up"></i>'+
                                                    '</a>'+split[1];*/

                                    return '<ul class="list-unstyled scroll-view">'+
                                                    '<li class="media event">'+
                                                        '<a class="btn btn-info pull-left profile_thumb" href="Detail?id='+split[0]+'&kode='+split[2]+'">'+
                                                            '<i class="fa fa-hand-o-up"></i>'+
                                                        '</a>'+
                                                        '<div class="media-body">'+
                                                            '<a class="title" href="#">'+split[1]+'</a>'+
                                                        '</div>'+
                                                    '</li>'+
                                                '</ul>';
                                }},
                                {"data": "no"},
                                {"data": "dari"}
	                      ],
	                    order: [[2, 'asc']],
	            dom: "Blfrtip",
	            buttons: [{
	                extend: "copy",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3 ]
                    }
	            }, {
	                extend: "csv",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3 ]
                    }
	            }, {
	                extend: "excel",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3 ]
                    }
	            }, {
	                extend: "pdf",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3 ]
                    }
	            }, {
	                extend: "print",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3 ]
                    }
	            }]
	        });

				function clearForm(){
                    $('#transaksi').val('tambah');
                    $('#field-1').val('').trigger('change');
                    $('#field-2').val('').trigger('change');
                    $('#field-3').val('');
                    $('#field-4').val('').trigger('change');
                    $('#field-5').val('');
                    $('#field-6').val('');
                }
            $('#field-1').change(function(e) {
                var config = $(this).val();
                getTanggal(config);
                getTanggal2(config);
            });

                function bukatutup(status){
                    $('#field-1').prop("readonly", status);
                    $('#field-2').prop("readonly", status);
                    $('#field-3').prop("readonly", status);
                    $('#field-4').prop("readonly", status);
                    $('#field-5').prop("readonly", status);
                }
                //GET UPDATE
                $('#datatable-buttons').on('click','.lihat-row',function(){
                    var id=$(this).attr('data');
                    window.open("<?php echo base_url('surattugas/prints/"+id+"')?>");
                    /*$.ajax({
                        type : "GET",
                        url  : "<?php echo base_url('index.php/pegawai/get_pegawai')?>",
                        dataType : "JSON",
                        data : {id:id},
                        success: function(data){
                            $('.editmodal').removeClass('hidden');
                            $('.deletemodal').addClass('hidden');
                            $('.editaja').addClass('hidden');
                            $( "#add" ).trigger( "click" );
                            $('#simpan').addClass('hidden');
                            bukatutup(true);
                            $.each(data,function(){
                                $('#textkode').val(id);
                                $('#field-1').val(data.a1);
                                $('#field-2').val(data.b1);
                                $('#field-3').val(data.c1);
                                $('#field-4').val(data.d1).trigger('change');
                            });
                        }
                    });
                    return false;*/
                });
                //GET UPDATE
                var pegawai = '';
                $('#datatable-buttons').on('click','.edit-row',function(){
                    var id=$(this).attr('data');
                    $.ajax({
                        type : "GET",
                        url  : "<?php echo base_url('index.php/Demo/get_pegawai')?>",
                        dataType : "JSON",
                        data : {id:id},
                        success: function(data){
                            bukatutup(false);
                            $('.editaja').removeClass('hidden');
                            $( "#add" ).trigger( "click" );
                            $('#simpan').removeClass('hidden');
                            $('#transaksi').val('edit');
                            $.each(data,function(){
                                $('#textkode').val(id);
                                $('#field-1').val(data.b1).trigger('change');
                                $('#field-2').val(data.a1).trigger('change');
                                pegawai = data.a1;
                                $('#field-13').datepicker('setDate', new Date(data.c1));
                                $('#field-14').datepicker('setDate', new Date(data.d1));
                            });
                            $('.gol2').select2({ width: '100%' });
                        }
                    });
                    return false;
                });
                $('#datatable-buttons').on('click','.hapus-row',function(){
                    var id=$(this).attr('data');
                    $('#confirm-delete').modal('show');
                    $('.btn-ok').on('click',function(){
	                    $.ajax({
	                    type : "POST",
	                    url  : "<?php echo base_url('index.php/Demo/hapus_pegawai')?>",
	                    dataType : "JSON",
	                            data : {kode: id},
	                            success: function(data){
                    				$('#confirm-delete').modal('hide');
	                                table.ajax.reload();
                            		$( "#batal" ).trigger( "click" );
	                            }
	                        });
	                        return false;
	                });
                });

                $('#simpan').on('click',function(){
                    var a1=$('#field-1').val();
                    var b1=$('#field-2').val();
                    var c1=$('#field-3').val();
                    if(b1 == '' || b1 == null){
                        b1 = pegawai;
                    }
                    var d1=$('#field-4').val();
                    var kode=$('#textkode').val();
                    var transaksi=$('#transaksi').val();
                    if(transaksi == 'tambah'){
	                    $.ajax({
	                        type : "POST",
	                        contentType: false,
	                        processData: false,
	                        url  : "<?php echo base_url('index.php/Demo/simpan_pegawai')?>",
	                        data: function() {
	                            var data = new FormData();
	                            data.append("a1", $("#field-1").val());
	                            data.append("b1", b1);
	                            data.append("c1", $("#field-13").val());
	                            data.append("d1", $("#field-14").val());
	                            return data;
	                            // Or simply return new FormData(jQuery("form")[0]);
	                        }(),
	                        success: function(data){
	                            table.ajax.reload();
                            	$( "#batal" ).trigger( "click" );
	                        }
	                    });
	                    return false;
                    } else {
	                    $.ajax({
	                        type : "POST",
	                        contentType: false,
	                        processData: false,
	                        url  : "<?php echo base_url('index.php/Demo/update_pegawai')?>",
	                        data: function() {
	                            var data = new FormData();
	                            data.append("a1", $("#field-1").val());
	                            data.append("b1", b1);
	                            data.append("c1", $("#field-13").val());
	                            data.append("d1", $("#field-14").val());
	                            data.append("kode", kode);
	                            return data;
	                            // Or simply return new FormData(jQuery("form")[0]);
	                        }(),
	                        success: function(data){
	                            table.ajax.reload();
                            	$( "#batal" ).trigger( "click" );
	                        }
	                    });
	                    return false;
                    }
                });
    </script>
	
	<script type="text/javascript" src="assets/js/jquery.smartWizard.js"></script>
</body>
</html>