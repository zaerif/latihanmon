<?php $this->load->view('layouts/admin'); ?>
            <!-- top navigation -->
            <div class="top_nav navbar-fixed-top">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="assets/images/users.png" alt=""><?php echo $this->session->userdata('username'); ?>                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    
                                    <li><a href="Welcome/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
                <div class="right_col" role="main">           


                	<div class="clearfix"></div>

                    <div class="row headofhtml">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><i class="fa fa-dashboard"></i> Master Harga <small>Sistem Informasi Surat Perjalanan Dinas</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                	<div class="col-md-2 col-sm-2 col-xs-12">
										<button type="button" class="btn btn-success" id="add"><i class="fa fa-plus-circle"></i> Tambah Daerah</button>
										<button type="button" class="btn btn-danger hidden" id="batal"><i class="fa fa-close"></i> Batal</button>
									</div>
									<div class="row editrole hidden">
				                        <div class="col-md-12">
				                            <div class="x_panel">
				                                <div class="x_title">
				                                    <h2 id="titleedit"><i class="fa fa-dashboard"></i> Tambah Daerah <small></small></h2>
				                                    <ul class="nav navbar-right panel_toolbox">
				                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				                                        </li>
				                                    </ul>
				                                    <div class="clearfix"></div>
				                                </div>
				                                <div class="x_content editroles">
				                                	<div class="row">
					                                    <div class="form-group"> 
					                                        <label for="field-1" class="control-label col-md-2" style="text-align:right;">Nama</label> 
					                                        <div class="col-md-6">
					                                        	<input type="text" id="transaksi" value="tambah" data-a-sign="" class="form-control hidden">
					                                        	<input type="text" id="textkode" value="" data-a-sign="" class="form-control hidden">
					                                        	<input type="text" id="field-1" placeholder="Nama" value="" data-a-sign="" class="form-control">
					                                        </div>
					                                    </div> 
					                                </div>
				                                	<div class="row" style="margin-top:10px;">
					                                    <div class="form-group"> 
					                                        <label for="field-2" class="control-label col-md-2" style="text-align:right;">IV Eselon</label> 
					                                        <div class="col-md-6">
                                                                <input type="text" id="field-2" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
					                                        </div>
					                                    </div> 
					                                </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">IV Karo</label> 
                                                            <div class="col-md-6">
                                                                <input type="text" id="field-3" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">IV Kabag</label> 
                                                            <div class="col-md-6">
                                                                <input type="text" id="field-4" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">III</label> 
                                                            <div class="col-md-6">
                                                                <input type="text" id="field-5" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">II</label> 
                                                            <div class="col-md-6">
                                                                <input type="text" id="field-6" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">Bandara</label> 
                                                            <div class="col-md-6">
                                                                <input type="text" id="field-7" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">Local</label> 
                                                            <div class="col-md-6">
                                                                <input type="text" id="field-8" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">Representasi I</label> 
                                                            <div class="col-md-6">
                                                                <input type="text" id="field-9" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">Representasi II</label> 
                                                            <div class="col-md-6">
                                                                <input type="text" id="field-10" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div>
                                                        </div> 
                                                    </div>
				                                	<div class="row" style="margin-top:10px;">
				                                		<div class="col-md-2">
				                                		</div>
				                                		<div class="col-md-6">
				                                			<button type="button" class="btn btn-success" id="simpan"><i class="fa fa-disket"></i> Simpan</button>
				                                		</div>
				                                	</div>
				                                    <div class="clearfix"></div>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
                                    <div class="table table-responsive">
                                        <table id="datatable-buttons" class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Nama</th>
                                                <th>Harian</th>
                                                <th>IV Eselon</th>
                                                <th>IV Karo</th>
                                                <th>IV Kabag</th>
                                                <th>III</th>
                                                <th>II</th>
                                                <th>Bandara</th>
                                                <th>Local</th>
                                                <th>Res I</th>
                                                <th>Res II</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            <!-- end content -->
            	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                Konfirmasi Hapus
				            </div>
				            <div class="modal-body">
				                Anda yakin menghapus data?
				            </div>
				            <div class="modal-footer">
				                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				                <a class="btn btn-danger btn-ok">Delete</a>
				            </div>
				        </div>
				    </div>
				</div>
                </div>

                <!-- footer content -->

           
            </div>
            <!-- /page content -->

        

    

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="assets/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="assets/js/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="assets/js/bootstrap-progressbar.min.js"></script>
    <script src="assets/js/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="assets/js/icheck.min.js"></script>
    <script src="assets/js/custom.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="assets/js/moment.min2.js"></script>
    <script type="text/javascript" src="assets/js/daterangepicker.js"></script>
    <!-- input mask -->
    <script src="assets/js/jquery.inputmask.js"></script>
    <!-- knob -->
    <script src="assets/js/jquery.knob.min.js"></script>
    <!-- range slider -->
    <script src="assets/js/ion.rangeSlider.min.js"></script>
    <!-- color picker -->
    <script src="assets/js/bootstrap-colorpicker.js"></script>
    <script src="assets/js/docs.js"></script>
	<!-- select2 -->
        <script src="assets/js/select2.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="assets/js/parsley.min.js"></script>
    <!-- image cropping -->
    <script src="assets/js/cropper.min.js"></script>
    <script src="assets/js/main2.js"></script>

	<script src="assets/js/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="assets/js/countries.js"></script>
        <script src="assets/js/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.bootstrap.js"></script>

		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/jszip.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/pdfmake.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/vfs_fonts.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.html5.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.print.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedHeader.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.keyTable.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.responsive.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/responsive.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.scroller.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.colVis.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedColumns.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/autoNumeric/autoNumeric.js" type="text/javascript"></script>
        <style type="text/css">
</style>
    <!-- input_mask -->
    <!-- /input mask -->
    <script type="text/javascript">
    	$('.gol2').select2({ width: '100%' });
        $('.autonumber').autoNumeric('init');
    	$('#add').on('click',function(){
            $(this).addClass('hidden');
            $('.editroles').css('display','block');
            $('#batal').removeClass('hidden');
            $('.editrole').removeClass('hidden');
            $('#transaksi').val('tambah');
            $('#simpan').removeClass('hidden');
            bukatutup(false);
            clearForm();
        });
    	$('#batal').on('click',function(){
            bukatutup(false);
            $(this).addClass('hidden');
            $('#add').removeClass('hidden');
            $('.editrole').addClass('hidden');
        });
        var table = $("#datatable-buttons").DataTable({
	          processing: true,
	          serverSide: true,
            lengthMenu: [[25, 100, -1], [25, 100, "All"]],
            pageLength: 25,
	                  ajax: {"url": "<?php echo base_url().'index.php/master/get_master_json'?>", "type": "POST"},
	                        columns: [
	                            {"data": "view"},
	                            {"data": "nama"},
                                {"data": "harian", render: $.fn.dataTable.render.number(',', '.', ''), className: "text-right"},
                                {"data": "iveselon", render: $.fn.dataTable.render.number(',', '.', ''), className: "text-right"},
                                {"data": "ivkaro", render: $.fn.dataTable.render.number(',', '.', ''), className: "text-right"},
                                {"data": "ivkabag", render: $.fn.dataTable.render.number(',', '.', ''), className: "text-right"},
                                {"data": "iii", render: $.fn.dataTable.render.number(',', '.', ''), className: "text-right"},
                                {"data": "ii", render: $.fn.dataTable.render.number(',', '.', ''), className: "text-right"},
                                {"data": "bandara", render: $.fn.dataTable.render.number(',', '.', ''), className: "text-right"},
                                {"data": "local", render: $.fn.dataTable.render.number(',', '.', ''), className: "text-right"},
                                {"data": "res1", render: $.fn.dataTable.render.number(',', '.', ''), className: "text-right"},
                                {"data": "res2", render: $.fn.dataTable.render.number(',', '.', ''), className: "text-right"}
	                      ],
	                    order: [[1, 'asc']],
	            sDom: "Blfrtip",
	            buttons: [{
	                extend: "copy",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                    }
	            }, {
	                extend: "csv",
	                className: "btn-sm"
	            }, {
	                extend: "excel",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                    }
	            }, {
	                extend: "pdf",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                    }
	            }, {
	                extend: "print",
	                className: "btn-sm",
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '10pt' )
                            .prepend(
                                '<img src="http://localhost:8888/adrian/kpu2/assets/images/logo_perhunungan.png" style="position:absolute; top:30%; left:30%; height:350px;width:350px;background-size: cover;opacity: 0.1;" />'
                            );
     
                        $(win.document.body).find( 'table' )
                            .addClass( 'compact' )
                            .css( 'font-size', 'inherit' );
                    },
                    exportOptions: {
                        columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                    }
	            }]
	        });

				function clearForm(){
                    $('#transaksi').val('tambah');
                    $('#field-1').val('');
                    $('#field-2').autoNumeric('set',0);
                    $('#field-3').autoNumeric('set',0);
                    $('#field-4').autoNumeric('set',0);
                    $('#field-5').autoNumeric('set',0);
                    $('#field-6').autoNumeric('set',0);
                    $('#field-7').autoNumeric('set',0);
                    $('#field-8').autoNumeric('set',0);
                    $('#field-9').autoNumeric('set',0);
                    $('#field-10').autoNumeric('set',0);
                }

                function bukatutup(status){
                    $('#field-1').prop("readonly", status);
                    $('#field-2').prop("readonly", status);
                    $('#field-3').prop("readonly", status);
                    $('#field-4').prop("readonly", status);
                    $('#field-5').prop("readonly", status);
                    $('#field-6').prop("readonly", status);
                    $('#field-7').prop("readonly", status);
                    $('#field-8').prop("readonly", status);
                    $('#field-9').prop("readonly", status);
                    $('#field-10').prop("readonly", status);
                }
                //GET UPDATE
                $('#datatable-buttons').on('click','.lihat-row',function(){
                    var id=$(this).attr('data');
                    $.ajax({
                        type : "GET",
                        url  : "<?php echo base_url('index.php/master/get_master')?>",
                        dataType : "JSON",
                        data : {id:id},
                        success: function(data){
                            $('.editmodal').removeClass('hidden');
                            $('.deletemodal').addClass('hidden');
                            $('.editaja').addClass('hidden');
                            $( "#add" ).trigger( "click" );
                            $('#simpan').addClass('hidden');
                            bukatutup(true);
                            $.each(data,function(){
                                $('#textkode').val(id);
                                $('#field-1').val(data.a1);
                                $('#field-2').autoNumeric('set',data.b1);
                                $('#field-3').autoNumeric('set',data.c1);
                                $('#field-4').autoNumeric('set',data.d1);
                                $('#field-5').autoNumeric('set',data.e1);
                                $('#field-6').autoNumeric('set',data.f1);
                                $('#field-7').autoNumeric('set',data.g1);
                                $('#field-8').autoNumeric('set',data.h1);
                                $('#field-9').autoNumeric('set',data.i1);
                                $('#field-10').autoNumeric('set',data.j1);
                            });
                        }
                    });
                    return false;
                });
                //GET UPDATE
                $('#datatable-buttons').on('click','.edit-row',function(){
                    var id=$(this).attr('data');
                    $.ajax({
                        type : "GET",
                        url  : "<?php echo base_url('index.php/master/get_master')?>",
                        dataType : "JSON",
                        data : {id:id},
                        success: function(data){
                            bukatutup(false);
                            $('.editaja').removeClass('hidden');
                            $( "#add" ).trigger( "click" );
                            $('#simpan').removeClass('hidden');
                            $('#transaksi').val('edit');
                            $.each(data,function(){
                                $('#textkode').val(id);
                                $('#field-1').val(data.a1);
                                $('#field-2').autoNumeric('set',data.b1);
                                $('#field-3').autoNumeric('set',data.c1);
                                $('#field-4').autoNumeric('set',data.d1);
                                $('#field-5').autoNumeric('set',data.e1);
                                $('#field-6').autoNumeric('set',data.f1);
                                $('#field-7').autoNumeric('set',data.g1);
                                $('#field-8').autoNumeric('set',data.h1);
                                $('#field-9').autoNumeric('set',data.i1);
                                $('#field-10').autoNumeric('set',data.j1);
                            });
                        }
                    });
                    return false;
                });
                $('#datatable-buttons').on('click','.hapus-row',function(){
                    var id=$(this).attr('data');
                    $('#confirm-delete').modal('show');
                    $('.btn-ok').on('click',function(){
	                    $.ajax({
	                    type : "POST",
	                    url  : "<?php echo base_url('index.php/master/hapus_master')?>",
	                    dataType : "JSON",
	                            data : {kode: id},
	                            success: function(data){
                    				$('#confirm-delete').modal('hide');
	                                table.ajax.reload();
                            		$( "#batal" ).trigger( "click" );
	                            }
	                        });
	                        return false;
	                });
                });

                $('#simpan').on('click',function(){
                    var a1=$('#field-1').val();
                    var b1=$('#field-2').val();
                    var c1=$('#field-3').val();
                    var d1=$('#field-4').val();
                    var kode=$('#textkode').val();
                    var transaksi=$('#transaksi').val();
                    if(transaksi == 'tambah'){
	                    $.ajax({
	                        type : "POST",
	                        contentType: false,
	                        processData: false,
	                        url  : "<?php echo base_url('index.php/master/simpan_master')?>",
	                        data: function() {
	                            var data = new FormData();
	                            data.append("a1", $("#field-1").val());
	                            data.append("b1", $("#field-2").val());
	                            data.append("c1", $("#field-3").val());
	                            data.append("d1", $("#field-4").val());
                                data.append("e1", $("#field-5").val());
                                data.append("f1", $("#field-6").val());
                                data.append("g1", $("#field-7").val());
                                data.append("h1", $("#field-8").val());
                                data.append("i1", $("#field-9").val());
                                data.append("j1", $("#field-10").val());
	                            return data;
	                            // Or simply return new FormData(jQuery("form")[0]);
	                        }(),
	                        success: function(data){
	                            table.ajax.reload();
                            	$( "#batal" ).trigger( "click" );
	                        }
	                    });
	                    return false;
                    } else {
	                    $.ajax({
	                        type : "POST",
	                        contentType: false,
	                        processData: false,
	                        url  : "<?php echo base_url('index.php/master/update_master')?>",
	                        data: function() {
	                            var data = new FormData();
	                            data.append("a1", $("#field-1").val());
                                data.append("b1", $("#field-2").val());
                                data.append("c1", $("#field-3").val());
                                data.append("d1", $("#field-4").val());
                                data.append("e1", $("#field-5").val());
                                data.append("f1", $("#field-6").val());
                                data.append("g1", $("#field-7").val());
                                data.append("h1", $("#field-8").val());
                                data.append("i1", $("#field-9").val());
                                data.append("j1", $("#field-10").val());
	                            data.append("kode", kode);
	                            return data;
	                            // Or simply return new FormData(jQuery("form")[0]);
	                        }(),
	                        success: function(data){
	                            table.ajax.reload();
                            	$( "#batal" ).trigger( "click" );
	                        }
	                    });
	                    return false;
                    }
                });
    </script>
	
	<script type="text/javascript" src="assets/js/jquery.smartWizard.js"></script>
</body>
</html>