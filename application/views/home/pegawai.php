<?php $this->load->view('layouts/admin'); ?>
            <!-- top navigation -->
            <div class="top_nav navbar-fixed-top">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="assets/images/users.png" alt=""><?php echo $this->session->userdata('username'); ?>                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    
                                    <li><a href="Welcome/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>
						</ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
                <div class="right_col" role="main">           


                	<div class="clearfix"></div>

                    <div class="row headofhtml">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><i class="fa fa-dashboard"></i> Pegawai KPU <small>Sistem Informasi Surat Perjalanan Dinas</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                	<div class="col-md-2 col-sm-2 col-xs-12">
										<button type="button" class="btn btn-success" id="add"><i class="fa fa-plus-circle"></i> Tambah Pegawai</button>
										<button type="button" class="btn btn-danger hidden" id="batal"><i class="fa fa-close"></i> Batal</button>
									</div>
									<div class="row editrole hidden">
				                        <div class="col-md-12">
				                            <div class="x_panel">
				                                <div class="x_title">
				                                    <h2 id="titleedit"><i class="fa fa-dashboard"></i> Tambah Pegawai <small></small></h2>
				                                    <ul class="nav navbar-right panel_toolbox">
				                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				                                        </li>
				                                    </ul>
				                                    <div class="clearfix"></div>
				                                </div>
				                                <div class="x_content editroles">
				                                	<div class="row">
					                                    <div class="form-group"> 
					                                        <label for="field-1" class="control-label col-md-2" style="text-align:right;">Nama</label> 
					                                        <div class="col-md-6">
					                                        	<input type="text" id="transaksi" value="tambah" data-a-sign="" class="form-control hidden">
					                                        	<input type="text" id="textkode" value="" data-a-sign="" class="form-control hidden">
					                                        	<input type="text" id="field-1" placeholder="Nama" value="" data-a-sign="" class="form-control">
					                                        </div>
					                                    </div> 
					                                </div>
				                                	<div class="row" style="margin-top:10px;">
					                                    <div class="form-group"> 
					                                        <label for="field-2" class="control-label col-md-2" style="text-align:right;">NIP</label> 
					                                        <div class="col-md-6">
					                                        	<input type="text" id="field-2" placeholder="NIP" value="" data-a-sign="" class="form-control">
					                                        </div>
					                                    </div> 
					                                </div>
				                                	<div class="row" style="margin-top:10px;">
					                                    <div class="form-group"> 
					                                        <label for="field-3" class="control-label col-md-2" style="text-align:right;">Jabatan</label> 
					                                        <div class="col-md-6">
					                                        	<input type="text" id="field-3" placeholder="Jabatan" value="" data-a-sign="" class="form-control">
					                                        </div>
					                                    </div> 
					                                </div>
				                                	<div class="row" style="margin-top:10px;">
					                                    <div class="form-group"> 
					                                        <label for="field-4" class="control-label col-md-2" style="text-align:right;">Golongan</label> 
					                                        <div class="col-md-6">
					                                        	<select class="gol2" id="field-4">
					                                        		<option value="">Pilih Golongan</option>
					                                        		<option value="IVeselon">IV (Eselon I)</option>
					                                        		<option value="IVkaro">IV (Karo)</option>
					                                        		<option value="IVkabag">IV (Kabag)</option>
					                                        		<option value="III">III</option>
					                                        		<option value="II">II</option>
					                                        	</select>
					                                        </div>
					                                    </div> 
				                                	</div>
				                                	<div class="row" style="margin-top:10px;">
				                                		<div class="col-md-2">
				                                		</div>
				                                		<div class="col-md-6">
				                                			<button type="button" class="btn btn-success" id="simpan"><i class="fa fa-disket"></i> Simpan</button>
				                                		</div>
				                                	</div>
				                                    <div class="clearfix"></div>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
                                    <div class="clearfix"></div>
                                    <div class="table-responsive">
                                        <table id="datatable-buttons" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th data-priority="1">Nama</th>
                                                <th>NIP</th>
                                                <th data-priority="2">Jabatan</th>
                                                <th>Golongan</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            <!-- end content -->
            	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                Konfirmasi Hapus
				            </div>
				            <div class="modal-body">
				                Anda yakin menghapus data?
				            </div>
				            <div class="modal-footer">
				                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				                <a class="btn btn-danger btn-ok">Delete</a>
				            </div>
				        </div>
				    </div>
				</div>
                </div>

                <!-- footer content -->

           
            </div>
            <!-- /page content -->

        

    

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="assets/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="assets/js/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="assets/js/bootstrap-progressbar.min.js"></script>
    <script src="assets/js/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="assets/js/icheck.min.js"></script>
    <script src="assets/js/custom.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="assets/js/moment.min2.js"></script>
    <script type="text/javascript" src="assets/js/daterangepicker.js"></script>
    <!-- input mask -->
    <script src="assets/js/jquery.inputmask.js"></script>
    <!-- knob -->
    <script src="assets/js/jquery.knob.min.js"></script>
    <!-- range slider -->
    <script src="assets/js/ion.rangeSlider.min.js"></script>
    <!-- color picker -->
    <script src="assets/js/bootstrap-colorpicker.js"></script>
    <script src="assets/js/docs.js"></script>
	<!-- select2 -->
        <script src="assets/js/select2.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="assets/js/parsley.min.js"></script>
    <!-- image cropping -->
    <script src="assets/js/cropper.min.js"></script>
    <script src="assets/js/main2.js"></script>

	<script src="assets/js/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="assets/js/countries.js"></script>
        <script src="assets/js/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.bootstrap.js"></script>

		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/jszip.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/pdfmake.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/vfs_fonts.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.html5.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.print.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedHeader.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.keyTable.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.responsive.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/responsive.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.scroller.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.colVis.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedColumns.min.js"></script>
    <!-- input_mask -->
    <!-- /input mask -->
    <script type="text/javascript">
    	$('.gol2').select2({ width: '100%' });
    	$('#add').on('click',function(){
            $(this).addClass('hidden');
            $('.editroles').css('display','block');
            $('#batal').removeClass('hidden');
            $('.editrole').removeClass('hidden');
            $('#transaksi').val('tambah');
            $('#simpan').removeClass('hidden');
            bukatutup(false);
            clearForm();
        });
    	$('#batal').on('click',function(){
            bukatutup(false);
            $(this).addClass('hidden');
            $('#add').removeClass('hidden');
            $('.editrole').addClass('hidden');
        });
        var table = $("#datatable-buttons").DataTable({
	          processing: true,
	          serverSide: true,
              responsive: true,
	                  ajax: {"url": "<?php echo base_url().'index.php/pegawai/get_pegawai_json'?>", "type": "POST"},
	                        columns: [
	                            {"data": "view"},
	                            {"data": "nama"},
                                {"data": "nip"},
                                {"data": "jabatan"},
                                {"data": "golongan"}
	                      ],
	                    order: [[2, 'asc']],
	            dom: "Blfrtip",
	            buttons: [{
	                extend: "copy",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3, 4 ]
                    }
	            }, {
	                extend: "csv",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3, 4 ]
                    }
	            }, {
	                extend: "excel",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3, 4 ]
                    }
	            }, {
	                extend: "pdf",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3, 4 ]
                    }
	            }, {
	                extend: "print",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3, 4 ]
                    }
	            }]
	        });
            new $.fn.dataTable.FixedHeader( table );

				function clearForm(){
                    $('#transaksi').val('tambah');
                    $('#field-1').val('');
                    $('#field-2').val('');
                    $('#field-3').val('');
                    $('#field-4').val('').trigger('change');
                    $('#field-5').val('');
                    $('#field-6').val('');
                }

                function bukatutup(status){
                    $('#field-1').prop("readonly", status);
                    $('#field-2').prop("readonly", status);
                    $('#field-3').prop("readonly", status);
                    $('#field-4').prop("readonly", status);
                    $('#field-5').prop("readonly", status);
                }
                //GET UPDATE
                $('#datatable-buttons').on('click','.lihat-row',function(){
                    var id=$(this).attr('data');
                    $.ajax({
                        type : "GET",
                        url  : "<?php echo base_url('index.php/pegawai/get_pegawai')?>",
                        dataType : "JSON",
                        data : {id:id},
                        success: function(data){
                            $('.editmodal').removeClass('hidden');
                            $('.deletemodal').addClass('hidden');
                            $('.editaja').addClass('hidden');
                            $( "#add" ).trigger( "click" );
                            $('#simpan').addClass('hidden');
                            bukatutup(true);
                            $.each(data,function(){
                                $('#textkode').val(id);
                                $('#field-1').val(data.a1);
                                $('#field-2').val(data.b1);
                                $('#field-3').val(data.c1);
                                $('#field-4').val(data.d1).trigger('change');
                            });
                        }
                    });
                    return false;
                });
                //GET UPDATE
                $('#datatable-buttons').on('click','.edit-row',function(){
                    var id=$(this).attr('data');
                    $.ajax({
                        type : "GET",
                        url  : "<?php echo base_url('index.php/pegawai/get_pegawai')?>",
                        dataType : "JSON",
                        data : {id:id},
                        success: function(data){
                            bukatutup(false);
                            $('.editaja').removeClass('hidden');
                            $( "#add" ).trigger( "click" );
                            $('#simpan').removeClass('hidden');
                            $('#transaksi').val('edit');
                            $.each(data,function(){
                                $('#textkode').val(id);
                                $('#field-1').val(data.a1);
                                $('#field-2').val(data.b1);
                                $('#field-3').val(data.c1);
                                $('#field-4').val(data.d1).trigger('change');
                            });
                        }
                    });
                    return false;
                });
                $('#datatable-buttons').on('click','.hapus-row',function(){
                    var id=$(this).attr('data');
                    $('#confirm-delete').modal('show');
                    $('.btn-ok').on('click',function(){
	                    $.ajax({
	                    type : "POST",
	                    url  : "<?php echo base_url('index.php/pegawai/hapus_pegawai')?>",
	                    dataType : "JSON",
	                            data : {kode: id},
	                            success: function(data){
                    				$('#confirm-delete').modal('hide');
	                                table.ajax.reload();
                            		$( "#batal" ).trigger( "click" );
	                            }
	                        });
	                        return false;
	                });
                });

                $('#simpan').on('click',function(){
                    var a1=$('#field-1').val();
                    var b1=$('#field-2').val();
                    var c1=$('#field-3').val();
                    var d1=$('#field-4').val();
                    var kode=$('#textkode').val();
                    var transaksi=$('#transaksi').val();
                    if(transaksi == 'tambah'){
	                    $.ajax({
	                        type : "POST",
	                        contentType: false,
	                        processData: false,
	                        url  : "<?php echo base_url('index.php/pegawai/simpan_pegawai')?>",
	                        data: function() {
	                            var data = new FormData();
	                            data.append("a1", $("#field-1").val());
	                            data.append("b1", $("#field-2").val());
	                            data.append("c1", $("#field-3").val());
	                            data.append("d1", $("#field-4").val());
	                            return data;
	                            // Or simply return new FormData(jQuery("form")[0]);
	                        }(),
	                        success: function(data){
	                            table.ajax.reload();
                            	$( "#batal" ).trigger( "click" );
	                        }
	                    });
	                    return false;
                    } else {
	                    $.ajax({
	                        type : "POST",
	                        contentType: false,
	                        processData: false,
	                        url  : "<?php echo base_url('index.php/pegawai/update_pegawai')?>",
	                        data: function() {
	                            var data = new FormData();
	                            data.append("a1", $("#field-1").val());
	                            data.append("b1", $("#field-2").val());
	                            data.append("c1", $("#field-3").val());
	                            data.append("d1", $("#field-4").val());
	                            data.append("kode", kode);
	                            return data;
	                            // Or simply return new FormData(jQuery("form")[0]);
	                        }(),
	                        success: function(data){
	                            table.ajax.reload();
                            	$( "#batal" ).trigger( "click" );
	                        }
	                    });
	                    return false;
                    }
                });
    </script>
	
	<script type="text/javascript" src="assets/js/jquery.smartWizard.js"></script>
</body>
</html>