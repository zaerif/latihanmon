<?php $this->load->view('layouts/admin'); ?>
            <!-- top navigation -->
            <div class="top_nav navbar-fixed-top">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="assets/images/users.png" alt=""><?php echo $this->session->userdata('username'); ?>                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    
                                    <li><a href="Welcome/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
                <div class="right_col" role="main">           


                	<div class="clearfix"></div>

                    <div class="row headofhtml">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><i class="fa fa-dashboard"></i> Pembayaran <small>Sistem Informasi Surat Perjalanan Dinas</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                	<div class="col-md-2 col-sm-2 col-xs-12">
                                        <button type="button" class="btn btn-success" id="settingadd"><i class="fa fa-gears"></i> Setting Penyetuju</button>
										<button type="button" class="btn btn-success hidden" id="add"><i class="fa fa-plus-circle"></i> Tambah Surat Tugas</button>
										<button type="button" class="btn btn-danger hidden" id="batal"><i class="fa fa-close"></i> Batal</button>
									</div>
									<div class="row editrole hidden">
				                        <div class="col-md-12">
				                            <div class="x_panel">
				                                <div class="x_title">
				                                    <h2 id="titleedit"><i class="fa fa-dashboard"></i> Edit Pembayaran <small></small></h2>
				                                    <ul class="nav navbar-right panel_toolbox">
				                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				                                        </li>
				                                    </ul>
				                                    <div class="clearfix"></div>
				                                </div>
				                                <div class="x_content editroles">
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Nomor</label> 
                                                            <div class="col-md-6">
                                                                <input type="text" id="transaksi" value="tambah" data-a-sign="" class="form-control hidden">
                                                                <input type="text" id="textkode" value="" data-a-sign="" class="form-control hidden">
                                                                <input type="text" id="field-1" placeholder="Nomor" value="" data-a-sign="" class="form-control" disabled>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Kepada</label> 
                                                            <div class="col-md-3">
                                                                <input type="text" id="field-0" placeholder="Nama" value="" data-a-sign="" class="form-control autonumber" style="text-align:left;">
                                                            </div> 
                                                            <div class="col-md-3">
                                                                <p id="namainfo" class="text-right">2 x 360000</p>
                                                            </div> 
                                                        </div> 
                                                    </div>
                                                    <div class="row hidden" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Kwitansi</label> 
                                                            <div class="col-md-3">
                                                            </div> 
                                                            <div class="col-md-3">
                                                                <input type="text" id="field-2" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div> 
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Harian</label> 
                                                            <div class="col-md-3">
                                                                <p id="harianinfo" class="text-right">2 x 360000</p>
                                                            </div> 
                                                            <div class="col-md-3">
                                                                <input type="text" id="field-3" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;" disabled>
                                                            </div> 
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Tiket/Transport</label> 
                                                            <div class="col-md-3">
                                                                <select class="form-control gol2" id="filter1">
                                                                    <option value="">None</option>
                                                                    <option value="1">Kelengkapan</option>
                                                                </select>
                                                            </div> 
                                                            <div class="col-md-3">
                                                                <input type="text" id="field-4" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div> 
                                                        </div> 
                                                    </div>
                                                    <div class="row hidden" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Transport</label> 
                                                            <div class="col-md-3">
                                                                <select class="form-control gol2" id="filter4">
                                                                    <option value="">None</option>
                                                                    <option value="1">Kelengkapan</option>
                                                                </select>
                                                            </div> 
                                                            <div class="col-md-3">
                                                                <input type="text" id="field-7" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div> 
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Penginapan</label> 
                                                            <div class="col-md-3">
                                                                <select class="form-control gol2" id="filter2">
                                                                    <option value="">None</option>
                                                                    <option value="1">Kelengkapan</option>
                                                                    <option value="2">Master</option>
                                                                </select>
                                                            </div> 
                                                            <div class="col-md-3">
                                                                <input type="text" id="field-5" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div> 
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Representasi </label> 
                                                            <div class="col-md-3">
                                                                <select class="form-control gol2" id="filter3">
                                                                    <option value="">None</option>
                                                                    <option value="1">Representasi I</option>
                                                                    <option value="2">Representasi II</option>
                                                                </select>
                                                            </div> 
                                                            <div class="col-md-3">
                                                                <input type="text" id="field-6" placeholder="Nominal" value="" data-a-sign="" class="form-control autonumber" style="text-align:right;">
                                                            </div> 
                                                        </div> 
                                                    </div>
				                                	<div class="row" style="margin-top:10px;">
				                                		<div class="col-md-2">
				                                		</div>
				                                		<div class="col-md-6">
                                                            <button type="button" class="btn btn-danger" id="cancel"><i class="fa fa-disket"></i> Batal</button>
				                                			<button type="button" class="btn btn-primary" id="simpan"><i class="fa fa-print"></i> Print</button>
				                                		</div>
				                                	</div>
				                                    <div class="clearfix"></div>
				                                </div>
				                            </div>
				                        </div>
				                    </div>

		                            <table id="datatable-buttons" class="table table-striped table-bordered">
		                                <thead>
		                                <tr>
		                                    <th>Action</th>
		                                    <th>No</th>
                                            <th>Nama</th>
                                            <th>Lama SPPD</th>
		                                    <th>Daerah</th>
		                                </tr>
		                                </thead>
		                                <tbody>
		                                </tbody>
		                            </table>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            <!-- end content -->
            	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                Setting Penyetuju
				            </div>
				            <div class="modal-body">
				                <div class="row" style="margin-top:10px;">
                                    <div class="form-group"> 
                                        <label for="field-4" class="control-label col-md-2" style="text-align:right;">Penyetuju</label> 
                                        <div class="col-md-6">
                                            <select class="gol2" id="pfield-1">
                                                <option value="">Pilih Pegawai</option>
                                                <?php
                                                    foreach($mst_pegawai->result_array() as $me)
                                                    {
                                                        if($me['id'] == 5){
                                                            ?> <option value="<?php echo $me['id']; ?>"  selected="selected"><?php echo $me['nama']; ?></option>
                                                            <?php
                                                        } else {
                                                        
                                                ?>
                                                    <option value="<?php echo $me['id']; ?>"><?php echo $me['nama']; ?></option>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div> 
                                </div> 
                                <div class="row" style="margin-top:10px;">
                                    <div class="form-group"> 
                                        <label for="field-4" class="control-label col-md-2" style="text-align:right;">Bendahara</label> 
                                        <div class="col-md-6">
                                            <select class="gol2" id="pfield-2">
                                                <option value="">Pilih Pegawai</option>
                                                <?php
                                                    foreach($mst_pegawai->result_array() as $me)
                                                    {
                                                        if($me['id'] == 6){
                                                            ?> <option value="<?php echo $me['id']; ?>"  selected="selected"><?php echo $me['nama']; ?></option>
                                                            <?php
                                                        } else {
                                                ?>
                                                    <option value="<?php echo $me['id']; ?>"><?php echo $me['nama']; ?></option>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div> 
                                </div>
				            </div>
				        </div>
				    </div>
				</div>
                </div>

                <!-- footer content -->

           
            </div>
            <!-- /page content -->

        

    

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="assets/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="assets/js/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="assets/js/bootstrap-progressbar.min.js"></script>
    <script src="assets/js/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="assets/js/icheck.min.js"></script>
    <script src="assets/js/custom.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="assets/js/moment.min2.js"></script>
    <script type="text/javascript" src="assets/js/daterangepicker.js"></script>
    <!-- input mask -->
    <script src="assets/js/jquery.inputmask.js"></script>
    <!-- knob -->
    <script src="assets/js/jquery.knob.min.js"></script>
    <!-- range slider -->
    <script src="assets/js/ion.rangeSlider.min.js"></script>
    <!-- color picker -->
    <script src="assets/js/bootstrap-colorpicker.js"></script>
    <script src="assets/js/docs.js"></script>
	<!-- select2 -->
        <script src="assets/js/select2.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="assets/js/parsley.min.js"></script>
    <!-- image cropping -->
    <script src="assets/js/cropper.min.js"></script>
    <script src="assets/js/main2.js"></script>

	<script src="assets/js/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="assets/js/countries.js"></script>
        <script src="assets/js/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.bootstrap.js"></script>

		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/jszip.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/pdfmake.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/vfs_fonts.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.html5.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.print.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedHeader.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.keyTable.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.responsive.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/responsive.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.scroller.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.colVis.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedColumns.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/autoNumeric/autoNumeric.js" type="text/javascript"></script>
    <!-- input_mask -->
    <!-- /input mask -->
    <script type="text/javascript">
    	$('.gol2').select2({ width: '100%' });
        $('.autonumber').autoNumeric('init');
    	$('#add').on('click',function(){
            $(this).addClass('hidden');
            $('.editroles').css('display','block');
            $('.editrole').removeClass('hidden');
            $('#transaksi').val('tambah');
            $('#simpan').removeClass('hidden');
            bukatutup(false);
            clearForm();
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: "dd/mm/yyyy"
        });
    	$('#batal').on('click',function(){
            bukatutup(false);
            $('.editrole').addClass('hidden');
        });
        $('#settingadd').on('click',function(){
            $('#confirm-delete').modal('show');
        });
        $('#cancel').on('click',function(){
            bukatutup(false);
            $('.editrole').addClass('hidden');
        });
        var table = $("#datatable-buttons").DataTable({
	          processing: true,
	          serverSide: true,
	                  ajax: {"url": "<?php echo base_url().'index.php/pembayaran/get_surat_json'?>", "type": "POST"},
	                        columns: [
	                            {"data": "view"},
	                            {"data": "no"},
                                {"data": "nama"},
                                {"data": "lama"},
                                {"data": "daerah"}
	                      ],
	                    order: [[2, 'asc']],
	            dom: "Blfrtip",
	            buttons: [{
	                extend: "copy",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3, 4 ]
                    }
	            }, {
	                extend: "csv",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3, 4 ]
                    }
	            }, {
	                extend: "excel",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3, 4 ]
                    }
	            }, {
	                extend: "pdf",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3, 4 ]
                    }
	            }, {
	                extend: "print",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3, 4 ]
                    }
	            }]
	        });

				function clearForm(){
                    $('#transaksi').val('tambah');
                    $('#field-1').val('');
                    $('#field-2').val('');
                    $('#field-13').val('');
                    $('#field-14').val('');
                }

                function bukatutup(status){
                    $('#field-1').prop("readonly", status);
                    $('#field-2').prop("readonly", status);
                    $('#field-3').prop("readonly", status);
                    $('#field-4').prop("readonly", status);
                    $('#field-5').prop("readonly", status);
                    $('#field-6').prop("readonly", status);
                    $('#field-7').prop("readonly", status);
                }
                var hotel = '', transport = '',tiket = '', mhotel = '', res1 = '', res2 = '', lama=0;
                $('#datatable-buttons').on('click','.edit-row',function(){
                    var id=$(this).attr('data');
                    $.ajax({
                        type : "GET",
                        url  : "<?php echo base_url('index.php/pembayaran/get_surat')?>",
                        dataType : "JSON",
                        data : {id:id},
                        success: function(data){
                            bukatutup(false);
                            $('.editaja').removeClass('hidden');
                            $( "#add" ).trigger( "click" );
                            $.each(data,function(){
                                $('#textkode').val(data.n1);
                                $('#transaksi').val(id);
                                $('#field-1').val(data.a1);
                                $('#field-2').autoNumeric('set',0);
                                $('#harianinfo').html(data.b1+' x '+ data.c1);
                                $('#field-3').autoNumeric('set',(data.c1*data.b1));
                                $('#field-0').val(data.l1);
                                $('#namainfo').html('ke '+data.m1);
                                $('#filter2').val(2).trigger('change');
                                tiket = data.i1;
                                hotel = data.k1;
                                mhotel = data.d1;
                                transport = data.j1;
                                res1 = data.g1;
                                res2 = data.h1;
                                lama = data.b1;
                            });
                        }
                    });
                    return false;
                });
            $('#filter1').change(function(e) {
                var config = $(this).val();
                if(config == 0){
                    $('#field-4').autoNumeric('set',0);
                } else {
                    $('#field-4').autoNumeric('set',tiket);
                }
            });
            $('#filter2').change(function(e) {
                var config = $(this).val();
                if(config == 0){
                    $('#field-5').autoNumeric('set',0);
                } else if(config == 1){
                    $('#field-5').autoNumeric('set',hotel);
                } else {
                    $('#field-5').autoNumeric('set',(lama-1)*mhotel);
                }
            });
            $('#filter3').change(function(e) {
                var config = $(this).val();
                if(config == 0){
                    $('#field-6').autoNumeric('set',0);
                } else if(config == 1){
                    $('#field-6').autoNumeric('set',lama*res1);
                } else {
                    $('#field-6').autoNumeric('set',lama*res2);
                }
            });
            $('#filter4').change(function(e) {
                var config = $(this).val();
                if(config == 0){
                    $('#field-7').autoNumeric('set',0);
                } else if(config == 1){
                    $('#field-7').autoNumeric('set',transport);
                }
            });

                $('#simpan').on('click',function(){
                    var kode=$('#textkode').val();
                    var transaksi=$('#transaksi').val();
                    $.ajax({
                        type : "POST",
                        contentType: false,
                        processData: false,
                        url  : "<?php echo base_url('index.php/pembayaran/simpan_surat')?>",
                        data: function() {
                            var data = new FormData();
                            data.append("a1", $("#field-1").val());
                            data.append("b1", $("#field-2").autoNumeric('get'));
                            data.append("c1", $("#field-3").autoNumeric('get'));
                            data.append("d1", $("#field-4").autoNumeric('get'));
                            data.append("e1", $("#field-5").autoNumeric('get'));
                            data.append("f1", $("#field-6").autoNumeric('get'));
                            data.append("h1", $("#field-7").autoNumeric('get'));
                            data.append("g1", kode);
                            data.append("i1", $("#pfield-1").val());
                            data.append("j1", $("#pfield-2").val());
                            data.append("k1", transaksi);
                            return data;
                            // Or simply return new FormData(jQuery("form")[0]);
                        }(),
                        success: function(data){
                            table.ajax.reload();
                            $( "#batal" ).trigger( "click" );
                            window.open("<?php echo base_url('pembayaran/prints/?kode="+transaksi+"')?>");
                        }
                    });
                    return false;
                });
    </script>
	
	<script type="text/javascript" src="assets/js/jquery.smartWizard.js"></script>
</body>
</html>