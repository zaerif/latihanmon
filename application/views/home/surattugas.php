<?php $this->load->view('layouts/admin'); ?>
            <!-- top navigation -->
            <div class="top_nav navbar-fixed-top">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="assets/images/users.png" alt=""><?php echo $this->session->userdata('username'); ?>                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    
                                    <li><a href="Welcome/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
                <div class="right_col" role="main">           


                	<div class="clearfix"></div>

                    <div class="row headofhtml">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><i class="fa fa-dashboard"></i> Surat Tugas <small>Sistem Informasi Surat Perjalanan Dinas</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                	<div class="col-md-2 col-sm-2 col-xs-12">
										<button type="button" class="btn btn-success" id="add"><i class="fa fa-plus-circle"></i> Tambah Surat Tugas</button>
										<button type="button" class="btn btn-danger hidden" id="batal"><i class="fa fa-close"></i> Batal</button>
									</div>
                                        <button type="button" class="btn btn-success hidden" id="settingadd"><i class="fa fa-gears"></i> Setting Penyetuju</button>
									<div class="row editrole hidden">
				                        <div class="col-md-12">
				                            <div class="x_panel">
				                                <div class="x_title">
				                                    <h2 id="titleedit"><i class="fa fa-dashboard"></i> Tambah Surat Tugas <small></small></h2>
				                                    <ul class="nav navbar-right panel_toolbox">
				                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				                                        </li>
				                                    </ul>
				                                    <div class="clearfix"></div>
				                                </div>
				                                <div class="x_content editroles">
                                                    <fieldset class="col-md-12 hidden">     
                                                        <legend>Nota Dinas</legend>
                                                        
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-4" class="control-label col-md-2" style="text-align:right;">Penyetujui</label> 
                                                            <div class="col-md-6">
                                                                <select class="gol2" id="nota-1">
                                                                    <option value="">Pilih Penyetujui</option>
                                                                    <?php
                                                                        foreach($mst_pegawai->result_array() as $me)
                                                                        {
                                                                            
                                                                    ?>
                                                                        <option value="<?php echo $me['id']; ?>"><?php echo $me['nama']; ?></option>
                                                                    <?php
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">Nomor</label> 
                                                            <div class="col-md-10">
                                                                <input type="text" id="nota-2" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">Perihal</label> 
                                                            <div class="col-md-10">
                                                                <input type="text" id="nota-3" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">Isian</label> 
                                                            <div class="col-md-10">
                                                                <textarea id="nota-4" class="form-control" rows="5">Bersamaini dengan hormat dilaporkan, dalam rangka Fasilitas Pengamanan Acara Pelantikan Timsel, Provinsi/Kabupaten/Kota dan Pelantikan Komisioner KPU Provinsi Kabupaten/Kota Periode 2018-2023 di Hotel Shantika Jakarta tanggal 05 s/d 07 Juni 2018, maka dipandang perlu menugaskan personil Satuan Pengamanan KPU yang dianggap cakap dan sigap serta bertanggung jawab dalam melaksanakan tugas.</textarea>
                                                            </div>
                                                        </div> 
                                                    </div>

                                                        </div>
                                                        
                                                    </fieldset> 
                                                    <fieldset class="col-md-12">     
                                                        <legend>Surat Tugas</legend>
                                                        
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-4" class="control-label col-md-2" style="text-align:right;">Penyetujui</label> 
                                                            <div class="col-md-6">
                                                                <select class="gol2" id="field-4">
                                                                    <option value="">Pilih Penyetujui</option>
                                                                    <?php
                                                                        foreach($mst_pegawai->result_array() as $me)
                                                                        {
                                                                            
                                                                    ?>
                                                                        <option value="<?php echo $me['id']; ?>"><?php echo $me['nama']; ?></option>
                                                                    <?php
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-1" class="control-label col-md-2" style="text-align:right;">Nomor</label> 
                                                            <div class="col-md-6">
                                                                <input type="text" id="transaksi" value="tambah" data-a-sign="" class="form-control hidden">
                                                                <input type="text" id="textkode" value="" data-a-sign="" class="form-control hidden">
                                                                <input type="text" id="field-1" placeholder="Nomor" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">Tanggal</label> 
                                                            <div class="col-md-6">
                                                                <div class="input-daterange input-group" id="date-range">
                                                                    <input type="text" placeholder="Awal SPPD" class="form-control" id="field-13" />
                                                                    <span class="input-group-addon bg-custom b-0 text-white">to</span>
                                                                    <input type="text" placeholder="Akhir SPPD" class="form-control" id="field-14" />
                                                                </div>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-3" class="control-label col-md-2" style="text-align:right;">Tugas Ke</label> 
                                                            <div class="col-md-6">
                                                                <select class="gol2" id="field-3">
                                                                    <option value="">Pilih Daerah</option>
                                                                    <?php
                                                                        foreach($mst_daerah->result_array() as $me)
                                                                        {
                                                                            
                                                                    ?>
                                                                        <option value="<?php echo $me['nama']; ?>"><?php echo $me['nama']; ?></option>
                                                                    <?php
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">Menimbang</label> 
                                                            <div class="col-md-10">
                                                                <textarea id="field-5" class="form-control" rows="1"></textarea>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;"></label> 
                                                            <div class="col-md-10">
                                                                <textarea id="men-2" class="form-control" rows="1"></textarea>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;"></label> 
                                                            <div class="col-md-10">
                                                                <textarea id="men-3" class="form-control" rows="1"></textarea>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;"></label> 
                                                            <div class="col-md-10">
                                                                <textarea id="men-4" class="form-control" rows="1"></textarea>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;"></label> 
                                                            <div class="col-md-10">
                                                                <textarea id="men-5" class="form-control" rows="1"></textarea>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">Dasar</label> 
                                                            <div class="col-md-10">
                                                                <input type="text" id="field-6" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;"></label> 
                                                            <div class="col-md-10">
                                                                <input type="text" id="das-2" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;"></label> 
                                                            <div class="col-md-10">
                                                                <input type="text" id="das-3" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;"></label> 
                                                            <div class="col-md-10">
                                                                <input type="text" id="das-4" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;"></label> 
                                                            <div class="col-md-10">
                                                                <input type="text" id="das-5" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;"></label> 
                                                            <div class="col-md-10">
                                                                <input type="text" id="das-6" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;"></label> 
                                                            <div class="col-md-10">
                                                                <input type="text" id="das-7" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;"></label> 
                                                            <div class="col-md-10">
                                                                <input type="text" id="das-8" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;"></label> 
                                                            <div class="col-md-10">
                                                                <input type="text" id="das-9" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;"></label> 
                                                            <div class="col-md-10">
                                                                <input type="text" id="das-10" value="" data-a-sign="" class="form-control">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="control-label col-md-2" style="text-align:right;">Untuk</label> 
                                                            <div class="col-md-10">
                                                                <textarea id="field-7" class="form-control" rows="5">Mendampingi dan Memfasilitasi Anggota KPU Melakukan Supervisi dan Monitoring pelaksanaan verifikasi faktual terhadap sampel dukungan perseorangan calon Peserta Pemilu Anggota DPD yang telah memenuhi syarat berdasarkan penelitian administrasi, di Banda Aceh pada tanggal 7 s/d 8 Juni 2018 selama 2 ( dua ) hari.</textarea>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row" style="margin-top:10px;">
                                                        <div class="col-md-2">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <button type="button" class="btn btn-success" id="simpan"><i class="fa fa-disket"></i> Simpan</button>
                                                        </div>
                                                    </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
				                                    <div class="clearfix"></div>
				                                </div>
				                            </div>
				                        </div>
				                    </div>

		                            <table id="datatable-buttons" class="table table-striped table-bordered">
		                                <thead>
		                                <tr>
		                                    <th>Action</th>
                                            <th>Nomor</th>
		                                    <th>Tugas Ke</th>
		                                    <th>Tanggal</th>
		                                </tr>
		                                </thead>
		                                <tbody>
		                                </tbody>
		                            </table>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            <!-- end content -->
            	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                Konfirmasi Hapus
				            </div>
				            <div class="modal-body">
				                Anda yakin menghapus data?
				            </div>
				            <div class="modal-footer">
				                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				                <a class="btn btn-danger btn-ok">Delete</a>
				            </div>
				        </div>
				    </div>
				</div>         <!-- end content -->
                <div class="modal fade" id="confirm-delete1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                Setting Penyetuju
                            </div>
                            <div class="modal-body">
                                <div class="row" style="margin-top:10px;">
                                    <div class="form-group"> 
                                        <label for="field-4" class="control-label col-md-2" style="text-align:right;">Penyetuju</label> 
                                        <div class="col-md-6">
                                            <select class="gol2" id="pfield-1">
                                                <option value="">Pilih Pegawai</option>
                                                <?php
                                                    foreach($mst_pegawai->result_array() as $me)
                                                    {
                                                        if($me['id'] == 8){
                                                            ?> <option value="<?php echo $me['id']; ?>"  selected="selected"><?php echo $me['nama']; ?></option>
                                                            <?php
                                                        } else {
                                                        
                                                ?>
                                                    <option value="<?php echo $me['id']; ?>"><?php echo $me['nama']; ?></option>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div> 
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                </div>

                <!-- footer content -->

           
            </div>
            <!-- /page content -->

        

    

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="assets/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="assets/js/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="assets/js/bootstrap-progressbar.min.js"></script>
    <script src="assets/js/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="assets/js/icheck.min.js"></script>
    <script src="assets/js/custom.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="assets/js/moment.min2.js"></script>
    <script type="text/javascript" src="assets/js/daterangepicker.js"></script>
    <!-- input mask -->
    <script src="assets/js/jquery.inputmask.js"></script>
    <!-- knob -->
    <script src="assets/js/jquery.knob.min.js"></script>
    <!-- range slider -->
    <script src="assets/js/ion.rangeSlider.min.js"></script>
    <!-- color picker -->
    <script src="assets/js/bootstrap-colorpicker.js"></script>
    <script src="assets/js/docs.js"></script>
	<!-- select2 -->
        <script src="assets/js/select2.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="assets/js/parsley.min.js"></script>
    <!-- image cropping -->
    <script src="assets/js/cropper.min.js"></script>
    <script src="assets/js/main2.js"></script>

	<script src="assets/js/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="assets/js/countries.js"></script>
        <script src="assets/js/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.bootstrap.js"></script>

		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/jszip.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/pdfmake.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/vfs_fonts.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.html5.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/buttons.print.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedHeader.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.keyTable.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.responsive.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/responsive.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.scroller.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.colVis.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.fixedColumns.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <!-- input_mask -->
    <!-- /input mask -->
    <script type="text/javascript">
    	$('.gol2').select2({ width: '100%' });
    	$('#add').on('click',function(){
            $(this).addClass('hidden');
            $('.editroles').css('display','block');
            $('#batal').removeClass('hidden');
            $('.editrole').removeClass('hidden');
            $('#transaksi').val('tambah');
            $('#simpan').removeClass('hidden');
            bukatutup(false);
            clearForm();
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: "dd/mm/yyyy"
        });
    	$('#batal').on('click',function(){
            bukatutup(false);
            $(this).addClass('hidden');
            $('#add').removeClass('hidden');
            $('.editrole').addClass('hidden');
        });
        $('#settingadd').on('click',function(){
            $('#confirm-delete1').modal('show');
        });
        var table = $("#datatable-buttons").DataTable({
	          processing: true,
	          serverSide: true,
	                  ajax: {"url": "<?php echo base_url().'index.php/surattugas/get_surat_json'?>", "type": "POST"},
	                        columns: [
	                            {"data": "view"},
	                            {"data": "no"},
                                {"data": "daerah"},
                                {"data": "dari"}
	                      ],
	                    order: [[2, 'asc']],
	            dom: "Blfrtip",
	            buttons: [{
	                extend: "copy",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3 ]
                    }
	            }, {
	                extend: "csv",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3 ]
                    }
	            }, {
	                extend: "excel",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3 ]
                    }
	            }, {
	                extend: "pdf",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3 ]
                    }
	            }, {
	                extend: "print",
	                className: "btn-sm",
                    exportOptions: {
                        columns: [ 1, 2, 3 ]
                    }
	            }]
	        });

				function clearForm(){
                    $('#transaksi').val('tambah');
                    $('#field-1').val('');
                    $('#field-2').val('');
                    $('#field-13').val('');
                    $('#field-14').val('');
                    $('#field-3').val('').trigger('change');
                    $('#field-4').val('').trigger('change');
                    $('#nota-1').val('').trigger('change');
                    $('#field-5').val('Bahwa dalam rangka memastikan pelaksanaan verifikasi faktual terhadap sampel dukungan perseorangan calon Peserta Pemilu Anggota DPD yang telah memenuhi syarat berdasarkan penelitian administrasi yang dilaksanakan oleh KPU/ KIP Kabupaten/Kota dilakukan sesuai dengan ketentuan Peraturan KPU Nomor 14 Tahun 2018 tentang Pencalonan Perseorangan Peserta Pemilihan Umum Anggota Dewan Perwakilan Daerah;');
                    $('#men-2').val('Bahwa dalam rangka Mendampingi dan Memfasilitasi Anggota KPU Melakukan Supervisi dan Monitoring pelaksanaan verifikasi faktual terhadap sampel  dukungan perseorangan calon Peserta Pemilu Anggota DPD yang telah memenuhi syarat berdasarkan penelitian administrasi;');
                    $('#men-3').val('Bahwa untuk melaksanakan kegiatan pada point a dan b dipandang perlu pembentukan Surat Tugas dimaksud.');
                    $('#men-4').val('');
                    $('#men-5').val('');
                    $('#field-6').val('Undang-Undang Nomor 7 Tahun 2017 tentang Pemilihan Umum ( Lembaran Negara Republik Indonesia Tahun 2011 Nomor 182, Tambahan Lembaran Negara Republik Indonesia Nomor 6109 );');
                    $('#das-2').val('Peraturan Komisi Pemilihan Umum Nomor 05 Tahun 2008 Tentang Tata Kerja Komisi Pemilihan Umum, Komisi Pemilihan Umum Provinsi dan Komisi Pemilihan Umum Kabupaten/ Kota sebagaimana di ubah dengan Peraturan Komisi Pemilihan Umum Nomor 21 Tahun 2008, Peraturan Komisi Pemilihan Umum Nomor 37 Tahun 2008 dan Peraturan Komisi Pemilihan Nomor 01 Tahun 2010;');
                    $('#das-3').val('Peraturan Komisi Pemilihan Umum Nomor 06 Tahun 2008 tentang Susunan Organisasi dan Tata Kerja Sekretariat Jenderal KPU, Sekretariat KPU Provinsi dan Sekretariat KPU Kabupaten/ Kota sebagaimana diubah dengan Peraturan Komisi Pemilihan Umum Nomor 22 Tahun 2008;');
                    $('#das-4').val('Peraturan Menteri Keuangan Nomor. 49/PMK.02/2017 tentang Standar Biaya Masukan Tahun Anggaran 2018;');
                    $('#das-5').val('SK. Sekretaris Jenderal KPU Nomor. 442 / Kpts / Setjen / TAHUN 2015 tentang Pelimpahan Wewenang Penandatangan Surat Tugas.');
                    $('#das-6').val('DIPA KPU BA. 076 Tahun Anggaran 2018.');
                    $('#nota-4').val('Fasilitas Pengamanan');
                    $('#nota-4').val('Bersama ini dengan hormat dilaporkan, dalam rangka Fasilitas Pengamanan Acara Pelantikan Timsel, Provinsi/Kabupaten/Kota dan Pelantikan Komisioner KPU Provinsi Kabupaten/Kota Periode 2018-2023 di Hotel Shantika Jakarta tanggal 05 s/d 07 Juni 2018, maka dipandang perlu menugaskan personil Satuan Pengamanan KPU yang dianggap cakap dan sigap serta bertanggung jawab dalam melaksanakan tugas.');
                }

                function bukatutup(status){
                    $('#field-1').prop("readonly", status);
                    $('#field-2').prop("readonly", status);
                    $('#field-3').prop("readonly", status);
                    $('#field-4').prop("readonly", status);
                    $('#field-5').prop("readonly", status);
                    $('#field-6').prop("readonly", status);
                    $('#field-7').prop("readonly", status);
                }
                //GET UPDATE
                $('#datatable-buttons').on('click','.lihat-row',function(){
                    var id=$(this).attr('data');
                    window.open("<?php echo base_url('surattugas/prints2/"+id+"')?>");
                });
                //GET UPDATE
                $('#datatable-buttons').on('click','.edit-row',function(){
                    var id=$(this).attr('data');
                    $.ajax({
                        type : "GET",
                        url  : "<?php echo base_url('index.php/surattugas/get_surat')?>",
                        dataType : "JSON",
                        data : {id:id},
                        success: function(data){
                            bukatutup(false);
                            $('.editaja').removeClass('hidden');
                            $( "#add" ).trigger( "click" );
                            $('#simpan').removeClass('hidden');
                            $('#transaksi').val('edit');
                            $.each(data,function(){
                                $('#textkode').val(id);
                                $('#field-1').val(data.a1);
                                $('#field-3').val(data.c1).trigger('change');
                                $('#field-4').val(data.d1).trigger('change');
                                $('#field-13').datepicker('setDate', new Date(data.b1));
                                $('#field-14').datepicker('setDate', new Date(data.e1));
                                var parse = data.f1.split('##');
                                $('#field-5').val(parse[0]);
                                $('#men-2').val(parse[1]);
                                $('#men-3').val(parse[2]);
                                $('#men-4').val(parse[3]);
                                $('#men-5').val(parse[4]);
                                var parse1 = data.g1.split('##');
                                $('#field-6').val(parse1[0]);
                                $('#das-2').val(parse1[1]);
                                $('#das-3').val(parse1[2]);
                                $('#das-4').val(parse1[3]);
                                $('#das-5').val(parse1[4]);
                                $('#das-6').val(parse1[5]);
                                $('#das-7').val(parse1[6]);
                                $('#das-8').val(parse1[7]);
                                $('#das-9').val(parse1[8]);
                                $('#das-10').val(parse1[9]);
                                $('#field-7').val(data.h1);
                                $('#nota-1').val(data.i1).trigger('change');
                                $('#nota-2').val(data.j1);
                                $('#nota-3').val(data.k1);
                                $('#nota-4').val(data.l1);
                            });
                        }
                    });
                    return false;
                });
                $('#datatable-buttons').on('click','.hapus-row',function(){
                    var id=$(this).attr('data');
                    $('#confirm-delete').modal('show');
                    $('.btn-ok').on('click',function(){
	                    $.ajax({
	                    type : "POST",
	                    url  : "<?php echo base_url('index.php/surattugas/hapus_surat')?>",
	                    dataType : "JSON",
	                            data : {kode: id},
	                            success: function(data){
                    				$('#confirm-delete').modal('hide');
	                                table.ajax.reload();
                            		$( "#batal" ).trigger( "click" );
	                            }
	                        });
	                        return false;
	                });
                });

                $('#simpan').on('click',function(){
                    var a1=$('#field-1').val();
                    var b1=$('#field-2').val();
                    var c1=$('#field-3').val();
                    var d1=$('#field-4').val();
                    var kode=$('#textkode').val();
                    var transaksi=$('#transaksi').val();
                    var string = $("#field-5").val()+'##'+$('#men-2').val()+'##'+$('#men-3').val()+'##'+$('#men-4').val()+'##'+$('#men-5').val();
                    var string1 = $('#field-6').val()+'##'+$('#das-2').val()+'##'+$('#das-3').val()+'##'+$('#das-4').val()+'##'+$('#das-5').val()+'##'+$('#das-6').val()+'##'+$('#das-7').val()+'##'+$('#das-8').val()+'##'+$('#das-9').val()+'##'+$('#das-10').val();
                    if(transaksi == 'tambah'){
	                    $.ajax({
	                        type : "POST",
	                        contentType: false,
	                        processData: false,
	                        url  : "<?php echo base_url('index.php/surattugas/simpan_surat')?>",
	                        data: function() {
	                            var data = new FormData();
	                            data.append("a1", $("#field-1").val());
	                            data.append("b1", $("#field-3").val());
	                            data.append("c1", $("#field-4").val());
	                            data.append("d1", string);
                                data.append("e1", string1);
                                data.append("f1", $("#field-7").val());
                                data.append("g1", $("#field-13").val());
                                data.append("h1", $("#field-14").val());
                                data.append("i1", $("#nota-1").val());
                                data.append("j1", $("#nota-2").val());
                                data.append("k1", $("#nota-3").val());
                                data.append("l1", $("#nota-4").val());
	                            return data;
	                            // Or simply return new FormData(jQuery("form")[0]);
	                        }(),
	                        success: function(data){
	                            table.ajax.reload();
                            	$( "#batal" ).trigger( "click" );
	                        }
	                    });
	                    return false;
                    } else {
	                    $.ajax({
	                        type : "POST",
	                        contentType: false,
	                        processData: false,
	                        url  : "<?php echo base_url('index.php/surattugas/update_surat')?>",
	                        data: function() {
	                            var data = new FormData();
                                data.append("a1", $("#field-1").val());
                                data.append("b1", $("#field-3").val());
                                data.append("c1", $("#field-4").val());
                                data.append("d1", string);
                                data.append("e1", string1);
                                data.append("f1", $("#field-7").val());
                                data.append("g1", $("#field-13").val());
                                data.append("h1", $("#field-14").val());
                                data.append("i1", $("#nota-1").val());
                                data.append("j1", $("#nota-2").val());
                                data.append("k1", $("#nota-3").val());
                                data.append("l1", $("#nota-4").val());
	                            data.append("kode", kode);
	                            return data;
	                            // Or simply return new FormData(jQuery("form")[0]);
	                        }(),
	                        success: function(data){
	                            table.ajax.reload();
                            	$( "#batal" ).trigger( "click" );
	                        }
	                    });
	                    return false;
                    }
                });
    </script>
	
	<script type="text/javascript" src="assets/js/jquery.smartWizard.js"></script>
</body>
</html>