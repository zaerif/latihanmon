<?php $this->load->view('layouts/admin'); ?>
            <!-- top navigation -->
            <div class="top_nav navbar-fixed-top">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="assets/images/logo_perhunungan.png" alt=""><?php echo $this->session->userdata('username'); ?>                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    
                                    <li><a href="Welcome/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
                <div class="right_col" role="main">
                <!-- top tiles -->
                <div class="row tile_count">
                    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                        <div class="left"></div>
                        <div class="right">
                            <span class="count_top"><i class="fa fa-clock-o"></i> Tahun</span>
                            <div class="count"><?php echo date('Y');?></div>
                            
                        </div>
                    </div>
                    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                        <div class="left"></div>
                        <div class="right">
                            <span class="count_top"><i class="fa fa-clock-o"></i> Perjalanan Aktif</span>
                            <div class="count">
							<?php echo $jalan;?>							</div>
                           
                        </div>
                    </div>
                    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                        <div class="left"></div>
                        <div class="right">
                            <span class="count_top"><i class="fa fa-user"></i> Perjalanan Rampung</span>
                            <div class="count green">
							<?php echo $rampung;?>							</div>
                            
                        </div>
                    </div>
                    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                        <div class="left"></div>
                        <div class="right">
                            <span class="count_top"><i class="fa fa-user"></i> Jumlah Pegawai							
							</span>
                            <div class="count"><?php echo $pegawai;?></div>
                           
                        </div>
                    </div>
                    <div class="animated flipInY col-md-4 col-sm-4 col-xs-4 tile_stats_count">
                        <div class="left"></div>
                        <div class="right">
                            <span class="count_top"><i class="fa fa-user"></i> Total Biaya PD</span>
                            <div class="count">
							Rp. <?php echo $total;?>					</div>
                           
                        </div>
                    </div>
                   

                </div>
                <!-- /top tiles style="font-size:15px;font-weight: 600;" -->


                


                <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><i class="fa fa-dashboard"></i> Aplikasi SPD <small>Sistem Informasi Surat Perjalanan Dinas</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <div class="col-md-6 col-lg-6 col-sm-7">
                                        <!-- blockquote -->
                                        <blockquote>
										
                                            <p>Selamat datang di Aplikasi Perjalanan Dinas</p>
                                            
											<p style="text-align:justify;"><img src="assets/images/logo_perhunungan.png" alt="Avatar" style="width:30%;float:left;margin-right:10px;" align="justify">SPD adalah surat yang memuat keterangan tentang penugasan seseorang pejabat/pegawai suatu kantor, untuk bertugas ke suatu wilayah dengan biaya kantor untuk jangka waktu yang ditentukan. Dalam dinas pemerintahan sering disebut Surat Perintah Perjalanan Dinas (SPPD) yang ditujukan kepada seorang pegawai untuk melakukan tugas tertentu. Yang dimaksud dengan perjalanan dinas adalah perjalanan ke luar dari tempat kedudukan yang jaraknya minimal 5km dari batas kota, dilakukan atas perintah dan wewenang untuk keperluan Negara.</p>
                                            <footer>2018 <cite title="Source Title">KOMISI PEMILIHAN UMUM REPUBLIK INDONESIA</cite>
                                            </footer>
                                        </blockquote>

                                        <blockquote class="blockquote-reverse">
                                            <p>SPD berguna sebagai pelengkap keterangan bagi si pemilik agar dalam melaksankan tugas dinas dapat berjalan dengan efisien sesuai dengan jadwal waktu yang sudah tersedia.</p>
                                            <footer>2018 <cite title="Source Title">KOMISI PEMILIHAN UMUM REPUBLIK INDONESIA</cite>
                                            </footer>
                                        </blockquote>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-5">
                                        <div class="x_content">
                                <ul class="list-unstyled timeline">
                                    <li>
                                        <div class="block">
                                            <div class="tags">
                                                <a href="Surattugas" class="tag">
                                                    <span>Perjalanan</span>
                                                </a>
                                            </div>
                                            <div class="block_content">
                                                <h2 class="title">
                                        <a>Input Surat Tugas</a>
                                    </h2>
                                                <div class="byline">
                                                    <span>&nbsp;</span>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="block">
                                            <div class="tags">
                                                <a href="Demo" class="tag">
                                                    <span>Penugasan</span>
                                                </a>
                                            </div>
                                            <div class="block_content">
                                                <h2 class="title">
                                        <a>Penugasan Pegawai</a>
                                    </h2>
                                                <div class="byline">
                                                    <span>&nbsp;</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="block">
                                            <div class="tags">
                                                <a data-toggle="modal" data-target=".confirm-delete" class="tag">
                                                    <span>Cetak</span>
                                                </a>
                                            </div>
                                            <div class="block_content">
                                                <h2 class="title">
                                        <a>Surat Tugas</a>
                                    </h2>
                                                <div class="byline">
                                                    <span>&nbsp;</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="block">
                                            <div class="tags">
                                                <a href="laporan/globals" class="tag">
                                                    <span>Rekap</span>
                                                </a>
                                            </div>
                                            <div class="block_content">
                                                <h2 class="title">
                                        <a>Rekap Kegiatan</a>
                                    </h2>
                                                <div class="byline">
                                                    <span>&nbsp;</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>            <!-- end content -->      <!-- end content -->
                <div class="modal fade confirm-delete" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                Cetak
                            </div>
                            <div class="modal-body">
                                <div class="row" style="margin-top:10px;">
                                    <div class="form-group"> 
                                        <label for="field-4" class="control-label col-md-2" style="text-align:right;">No Nota Dinas</label> 
                                        <div class="col-md-6">
                                            <select class="gol2" id="nota-0">
                                                <option value="">Pilih No Dinas</option>
                                                <?php
                                                    foreach($mst_nota->result_array() as $me)
                                                    {
                                                        
                                                ?>
                                                    <option value="<?php echo $me['id']; ?>"><?php echo $me['notano']; ?></option>
                                                <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div> 
                                </div>
                                <div class="row" style="margin-top:10px;">
                                    <div class="form-group"> 
                                        <label for="field-4" class="control-label col-md-2" style="text-align:right;">No ST</label> 
                                        <div class="col-md-6">
                                            <select class="gol2" id="nota-1">
                                                <option value="">Pilih No ST</option>
                                                <?php
                                                    foreach($mst_st->result_array() as $me)
                                                    {
                                                        
                                                ?>
                                                    <option value="<?php echo $me['id']; ?>"><?php echo $me['no']; ?></option>
                                                <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div> 
                                </div>
                                <div class="row" style="margin-top:10px;">
                                    <div class="form-group"> 
                                        <label for="field-4" class="control-label col-md-2" style="text-align:right;">Pegawai</label> 
                                        <div class="col-md-6">
                                            <select class="gol2" id="nota-2">
                                                <option value="">Pilih Pegawai</option>
                                                <?php
                                                    foreach($mst_pegawai->result_array() as $me)
                                                    {
                                                        
                                                ?>
                                                    <option value="<?php echo $me['id']; ?>"><?php echo $me['nama']; ?></option>
                                                <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="row">
                                    <fieldset class="col-md-12">     
                                        <legend>Surat</legend>
                                        <div class="col-md-3">
                                            <div class="bs-glyphicons">
                                                <ul class="bs-glyphicons-list">
                                                <a target="_blank" id="btnnt" style="cursor:pointer;">
                                                            <li style="width:100%;">
                                                                <span class="badge bg-orange"><i class="fa fa-exclamation"></i></span>
                                                                <span class="glyphicon glyphicon-print"></span>
                                                                <span class="glyphicon-class">Cetak Nota Dinas</span> 
                                                            </li>
                                                        </a>        
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="bs-glyphicons">
                                                <ul class="bs-glyphicons-list">
                                                <a target="_blank" id="btnst" style="cursor:pointer;">
                                                            <li style="width:100%;">
                                                                <span class="badge bg-orange"><i class="fa fa-exclamation"></i></span>
                                                                <span class="glyphicon glyphicon-print"></span>
                                                                <span class="glyphicon-class">Cetak Surat Tugas (ST)</span> 
                                                            </li>
                                                        </a>    
                                                </ul>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="col-md-12">     
                                        <legend>Pembayaran</legend>
                                        <div class="col-md-3">
                                            <div class="bs-glyphicons">
                                                <ul class="bs-glyphicons-list">
                                                <a target="_blank" id="btnpembayaranst" style="cursor:pointer;">
                                                            <li style="width:100%;">
                                                                <span class="badge bg-orange"><i class="fa fa-exclamation"></i></span>
                                                                <span class="glyphicon glyphicon-print"></span>
                                                                <span class="glyphicon-class">Cetak Surat Tugas</span> 
                                                            </li>
                                                        </a>    
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="bs-glyphicons">
                                                <ul class="bs-glyphicons-list">
                                                <a target="_blank" id="btnpembayaran" style="cursor:pointer;">
                                                            <li style="width:100%;">
                                                                <span class="badge bg-orange"><i class="fa fa-exclamation"></i></span>
                                                                <span class="glyphicon glyphicon-print"></span>
                                                                <span class="glyphicon-class">Cetak Lengkap</span> 
                                                            </li>
                                                        </a>    
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="bs-glyphicons">
                                                <ul class="bs-glyphicons-list">
                                                <a target="_blank" id="btnpembayaran0" style="cursor:pointer;">
                                                            <li style="width:100%;">
                                                                <span class="badge bg-orange"><i class="fa fa-exclamation"></i></span>
                                                                <span class="glyphicon glyphicon-print"></span>
                                                                <span class="glyphicon-class">Cetak Kwitansi</span> 
                                                            </li>
                                                        </a>    
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="bs-glyphicons">
                                                <ul class="bs-glyphicons-list">
                                                <a target="_blank" id="btnpembayaran1" style="cursor:pointer;">
                                                            <li style="width:100%;">
                                                                <span class="badge bg-orange"><i class="fa fa-exclamation"></i></span>
                                                                <span class="glyphicon glyphicon-print"></span>
                                                                <span class="glyphicon-class">Cetak Rincian</span> 
                                                            </li>
                                                        </a>    
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="bs-glyphicons">
                                                <ul class="bs-glyphicons-list">
                                                <a target="_blank" id="btnpembayaran2" style="cursor:pointer;">
                                                            <li style="width:100%;">
                                                                <span class="badge bg-orange"><i class="fa fa-exclamation"></i></span>
                                                                <span class="glyphicon glyphicon-print"></span>
                                                                <span class="glyphicon-class">Cetak Pengeluaran Rill</span> 
                                                            </li>
                                                        </a>    
                                                </ul>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>         <!-- end content -->
            
                </div>

                <!-- footer content -->

           
            </div>
            <!-- /page content -->

        

    

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="assets/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="assets/js/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="assets/js/bootstrap-progressbar.min.js"></script>
    <script src="assets/js/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="assets/js/icheck.min.js"></script>
    <script src="assets/js/custom.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="assets/js/moment.min2.js"></script>
    <script type="text/javascript" src="assets/js/daterangepicker.js"></script>
    <!-- input mask -->
    <script src="assets/js/jquery.inputmask.js"></script>
    <!-- knob -->
    <script src="assets/js/jquery.knob.min.js"></script>
    <!-- range slider -->
    <script src="assets/js/ion.rangeSlider.min.js"></script>
    <!-- color picker -->
    <script src="assets/js/bootstrap-colorpicker.js"></script>
    <script src="assets/js/docs.js"></script>
	<!-- select2 -->
        <script src="assets/js/select2.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="assets/js/parsley.min.js"></script>
    <!-- image cropping -->
    <script src="assets/js/cropper.min.js"></script>
    <script src="assets/js/main2.js"></script>

	<script src="assets/js/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="assets/js/countries.js"></script>
        <script src="assets/js/jquery.autocomplete.js"></script>
    <!-- input_mask -->
    <script>
        $(document).ready(function () {
            $(":input").inputmask();
        });
    </script>
    <!-- /input mask -->
	
	<script type="text/javascript" src="assets/js/jquery.smartWizard.js"></script>
    <script type="text/javascript">
        $('.gol2').select2({ width: '100%' });
        $('#nota-1').change(function(e) {
            var config = $(this).val();
            getTanggal2(config);
        });
        $('#btnnt').on('click',function(){
            var trans = $('#nota-0').val();
            window.open("<?php echo base_url('laporan/notadinas/"+trans+"')?>");
        });
        $('#btnst').on('click',function(){
            var trans = $('#nota-1').val();
            window.open("<?php echo base_url('surattugas/prints/"+trans+"')?>");
        });
        $('#btnpembayaranst').on('click',function(){
            var trans = $('#nota-1').val();
            var kode = $('#nota-2').val();
            window.open("<?php echo base_url('laporan/surat/"+trans+"')?>");
        });
        $('#btnpembayaran').on('click',function(){
            var trans = $('#nota-1').val();
            var kode = $('#nota-2').val();
            window.open("<?php echo base_url('pembayaran/printshome/?kode="+trans+"&id="+kode+"')?>");
        });
        $('#btnpembayaran0').on('click',function(){
            var trans = $('#nota-1').val();
            var kode = $('#nota-2').val();
            window.open("<?php echo base_url('pembayaran/printshomev1/?kode="+trans+"&id="+kode+"')?>");
        });
        $('#btnpembayaran1').on('click',function(){
            var trans = $('#nota-1').val();
            var kode = $('#nota-2').val();
            window.open("<?php echo base_url('pembayaran/printshomev2/?kode="+trans+"&id="+kode+"')?>");
        });
        $('#btnpembayaran2').on('click',function(){
            var trans = $('#nota-1').val();
            var kode = $('#nota-2').val();
            window.open("<?php echo base_url('pembayaran/printshomev3/?kode="+trans+"&id="+kode+"')?>");
        });
        function getTanggal2(data){
            $('#nota-2').select2().empty();
            var html = '<option value="">Pilih Pegawai</option>';
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('index.php/Welcome/get_pegawai3')?>",
                dataType : "JSON",
                data : {id:data},
                success: function(data){
                    /*html +='<option value="'+data.a1+'">'+data.b1+'</option>';*/
                    $.each(data,function(e){
                        html +='<option value="'+data[e].id+'">'+data[e].nama+'</option>';
                    });  
                    $('#nota-2').append(html);
                    $('.gol2').select2({ width: '100%' });
                }
            });
        }
    </script>
</body>
</html>