<!doctype html> 
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Surat Tugas</title>
      <!-- bootstrap -->
      <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	
	<style type="text/css">
/*	html, body{
		width: 150mm;
		height: 297mm;
		position: relative;
	}
	body{
		padding: 0 25.4mm;
		border: 0.5px solid black;
		font: 11pt Segoe UI;
	}
*/	#header{
		font: 10pt Segoe UI;
		height: 20mm;
	}
	#header h1{
		font: 14pt Segoe UI;
		margin-bottom: 0;
	}
	#header hr{
		margin: 2pt auto;
		width: 90%;
		border: 1px double black;
	}
	#header .left img{
		height: 36.8mm;
		width: 26.2mm;
		margin-top: 14pt;
	}
	#header .right img{
		height: 21.2mm;
		width: 24.1mm;
		margin-top: 24pt;
	}
	table{
		margin-left: -20px;
		margin-right: -20px;
		width: 100%;
	}
	#konten{
		margin-left: 0px;
		width: 100%;
		text-align: justify;
	}
	#ttd{
		float: right;
		text-align: center;
		margin-right: 150px;
	}
	tr td{
		margin-left: 0px;
		padding: 5 5 5 5;
		text-align: justify;
		vertical-align: top;
	}
	p.ganti{
		page-break-after: always;
	}
	tr td:first-child{
		margin-left: 0px;
	}
	tr td:last-child{
		margin-left: 0px;
		width: 50%;
	}
	footer{
		clear: both;
		margin-top: 30mm;
		text-align: justify;
		font-size: 10px;
/*		position: absolute;
		height: 25.4mm;
		bottom: 5.4mm;
*/		float: left; 
		margin-left: 10mm;
	}
	
	</style>

   </head>
   <body> 
	<div id="header">
		<div class="col-xs-6" style="text-align:center;">
			<b>KOMISI PEMILIHAN UMUM</b><br>
			<p>Jalan Imam Bonjol No. 29</p>
			<p>JAKARTA</p>
		</div>
		<div class="col-xs-2">
			Lembar <br>
			Kode No <br>
			Nomor <br>
</div>
		<div class="col-xs-1"  style="margin-left:-20mm;">
			:<br>
			:<br>
			:<br>
		</div>
		<div class="col-xs-2" style="margin-left:-20mm">
			............................................................<br>
			............................................................<br>
			............................................................<br>
		</div>
	</div>
	<div class="row">
		<div class="row">
		</div>
		<div class="col-md-12" align="center">
			<u>SURAT PERJALANAN DINAS</u></br>
		</div>
		<br>
		<div class="col-md-12" style="margin-left: 10mm;">
		</div>
	</div>

	<table border="1" style="width:100%;margin-right:-50px;">
		<tr style="width:100%;">
			<td style="width:20px;">1</td>
			<td width="35%">Pejabat Pembuat Komitmen</td>
			<td width="50%" colspan="2"><?php echo $penye; ?></td>
		</tr>
		<tr>
			<td style="width:20px;">2</td>
			<td width="35%">Nama/NIP Pegawai yang melaksanakan perjalanan dinas</td>
			<td width="50%" colspan="2"><?php echo $nama; ?></td>
		</tr>
		<tr>
			<td style="width:20px;">3</td>
			<td width="35%">a. Pengkat dan Golongan<br>
b. Jabatan / Instansi<br>
c. Tingkat Biaya Perjalanan Dinas</td>
			<td width="50%" colspan="2">a. <?php echo $golongan; ?><br>b. <?php echo $jabatan; ?><br>c. -</td>
		</tr>
		<tr>
			<td style="width:20px;">4</td>
			<td width="35%">Maksud Perjalanan Dinas </td>
			<td width="50%" colspan="2" style="text-align:justify;"><?php echo $untuk; ?></td>
		</tr>
		<tr>
			<td style="width:20px;">5</td>
			<td width="35%">Alat angkut yang depergunakan </td>
			<td width="50%" colspan="2"></td>
		</tr>
		<tr>
			<td style="width:20px;">6</td>
			<td width="35%">a. Tempat berangkat<br>
b. Tempat Tujuan
</td>
			<td width="50%" colspan="2"> a. Jakarta <br>b. <?php echo $daerah; ?></td>
		</tr>
		<tr>
			<td style="width:20px;">7</td>
			<td width="35%">a. Lamanya Perjalanan Dinas<br>
b. Tanggal berangkat <br>
c. Tanggal harus kembali/tiba di tempat baru *)
</td>
			<td width="50%" colspan="2"> a. <?php echo $lama; ?> hari <br>b. <?php echo $dari; ?><br>c. <?php echo $ke; ?></td>
		</tr>
		<tr>
			<td style="width:20px;">8</td>
			<td width="35%"> <b>Pengikut </b>: Nama </td>
			<td width="25%">Tanggal Lahir</td>
			<td width="25%">Keterangan</td>
		</tr>
		<tr>
			<td style="width:20px;"></td>
			<td width="35%"></td>
			<td width="25%"></td>
			<td width="25%"></td>
		</tr>
		<tr>
			<td style="width:20px;">9</td>
			<td width="35%"> Pembebanan Anggaran <br>
a. Instansi <br>
b. Akun </td>
			<td width="50%" colspan="2"> <br>a. KOMISI PEMILIHAN UMUM <br>b.</td>
		</tr>
		<tr>
			<td style="width:20px;">10</td>
			<td width="35%">Keterangan lain-lain</td>
			<td width="50%" colspan="2"></td>
		</tr>
	</table>
	<p>coret yang tidak perlu</p>
	<div id="ttd" class="row" style="margin-left:20mm;">
		<div class="col-md-12">
			Jakarta, <?php echo date('d F Y');?><br>
			Kepala Biro Umum,
			<p><br><br><br></p>
			<?php echo $penye; ?><br>
		</div>
	</div>

   </body>
</html>