<html lang="en">
<head>
    <link rel="shortcut icon" type="image/ico" href="logo-prob.png">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aplikasi Perjalanan Dinas</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">
    <link href="assets/css/custom.css" rel="stylesheet">
    <link href="assets/css/green.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
</head>

<body style="background:url(assets/images/bg.png);">
    
    <div class="">


        <div id="wrapper">
            <div id="login" class="animate shake">
                <section class="login_content">
				<img src="assets/images/logo_perhunungan.png" style="width:100px;height:100px;">
				<h3></h3>
				<br>
                    <h2>KOMISI PEMILIHAN UMUM <br>REPUBLIK INDONESIA</h2>
                    <h5>PEMBUATAN SURAT PERJALANAN DINAS</h5>
            <?php echo form_open('login/index','class="form-horizontal m-t-20"'); ?>
						<h1>LOGIN</h1>
                 
                        <div>
						
                        <input class="form-control" type="text" required="" placeholder="Username" name="username" value="<?php echo set_value('username'); ?>">
                        </div>
                        <div>
                            <input class="form-control" type="password" required="" placeholder="Password" name="password">
                        </div>
                        <div>
                            <input class="btn btn-primary submit" value="Login" type="submit">
                            
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">                         
                            <br>
                            <div>
                                <p>© Copyright 2018, Aplikasi Surat Perjalanan Dinas (SPT)</p>
                            </div>
                        </div>
           <?php echo form_close(); ?>
            
    
    <?php if(validation_errors()) { ?>
    <div class="alert alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
        <h4>Terjadi Kesalahan!</h4>
        <?php echo validation_errors(); ?>
    </div>
    <?php } ?>
    
    <?php if($this->session->flashdata('result_login')) { ?>
    <div class="alert alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
        <h4>Terjadi Kesalahan!</h4>
        <?php echo $this->session->flashdata('result_login'); ?>
    </div>
    <?php } ?>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
            
        </div>
    </div>




</body>
</html>