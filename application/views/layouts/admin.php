<html class="nprogress-busy" lang="en"><head>
    <link rel="shortcut icon" type="image/ico" href="logo-prob.png">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aplikasi Perjalanan Dinas | </title>
    <!-- DataTables -->
    <link href="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>assets/js/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>assets/js/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>assets/js/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>assets/js/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>assets/js/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>assets/js/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>assets/js/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <link href="assets/css/green.css" rel="stylesheet">
    <link href="assets/css/prettify.css" rel="stylesheet">
    <link href="assets/css/index.css" rel="stylesheet">
    <!-- select2 -->
    <link href="assets/css/select2.min.css" rel="stylesheet">
    <!-- switchery -->
    <link rel="stylesheet" href="assets/css/switchery.min.css">

    <script src="assets/js/jquery.min.js"></script>
    <style type="text/css">
            fieldset 
    {
        border: 1px solid #ddd !important;
        margin: 0;
        xmin-width: 0;
        padding: 5px;       
        position: relative;
        border-radius:2px;
        background-color:#f5f5f5;
        padding-left:10px!important;
    }   
    
        legend
        {
            font-size:14px;
            font-weight:bold;
            margin-bottom: 0px; 
            width: 35%; 
            border: 1px solid #ddd;
            border-radius: 4px; 
            padding: 5px 5px 5px 10px; 
            background-color: #ffffff;
        }
    </style>
	

</head>


<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view" style="overflow: hidden; cursor: -moz-grab;" tabindex="5000">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.php" class="site_title"><i class="fa fa-desktop"></i> <span>Aplikasi Perjalanan Dinas</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="assets/images/logo_perhunungan.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Selamat Datang,</span>
                            <h2><?php echo $this->session->userdata('username'); ?> </h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br>

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section active">
                            <h3> &nbsp; </h3>
                            <ul class="nav side-menu" style="">
                                <li class="current-page"><a href="Welcome"><i class="fa fa-home"></i> Home </a></li>
									<li><a href="Pegawai"><i class="fa fa-users"></i>Pegawai</a></li>
                                    <li class="nav child_menu"><a href="Notadinas"><i class="fa fa-keyboard-o"></i> Nota Dinas</a></li>
                                    <li class="nav child_menu"><a href="Surattugas"><i class="fa fa-keyboard-o"></i> Surat Tugas (ST)</a></li>
                                    <li class="nav child_menu"><a href="Demo"><i class="fa fa-keyboard-o"></i> Surat Perjalanan Dinas</a></li>
									<!-- <li><a href="Kelengkapantugas"><i class="fa fa-files-o"></i> Kelengkapan Perjalanan Dinas (SPPD)</a></li> -->
									<li><a href="Pembayaran"><i class="fa fa-money"></i> Pembayaran (SPPD)</a></li>
							   
							   <li><a><i class="fa fa-money"></i> Satuan Biaya Harian <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none;">
                                        <li><a href="Master">Nominal Pembayaran</a></li>   
										
                                    </ul>
                                </li>
								<!-- <li><a href="media.php?module=lapakhir"><i class="fa fa-line-chart"></i> Rekap Laporan</a></li> -->
								<li><a href="Admin"><i class="fa fa-user"></i> Admin Aplikasi</a></li>
								<!--<li><a href="media.php?module=laporan"><i class="fa fa-table"></i> Laporan </a></li>-->
                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->

                  
                </div>
            </div>