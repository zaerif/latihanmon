<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demo extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->library('datatables');
        $this->load->model('m_list');
    }
	public function index()
	{
        if($this->session->userdata('logged_in')!="" && $this->session->userdata('stts')=="1"){
            $d['mst_st'] = $this->db->get('surat_tugas');
            $d['mst_pegawai'] = $this->db->query('select * from tbl_user where id not in (select pegawai from list)');
        	$this->load->view('home/pegawai_list',$d);
        } else if($this->session->userdata('logged_in')!=""){
            $d['mst_st'] = $this->db->get('surat_tugas');
            $d['mst_pegawai'] = $this->db->query('select * from tbl_user where id not in (select pegawai from list)');
            $this->load->view('home/pegawai_listuser',$d);
        } else {
            header('location:'.base_url().'');
        }
	}


    function get_pegawai2(){
        $kobar=$this->input->get('id');
        $query = $this->db->query("select * from surat_tugas where no = '".$kobar."'");

        $row = $query->row();
        if (isset($row))
        {
            $d['dari'] = $row->dari; 
            $d['ke'] = $row->ke; 
        }
       
        echo json_encode($d);
    }

    function get_pegawai3(){
        $kobar=$this->input->get('id');

        $query = $this->db->query("select * from surat_tugas where no = '".$kobar."'");

        $row = $query->row();
        $data=$this->m_list->get_pegawailist($row->dari);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function detail()
    {
        $d['mst_st'] = $this->db->get('surat_tugas');
        $d['mst_pegawai'] = $this->db->query('select * from tbl_user where id not in (select pegawai from list)');
        $this->load->view('home/pegawai_list',$d);

    }

	public function get_pegawai_json(){
        header('Content-Type: application/json');
        echo $this->m_list->pegawai_list();

	}

    function get_pegawai(){
        $kobar=$this->input->get('id');
        $data=$this->m_list->get_pegawai_by_kode($kobar);
        echo json_encode($data);
    }

    function hapus_pegawai(){
        $kobar=$this->input->post('kode');
        $data=$this->m_list->hapus_pegawai($kobar);
        echo json_encode($data);
    }

    function update_pegawai(){
        $a2=$this->input->post('a1');
        $b2=$this->input->post('b1');
        $c2=$this->input->post('c1');
        $d2=$this->input->post('d1');
        $ko=$this->input->post('kode');
        $data=$this->m_list->update_pegawai($a2,$b2,$c2,$d2,$ko);
        echo json_encode($data);
    }

    function simpan_pegawai(){
        $a2=$this->input->post('a1');
        $b2=$this->input->post('b1');
        $c2=$this->input->post('c1');
        $d2=$this->input->post('d1');
        $data=$this->m_list->simpan_pegawai($a2,$b2,$c2,$d2);
        echo json_encode($data);
    }

}
