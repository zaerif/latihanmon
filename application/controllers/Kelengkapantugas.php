<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelengkapantugas extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->library('datatables');
        $this->load->model('kelengkapan');
    }
	public function index()
	{
        $d['mst_pegawai'] = $this->db->get('tbl_user');
        $d['mst_daerah'] = $this->db->get('master_gol');
		$this->load->view('home/kelengkapan',$d);

	}

	public function get_surat_json(){
        header('Content-Type: application/json');
        echo $this->kelengkapan->surat_list();

	}

    function get_surat(){
        $kobar=$this->input->get('id');
        $data=$this->kelengkapan->get_surat_by_kode($kobar);
        echo json_encode($data);
    }

    function hapus_surat(){
        $kobar=$this->input->post('kode');
        $data=$this->kelengkapan->hapus_surat($kobar);
        echo json_encode($data);
    }

    function simpan_surat(){
        $a2=$this->input->post('a1');
        $b2=$this->input->post('b1');
        $c2=$this->input->post('c1');
        $d2=$this->input->post('d1');
        $e2=$_FILES['e1'];
        $f2=$_FILES['f1'];
        $g2=$_FILES['g1'];
        $h2=$_FILES['h1'];
        $i2=$this->input->post('i1');
        $data=$this->kelengkapan->simpan_surat($a2,$b2,$c2,$d2,$e2,$f2,$g2,$h2,$i2);
        echo json_encode($data);
    }
    function simpan_surat2(){
        $a2=$this->input->post('a1');
        $b2=$this->input->post('b1');
        $i2=$this->input->post('i1');
        $data=$this->kelengkapan->simpan_surat2($a2,$b2,$i2);
        echo json_encode($data);
    }

}
