<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->library('datatables');
        $this->load->model('m_list');
    }
	public function index()
	{
        if($this->session->userdata('logged_in')!="" && $this->session->userdata('stts')=="1"){
            $a2=$this->input->get('id');
            $a1=$this->input->get('kode');

            $query = $this->db->query("select a.no,b.nama from list a left join tbl_user b on a.pegawai = b.id where a.id = '".$a1."'");

            $row = $query->row();

            if (isset($row))
            {
                $d['no'] = $row->no; 
                $d['nama'] = $row->nama; 
            }
            $d['kodeid']=$a1;
            $d['kodepeg']=$a2;
            $d['mst_st'] = $this->db->get('surat_tugas');
            $query2 = $this->db->query("select a.* from kelengkapan a left join tbl_user b on a.pegawai = b.id where a.pegawai ='".$a2."' AND a.no ='".$row->no."'");
    		$row2 = $query2->row();
                $d['tiket'] = isset($row2->nom_tiket) ? $row2->nom_tiket : '0'; 
                $d['transport'] = isset($row2->nom_transport) ? $row2->nom_transport : '0'; 
                $d['hotel'] = isset($row2->nom_hotel) ? $row2->nom_hotel : '0'; 
                $d['dtiket'] = isset($row2->doc_tiket) ? $row2->doc_tiket : ''; 
                $d['dtransport'] = isset($row2->doc_transport) ? $row2->doc_transport : ''; 
                $d['dhotel'] = isset($row2->doc_hotel) ? $row2->doc_hotel : ''; 
                $d['dkerja'] = isset($row2->doc_kerja) ? $row2->doc_kerja : ''; 
                $d['belakang'] = isset($row2->belakang) ? $row2->belakang : '##########################################################################################'; 
            

            $this->load->view('home/pegawai_list2',$d);
        } else if($this->session->userdata('logged_in')!=""){
            $a2=$this->session->userdata('id_user');
            $a1=$this->input->get('kode');

            $query = $this->db->query("select a.no,b.nama from list a left join tbl_user b on a.pegawai = b.id where a.id = '".$a1."'");

            $row = $query->row();

            if (isset($row))
            {
                $d['no'] = $row->no; 
                $d['nama'] = $row->nama; 
            }
            $d['kodeid']=$a1;
            $d['kodepeg']=$a2;
            $d['mst_st'] = $this->db->get('surat_tugas');
            $query2 = $this->db->query("select a.* from kelengkapan a left join tbl_user b on a.pegawai = b.id where a.pegawai ='".$a2."' AND a.no ='".$row->no."'");
            $row2 = $query2->row();
            $d['tiket'] = isset($row2->nom_tiket) ? $row2->nom_tiket : '0'; 
            $d['transport'] = isset($row2->nom_transport) ? $row2->nom_transport : '0'; 
            $d['hotel'] = isset($row2->nom_hotel) ? $row2->nom_hotel : '0'; 
            $d['dtiket'] = isset($row2->doc_tiket) ? $row2->doc_tiket : ''; 
            $d['dtransport'] = isset($row2->doc_transport) ? $row2->doc_transport : ''; 
            $d['dhotel'] = isset($row2->doc_hotel) ? $row2->doc_hotel : ''; 
            $d['dkerja'] = isset($row2->doc_kerja) ? $row2->doc_kerja : ''; 
            $d['belakang'] = isset($row2->belakang) ? $row2->belakang : '##########################################################################################'; 
        

            $this->load->view('home/pegawai_list2',$d);

        } else {
            header('location:'.base_url().'');
        }

	}

	public function get_pegawai_json(){
        header('Content-Type: application/json');
        echo $this->m_list->pegawai_list();

	}

    function get_pegawai(){
        $kobar=$this->input->get('id');
        $data=$this->m_list->get_pegawai_by_kode($kobar);
        echo json_encode($data);
    }

    function hapus_pegawai(){
        $kobar=$this->input->post('kode');
        $data=$this->m_list->hapus_pegawai($kobar);
        echo json_encode($data);
    }

    function update_pegawai(){
        $a2=$this->input->post('a1');
        $b2=$this->input->post('b1');
        $c2=$this->input->post('c1');
        $d2=$this->input->post('d1');
        $ko=$this->input->post('kode');
        $data=$this->m_list->update_pegawai($a2,$b2,$c2,$d2,$ko);
        echo json_encode($data);
    }

    function simpan_pegawai(){
        $a2=$this->input->post('a1');
        $b2=$this->input->post('b1');
        $c2=$this->input->post('c1');
        $d2=$this->input->post('d1');
        $data=$this->m_list->simpan_pegawai($a2,$b2,$c2,$d2);
        echo json_encode($data);
    }

}
