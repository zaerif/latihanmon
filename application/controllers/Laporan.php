<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->load->helper('terbilang_helper');
	}
	public function index()
	{
		$this->load->view('home/home');

	}


	public function belakang()
	{

        $this->load->library('pdf');

        $this->pdf->setPaper('F4', 'potrait');
        $this->pdf->filename = "pembayaran.pdf";
        $this->pdf->load_view('l_belakang');

	}


	public function belakang2()
	{

        $id = $this->uri->segment(3);
        $query2 = $this->db->query("select a.no,a.pegawai from list a where a.id = '".$id."'");
        $row2 = $query2->row();
        $query = $this->db->query("select belakang from kelengkapan a where a.no='".$row2->no."' and a.pegawai='".$row2->pegawai."'");

        $row = $query->row();

        if (isset($row))
        {
            $d['belakang'] = $row->belakang; 
        }
        $this->load->library('pdf');

        $this->pdf->setPaper('F4', 'potrait');
        $this->pdf->filename = "pembayaran.pdf";
        $this->pdf->load_view('l_belakang2',$d);

	}


	public function surat()
	{

        $id = $this->uri->segment(3);
        $query2 = $this->db->query("select a.no from list a where a.id = '".$id."'");
        $row2 = $query2->row();
        $query = $this->db->query("select a.*,DATEDIFF(b.ke,b.dari)+1 as lama,(select z.nama from tbl_user z where a.penyetuju = z.id) penye, group_concat(c.nama) nama, c.jabatan, c.golongan from surat_tugas a left join list b on a.no = b.no left join tbl_user c on b.pegawai = c.id where a.no='".$row2->no."'");

        $row = $query->row();

        if (isset($row))
        {
            $strText3 = str_replace("\n","<br>",$row->untuk);
            $date1 = DateTime::createFromFormat('Y-m-d', $row->dari);
            $date2 = DateTime::createFromFormat('Y-m-d', $row->ke);
            $d['daerah'] = $row->daerah; 
            $d['dari'] = $date1->format('d F Y'); 
            $d['no'] = $row->no;  
            $d['penye'] = $row->penye; 
            $d['ke'] = $date2->format('d F Y'); 
            $d['nama'] = $row->nama; 
            $d['jabatan'] = $row->jabatan; 
            $d['golongan'] = $row->golongan; 
            $d['untuk'] = $strText3; 
            $d['lama'] = $row->lama; 
        }

        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "st.pdf";
        $this->pdf->load_view('l_surattugas',$d);

	}


	public function globals()
	{

        $d['laporan'] = $this->db->query("select b.nama,DATEDIFF(e.ke,e.dari)+1 as lama, c.daerah,FORMAT(a.hotel,0) hotel,FORMAT(a.harian,0) harian,FORMAT(d.bandara,0) bandara,FORMAT(d.local,0) local,FORMAT(sum(a.hotel+a.harian+d.bandara+d.local),0) total from pembayaran a left join tbl_user b on a.pegawai = b.id left join list e on a.no = e.no and a.pegawai = e.pegawai left join surat_tugas c on a.`no` = c.`no` left join master_gol d on c.daerah = d.nama group by a.no,a.pegawai");

        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "global.pdf";
        $this->pdf->load_view('l_global',$d);

	}

	public function notadinas()
	{

        $id = $this->uri->segment(3);
        $query = $this->db->query("select a.*,(select z.nama from tbl_user z where a.notapenye = z.id) penye,(select z.jabatan from tbl_user z where a.notapenye = z.id) jabpenye from nota_dinas a where a.id='".$id."'");

        $row = $query->row();

        $strText3 = str_replace("\n","<br>",$row->isian);
        if (isset($row))
        {
            $date1 = DateTime::createFromFormat('Y-m-d', $row->tanggal);
            $d['dari'] = $date1->format('d F Y'); 
            $d['isian'] = $strText3; 
            $d['perihal'] = $row->perihal; 
            $d['no'] = $row->notano; 
            $d['jabpenye'] = $row->jabpenye; 
            $d['penye'] = $row->penye; 
        }

        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "notadinas.pdf";
        $this->pdf->load_view('l_notadinas', $d);

	}

	public function laporan_pdf(){

	}

}
