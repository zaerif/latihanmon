<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->library('datatables');
        $this->load->model('m_admin');
    }
	public function index()
	{
        if($this->session->userdata('logged_in')!="" && $this->session->userdata('stts')=="1"){
		    $this->load->view('home/admin');
        } else if($this->session->userdata('logged_in')!=""){
            $this->load->view('home/admin');
        } else {
            header('location:'.base_url().'');
        }

	}

	public function get_pegawai_json(){
        header('Content-Type: application/json');
        echo $this->m_admin->pegawai_list();

	}

    function get_pegawai(){
        $kobar=$this->input->get('id');
        $data=$this->m_admin->get_pegawai_by_kode($kobar);
        echo json_encode($data);
    }

    function hapus_pegawai(){
        $kobar=$this->input->post('kode');
        $data=$this->m_admin->hapus_pegawai($kobar);
        echo json_encode($data);
    }

    function update_pegawai(){
        $a2=$this->input->post('a1');
        $b2=$this->input->post('b1');
        $c2=$this->input->post('c1');
        $d2=$this->input->post('d1');
        $ko=$this->input->post('kode');
        $data=$this->m_admin->update_pegawai($a2,$b2,$c2,$d2,$ko);
        echo json_encode($data);
    }

    function simpan_pegawai(){
        $a2=$this->input->post('a1');
        $b2=$this->input->post('b1');
        $c2=$this->input->post('c1');
        $d2=$this->input->post('d1');
        $data=$this->m_admin->simpan_pegawai($a2,$b2,$c2,$d2);
        echo json_encode($data);
    }

}
