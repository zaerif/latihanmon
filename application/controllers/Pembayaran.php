<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->library('datatables');
        $this->load->model('m_pembayaran');
        $this->load->helper('terbilang_helper');
    }
	public function index()
	{
        if($this->session->userdata('logged_in')!="" && $this->session->userdata('stts')=="1"){
            $d['mst_pegawai'] = $this->db->get('tbl_user');
            $d['mst_daerah'] = $this->db->get('master_gol');
            $this->load->view('home/pembayaran',$d);
        } else {
            header('location:'.base_url().'');
        }

	}

	public function get_surat_json(){
        header('Content-Type: application/json');
        echo $this->m_pembayaran->surat_list();

	}

    function get_surat(){
        $kobar=$this->input->get('id');
        $data=$this->m_pembayaran->get_surat_by_kode($kobar);
        echo json_encode($data);
    }


    public function prints(){
        $a2=$this->input->get('kode');

        $query = $this->db->query("select a.*,(select z.jabatan from tbl_user z where a.penyetuju = z.id) jabpenye,(select z.nip from tbl_user z where a.penyetuju = z.id) nippenye,(select z.nama from tbl_user z where a.penyetuju = z.id) penye,(select z.nama from tbl_user z where a.bendahara = z.id) benda, b.nama,b.nip,b.jabatan,b.golongan,c.daerah,DATEDIFF(c.ke,c.dari)+1 as lama,c.dari, c.ke,d.harian mharian,d.bandara,d.local from pembayaran a left join tbl_user b on a.pegawai = b.id left join surat_tugas c on a.no = c.no left join master_gol d on c.daerah = d.nama where a.id='".$a2."' group by a.no;");

        $row = $query->row();
        if (isset($row))
        {
            $date1 = DateTime::createFromFormat('Y-m-d', $row->dari);
            $date2 = DateTime::createFromFormat('Y-m-d', $row->ke);
            $total = $row->harian + $row->hotel + $row->tiket;
            $total1 = $row->bandara + $row->local + $row->res;
            $strText = str_replace("\n","<br>",$row->jabpenye);
            $ttotal = penyebut($total);
            $totalall = $total + $total1;
            $ttotalall = penyebut($totalall);
            $d['daerah'] = $row->daerah; 
            $d['nama'] = $row->nama; 
            $d['nip'] = isset($row->nip) ? $row->nip : '-'; 
            $d['jabatan'] = $row->jabatan; 
            $d['dari'] = $date1->format('d M Y'); 
            $d['no'] = $row->no; 
            $d['lama'] = $row->lama; 
            $d['ke'] = $date2->format('d-m-Y'); 
            $d['hotel'] = number_format($row->hotel,0); 
            $d['tiket'] = number_format($row->tiket,0); 
            $d['harian'] = number_format($row->harian,0); 
            $d['mharian'] = number_format($row->mharian,0); 
            $d['transport'] = number_format($row->transport,0); 
            $d['res'] = number_format($row->res,0); 
            $d['bandara'] = number_format($row->bandara,0); 
            $d['local'] = number_format($row->local,0); 
            $d['total'] = number_format($total,0);  
            $d['ttotal'] = $ttotal; 
            $d['total1'] = number_format($total1,0); 
            $d['penyetuju'] = $row->penye; 
            $d['nippenyetuju'] = $row->nippenye; 
            $d['jabpenyetuju'] = $strText; 
            $d['bendahara'] = $row->benda; 
            $d['totalall'] = number_format($totalall,0); 
            $d['ttotalall'] = $ttotalall; 
        }
        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "pembayaran.pdf";
        $this->pdf->load_view('l_pembayaran',$d);

    }

    public function printshome(){
        $a2=$this->input->get('kode');
        $a1=$this->input->get('id');
        $query2 = $this->db->query("select a.* from list a where a.id='".$a2."' group by a.no;");

        $row2 = $query2->row();

        $query = $this->db->query("select a.*,(select z.jabatan from tbl_user z where a.penyetuju = z.id) jabpenye,(select z.nip from tbl_user z where a.penyetuju = z.id) nippenye,(select z.nama from tbl_user z where a.penyetuju = z.id) penye,(select z.nama from tbl_user z where a.bendahara = z.id) benda, b.nama,b.nip,b.jabatan,b.golongan,c.daerah,DATEDIFF(c.ke,c.dari)+1 as lama,c.dari, c.ke,d.harian mharian,d.bandara,d.local from pembayaran a left join tbl_user b on a.pegawai = b.id left join surat_tugas c on a.no = c.no left join master_gol d on c.daerah = d.nama where a.no='".$row2->no."' and a.pegawai ='".$a1."' group by a.no;");

        $row = $query->row();
        if (isset($row))
        {
            $date1 = DateTime::createFromFormat('Y-m-d', $row->dari);
            $date2 = DateTime::createFromFormat('Y-m-d', $row->ke);
            $total = $row->harian + $row->hotel + $row->tiket;
            $total1 = $row->bandara + $row->local + $row->res;
            $strText = str_replace("\n","<br>",$row->jabpenye);
            $ttotal = penyebut($total);
            $totalall = $total + $total1;
            $ttotalall = penyebut($totalall);
            $d['daerah'] = $row->daerah; 
            $d['nama'] = $row->nama; 
            $d['nip'] = isset($row->nip) ? $row->nip : '-'; 
            $d['jabatan'] = $row->jabatan; 
            $d['dari'] = $date1->format('d M Y'); 
            $d['no'] = $row->no; 
            $d['lama'] = $row->lama; 
            $d['ke'] = $date2->format('d-m-Y'); 
            $d['hotel'] = number_format($row->hotel,0); 
            $d['tiket'] = number_format($row->tiket,0); 
            $d['harian'] = number_format($row->harian,0); 
            $d['mharian'] = number_format($row->mharian,0); 
            $d['transport'] = number_format($row->transport,0); 
            $d['res'] = number_format($row->res,0); 
            $d['bandara'] = number_format($row->bandara,0); 
            $d['local'] = number_format($row->local,0); 
            $d['total'] = number_format($total,0);  
            $d['ttotal'] = $ttotal; 
            $d['total1'] = number_format($total1,0); 
            $d['penyetuju'] = $row->penye; 
            $d['nippenyetuju'] = $row->nippenye; 
            $d['jabpenyetuju'] = $strText; 
            $d['bendahara'] = $row->benda; 
            $d['totalall'] = number_format($totalall,0); 
            $d['ttotalall'] = $ttotalall; 
        }
        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "pembayaran.pdf";
        $this->pdf->load_view('l_pembayaran',$d);

    }


    public function printshomev1(){
        $a2=$this->input->get('kode');
        $a1=$this->input->get('id');
        $query2 = $this->db->query("select a.* from list a where a.id='".$a2."' group by a.no;");

        $row2 = $query2->row();

        $query = $this->db->query("select a.*,(select z.jabatan from tbl_user z where a.penyetuju = z.id) jabpenye,(select z.nip from tbl_user z where a.penyetuju = z.id) nippenye,(select z.nama from tbl_user z where a.penyetuju = z.id) penye,(select z.nama from tbl_user z where a.bendahara = z.id) benda, b.nama,b.nip,b.jabatan,b.golongan,c.daerah,DATEDIFF(c.ke,c.dari)+1 as lama,c.dari, c.ke,d.harian mharian,d.bandara,d.local from pembayaran a left join tbl_user b on a.pegawai = b.id left join surat_tugas c on a.no = c.no left join master_gol d on c.daerah = d.nama where a.no='".$row2->no."' and a.pegawai ='".$a1."' group by a.no;");

        $row = $query->row();
        if (isset($row))
        {
            $date1 = DateTime::createFromFormat('Y-m-d', $row->dari);
            $date2 = DateTime::createFromFormat('Y-m-d', $row->ke);
            $total = $row->harian + $row->hotel + $row->tiket;
            $total1 = $row->bandara + $row->local + $row->res;
            $strText = str_replace("\n","<br>",$row->jabpenye);
            $ttotal = penyebut($total);
            $totalall = $total + $total1;
            $ttotalall = penyebut($totalall);
            $d['daerah'] = $row->daerah; 
            $d['nama'] = $row->nama; 
            $d['nip'] = isset($row->nip) ? $row->nip : '-'; 
            $d['jabatan'] = $row->jabatan; 
            $d['dari'] = $date1->format('d M Y'); 
            $d['no'] = $row->no; 
            $d['lama'] = $row->lama; 
            $d['ke'] = $date2->format('d-m-Y'); 
            $d['hotel'] = number_format($row->hotel,0); 
            $d['tiket'] = number_format($row->tiket,0); 
            $d['harian'] = number_format($row->harian,0); 
            $d['mharian'] = number_format($row->mharian,0); 
            $d['transport'] = number_format($row->transport,0); 
            $d['res'] = number_format($row->res,0); 
            $d['bandara'] = number_format($row->bandara,0); 
            $d['local'] = number_format($row->local,0); 
            $d['total'] = number_format($total,0);  
            $d['ttotal'] = $ttotal; 
            $d['total1'] = number_format($total1,0); 
            $d['penyetuju'] = $row->penye; 
            $d['nippenyetuju'] = $row->nippenye; 
            $d['jabpenyetuju'] = $strText; 
            $d['bendahara'] = $row->benda; 
            $d['totalall'] = number_format($totalall,0); 
            $d['ttotalall'] = $ttotalall; 
        }
        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "pembayaran.pdf";
        $this->pdf->load_view('l_kwitansi',$d);

    }

    public function printshomev2(){
        $a2=$this->input->get('kode');
        $a1=$this->input->get('id');
        $query2 = $this->db->query("select a.* from list a where a.id='".$a2."' group by a.no;");

        $row2 = $query2->row();

        $query = $this->db->query("select a.*,(select z.jabatan from tbl_user z where a.penyetuju = z.id) jabpenye,(select z.nip from tbl_user z where a.penyetuju = z.id) nippenye,(select z.nama from tbl_user z where a.penyetuju = z.id) penye,(select z.nama from tbl_user z where a.bendahara = z.id) benda, b.nama,b.nip,b.jabatan,b.golongan,c.daerah,DATEDIFF(c.ke,c.dari)+1 as lama,c.dari, c.ke,d.harian mharian,d.bandara,d.local from pembayaran a left join tbl_user b on a.pegawai = b.id left join surat_tugas c on a.no = c.no left join master_gol d on c.daerah = d.nama where a.no='".$row2->no."' and a.pegawai ='".$a1."' group by a.no;");

        $row = $query->row();
        if (isset($row))
        {
            $date1 = DateTime::createFromFormat('Y-m-d', $row->dari);
            $date2 = DateTime::createFromFormat('Y-m-d', $row->ke);
            $total = $row->harian + $row->hotel + $row->tiket;
            $total1 = $row->bandara + $row->local + $row->res;
            $strText = str_replace("\n","<br>",$row->jabpenye);
            $ttotal = penyebut($total);
            $totalall = $total + $total1;
            $ttotalall = penyebut($totalall);
            $d['daerah'] = $row->daerah; 
            $d['nama'] = $row->nama; 
            $d['nip'] = isset($row->nip) ? $row->nip : '-'; 
            $d['jabatan'] = $row->jabatan; 
            $d['dari'] = $date1->format('d M Y'); 
            $d['no'] = $row->no; 
            $d['lama'] = $row->lama; 
            $d['ke'] = $date2->format('d-m-Y'); 
            $d['hotel'] = number_format($row->hotel,0); 
            $d['tiket'] = number_format($row->tiket,0); 
            $d['harian'] = number_format($row->harian,0); 
            $d['mharian'] = number_format($row->mharian,0); 
            $d['transport'] = number_format($row->transport,0); 
            $d['res'] = number_format($row->res,0); 
            $d['bandara'] = number_format($row->bandara,0); 
            $d['local'] = number_format($row->local,0); 
            $d['total'] = number_format($total,0);  
            $d['ttotal'] = $ttotal; 
            $d['total1'] = number_format($total1,0); 
            $d['penyetuju'] = $row->penye; 
            $d['nippenyetuju'] = $row->nippenye; 
            $d['jabpenyetuju'] = $strText; 
            $d['bendahara'] = $row->benda; 
            $d['totalall'] = number_format($totalall,0); 
            $d['ttotalall'] = $ttotalall; 
        }
        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "pembayaran.pdf";
        $this->pdf->load_view('l_rincian',$d);

    }

    public function printshomev3(){
        $a2=$this->input->get('kode');
        $a1=$this->input->get('id');
        $query2 = $this->db->query("select a.* from list a where a.id='".$a2."' group by a.no;");

        $row2 = $query2->row();

        $query = $this->db->query("select a.*,(select z.jabatan from tbl_user z where a.penyetuju = z.id) jabpenye,(select z.nip from tbl_user z where a.penyetuju = z.id) nippenye,(select z.nama from tbl_user z where a.penyetuju = z.id) penye,(select z.nama from tbl_user z where a.bendahara = z.id) benda, b.nama,b.nip,b.jabatan,b.golongan,c.daerah,DATEDIFF(c.ke,c.dari)+1 as lama,c.dari, c.ke,d.harian mharian,d.bandara,d.local from pembayaran a left join tbl_user b on a.pegawai = b.id left join surat_tugas c on a.no = c.no left join master_gol d on c.daerah = d.nama where a.no='".$row2->no."' and a.pegawai ='".$a1."' group by a.no;");

        $row = $query->row();
        if (isset($row))
        {
            $date1 = DateTime::createFromFormat('Y-m-d', $row->dari);
            $date2 = DateTime::createFromFormat('Y-m-d', $row->ke);
            $total = $row->harian + $row->hotel + $row->tiket;
            $total1 = $row->bandara + $row->local + $row->res;
            $strText = str_replace("\n","<br>",$row->jabpenye);
            $ttotal = penyebut($total);
            $totalall = $total + $total1;
            $ttotalall = penyebut($totalall);
            $d['daerah'] = $row->daerah; 
            $d['nama'] = $row->nama; 
            $d['nip'] = isset($row->nip) ? $row->nip : '-'; 
            $d['jabatan'] = $row->jabatan; 
            $d['dari'] = $date1->format('d M Y'); 
            $d['no'] = $row->no; 
            $d['lama'] = $row->lama; 
            $d['ke'] = $date2->format('d-m-Y'); 
            $d['hotel'] = number_format($row->hotel,0); 
            $d['tiket'] = number_format($row->tiket,0); 
            $d['harian'] = number_format($row->harian,0); 
            $d['mharian'] = number_format($row->mharian,0); 
            $d['transport'] = number_format($row->transport,0); 
            $d['res'] = number_format($row->res,0); 
            $d['bandara'] = number_format($row->bandara,0); 
            $d['local'] = number_format($row->local,0); 
            $d['total'] = number_format($total,0);  
            $d['ttotal'] = $ttotal; 
            $d['total1'] = number_format($total1,0); 
            $d['penyetuju'] = $row->penye; 
            $d['nippenyetuju'] = $row->nippenye; 
            $d['jabpenyetuju'] = $strText; 
            $d['bendahara'] = $row->benda; 
            $d['totalall'] = number_format($totalall,0); 
            $d['ttotalall'] = $ttotalall; 
        }
        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "pembayaran.pdf";
        $this->pdf->load_view('l_drill',$d);

    }

    function simpan_surat(){
        $a2=$this->input->post('a1');
        $b2=$this->input->post('b1');
        $c2=$this->input->post('c1');
        $d2=$this->input->post('d1');
        $e2=$this->input->post('e1');
        $f2=$this->input->post('f1');
        $g2=$this->input->post('g1');
        $h2=$this->input->post('h1');
        $i2=$this->input->post('i1');
        $j2=$this->input->post('j1');
        $k2=$this->input->post('k1');
        $data=$this->m_pembayaran->simpan_surat($a2,$b2,$c2,$d2,$e2,$f2,$g2,$h2,$i2,$j2,$k2);
        echo json_encode($data);
    }

}
