<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surattugas extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->library('datatables');
        $this->load->model('m_surattugas');
    }
	public function index()
	{
        if($this->session->userdata('logged_in')!="" && $this->session->userdata('stts')=="1"){
            $d['mst_pegawai'] = $this->db->get('tbl_user');
            $d['mst_daerah'] = $this->db->get('master_gol');
    		$this->load->view('home/surattugas',$d);
        } else {
            header('location:'.base_url().'');
        }

	}

	public function get_surat_json(){
        header('Content-Type: application/json');
        echo $this->m_surattugas->surat_list();

	}

    public function prints2(){
        $id = $this->uri->segment(3);
        $query = $this->db->query("select a.*, group_concat(c.nama) nama from surat_tugas a left join list b on a.no = b.no left join tbl_user c on b.pegawai = c.id where a.id='".$id."'");

        $row = $query->row();

        if (isset($row))
        {
            $strText = str_replace("\n","<br>",$row->menimbang);
            $strText2 = str_replace("\n","<br>",$row->dasar);
            $strText3 = str_replace("\n","<br>",$row->untuk);
            $date1 = DateTime::createFromFormat('Y-m-d', $row->dari);
            $date2 = DateTime::createFromFormat('Y-m-d', $row->ke);
            $d['daerah'] = $row->daerah; 
            $d['menimbang'] = $strText; 
            $d['dasar'] = $strText2; 
            $d['untuk'] = $strText3; 
            $d['dari'] = $date1->format('d-m-Y'); 
            $d['no'] = $row->no; 
            $d['ke'] = $date2->format('d-m-Y'); 
            $d['nama'] = $row->nama; 
            $query2 = $this->db->query("select a.nama, a.nip from tbl_user a where a.id = '".$row->penyetuju."'");
            $row2 = $query2->row();
            $d['penyetuju'] = $row2->nama;
            $d['nippenyetuju'] = $row2->nip;
        }
        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "pembayaran.pdf";
        $this->pdf->load_view('laporan_pdf', $d);

    }

    public function prints(){
        $id = $this->uri->segment(3);
        $query2 = $this->db->query("select a.no from list a where a.id = '".$id."'");
        $row2 = $query2->row();
        $query = $this->db->query("select a.*, group_concat(c.nama) nama from surat_tugas a left join list b on a.no = b.no left join tbl_user c on b.pegawai = c.id where a.no='".$row2->no."'");

        $row = $query->row();

        if (isset($row))
        {
            $strText = str_replace("\n","<br>",$row->menimbang);
            $strText2 = str_replace("\n","<br>",$row->dasar);
            $strText3 = str_replace("\n","<br>",$row->untuk);
            $date1 = DateTime::createFromFormat('Y-m-d', $row->dari);
            $date2 = DateTime::createFromFormat('Y-m-d', $row->ke);
            $d['daerah'] = $row->daerah; 
            $d['menimbang'] = $strText; 
            $d['dasar'] = $strText2; 
            $d['untuk'] = $strText3; 
            $d['dari'] = $date1->format('d-m-Y'); 
            $d['no'] = $row->no; 
            $d['ke'] = $date2->format('d-m-Y'); 
            $d['nama'] = $row->nama; 
        }
        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "pembayaran.pdf";
        $this->pdf->load_view('laporan_pdf', $d);

    }

    function get_surat(){
        $kobar=$this->input->get('id');
        $data=$this->m_surattugas->get_surat_by_kode($kobar);
        echo json_encode($data);
    }

    function hapus_surat(){
        $kobar=$this->input->post('kode');
        $data=$this->m_surattugas->hapus_surat($kobar);
        echo json_encode($data);
    }

    function update_surat(){
        $a2=$this->input->post('a1');
        $b2=$this->input->post('b1');
        $c2=$this->input->post('c1');
        $d2=$this->input->post('d1');
        $e2=$this->input->post('e1');
        $f2=$this->input->post('f1');
        $g2=$this->input->post('g1');
        $h2=$this->input->post('h1');
        $i2=$this->input->post('i1');
        $j2=$this->input->post('j1');
        $k2=$this->input->post('k1');
        $l2=$this->input->post('l1');
        $ko=$this->input->post('kode');
        $data=$this->m_surattugas->update_surat($a2,$b2,$c2,$d2,$e2,$f2,$g2,$h2,$i2,$j2,$k2,$l2,$ko);
        echo json_encode($data);
    }

    function simpan_surat(){
        $a2=$this->input->post('a1');
        $b2=$this->input->post('b1');
        $c2=$this->input->post('c1');
        $d2=$this->input->post('d1');
        $e2=$this->input->post('e1');
        $f2=$this->input->post('f1');
        $g2=$this->input->post('g1');
        $h2=$this->input->post('h1');
        $i2=$this->input->post('i1');
        $j2=$this->input->post('j1');
        $k2=$this->input->post('k1');
        $l2=$this->input->post('l1');
        $data=$this->m_surattugas->simpan_surat($a2,$b2,$c2,$d2,$e2,$f2,$g2,$h2,$i2,$j2,$k2,$l2);
        echo json_encode($data);
    }

}
