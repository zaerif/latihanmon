<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
	}
	public function index()
	{
        if($this->session->userdata('logged_in')!="" && $this->session->userdata('stts')=="1"){
			$tot_hal = $this->db->query("select * from tbl_user");
			$d['pegawai'] = $tot_hal->num_rows();
			$tot_hal1 = $this->db->query("select * from list");
			$d['jalan'] = $tot_hal1->num_rows();
			$tot_hal2 = $this->db->query("select * from list where ke <='".date('Y-m-d')."'");
			$tot_hal3 = $this->db->query("select FORMAT(sum(a.hotel+a.harian+d.bandara+d.local),0) total  from pembayaran a left join tbl_user b on a.pegawai = b.id left join surat_tugas c on a.`no` = c.`no` left join master_gol d on c.daerah = d.nama");
			$d['rampung'] = $tot_hal2->num_rows();
	        $d['mst_pegawai'] = $this->db->get('tbl_user');
	        $d['mst_st'] = $this->db->group_by('no')->get('list');
	        $d['mst_nota'] = $this->db->get('nota_dinas');
			$row = $tot_hal3->row();

	        if (isset($row))
	        {
	            $d['total'] = $row->total; 
	        }
			$this->load->view('home/home',$d);
        } else if($this->session->userdata('logged_in')!=""){
			$id_user = $this->session->userdata('id_user');
			$tot_hal = $this->db->query("select * from tbl_user where id =  '$id_user'");
			$d['pegawai'] = $tot_hal->num_rows();
			$tot_hal1 = $this->db->query("select * from list where pegawai = '$id_user';");
			$d['jalan'] = $tot_hal1->num_rows();
			$tot_hal2 = $this->db->query("select * from list where ke <='".date('Y-m-d')."' and pegawai = '$id_user'");
			$tot_hal3 = $this->db->query("select FORMAT(IFNULL(sum(a.hotel+a.harian+d.bandara+d.local),0),0) total  from pembayaran a left join tbl_user b on a.pegawai = b.id left join surat_tugas c on a.`no` = c.`no` left join master_gol d on c.daerah = d.nama where a.pegawai = '$id_user'");
			$d['rampung'] = $tot_hal2->num_rows();
	        $d['mst_pegawai'] = $this->db->where('id', $id_user)->get('tbl_user');
	        $d['mst_st'] = $this->db->where('pegawai', $id_user)->group_by('no')->get('list');
			$row = $tot_hal3->row();

	        if (isset($row))
	        {
	            $d['total'] = $row->total; 
	        }
            $this->load->view('home/adminpegawai',$d);
        } else {
            header('location:'.base_url().'');
        }

	}

	function get_pegawai3(){
        $kobar=$this->input->get('id');
        $query = $this->db->query("select * from list where id = '".$kobar."'");
        $row = $query->row();

        $query2 = $this->db->query("select b.nama,b.id from list a left join tbl_user b on a.pegawai = b.id where a.no = '".$row->no."'");

        header('Content-Type: application/json');
        echo json_encode($query2->result_array());
    }
    
    function logout()
    {
        $this->session->sess_destroy();
        header('location:'.base_url().'');
    }

	public function laporan_pdf(){

	}

}
