<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notadinas extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->library('datatables');
        $this->load->model('m_notadinas');
    }
	public function index()
	{
        if($this->session->userdata('logged_in')!="" && $this->session->userdata('stts')=="1"){
            $d['mst_pegawai'] = $this->db->get('tbl_user');
            $d['mst_daerah'] = $this->db->get('master_gol');
    		$this->load->view('home/notadinas',$d);
        } else {
            header('location:'.base_url().'');
        }

	}

	public function get_surat_json(){
        header('Content-Type: application/json');
        echo $this->m_notadinas->surat_list();

	}

    public function prints(){

        $id = $this->uri->segment(3);
        $query = $this->db->query("select a.*,(select z.nama from tbl_user z where a.notapenye = z.id) penye,(select z.jabatan from tbl_user z where a.notapenye = z.id) jabpenye from nota_dinas a where a.id='".$id."'");

        $row = $query->row();

        $strText3 = str_replace("\n","<br>",$row->isian);
        if (isset($row))
        {
            $date1 = DateTime::createFromFormat('Y-m-d', $row->tanggal);
            $d['dari'] = $date1->format('d F Y'); 
            $d['isian'] = $strText3; 
            $d['perihal'] = $row->perihal; 
            $d['no'] = $row->notano; 
            $d['jabpenye'] = $row->jabpenye; 
            $d['penye'] = $row->penye; 
        }

        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "notadinas.pdf";
        $this->pdf->load_view('l_notadinas', $d);

    }

    function get_surat(){
        $kobar=$this->input->get('id');
        $data=$this->m_notadinas->get_surat_by_kode($kobar);
        echo json_encode($data);
    }

    function hapus_surat(){
        $kobar=$this->input->post('kode');
        $data=$this->m_notadinas->hapus_surat($kobar);
        echo json_encode($data);
    }

    function update_surat(){
        $i2=$this->input->post('i1');
        $j2=$this->input->post('j1');
        $k2=$this->input->post('k1');
        $l2=$this->input->post('l1');
        $m2=$this->input->post('m1');
        $ko=$this->input->post('kode');
        $data=$this->m_notadinas->update_surat($i2,$j2,$k2,$l2,$m2,$ko);
        echo json_encode($data);
    }

    function simpan_surat(){
        $i2=$this->input->post('i1');
        $j2=$this->input->post('j1');
        $k2=$this->input->post('k1');
        $l2=$this->input->post('l1');
        $m2=$this->input->post('m1');
        $data=$this->m_notadinas->simpan_surat($i2,$j2,$k2,$l2,$m2);
        echo json_encode($data);
    }

}
