<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

  public function __construct()
	{
	    parent::__construct();
	    $this->load->helper('url', 'file');

	}
	public function index()
	{
		if($this->session->userdata('logged_in')==""){			
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('layouts/login');
			}
			else
			{
				$dt['username'] = $this->input->post('username');
				$dt['password'] = $this->input->post('password');
				$this->load->model('user_login');
				$this->user_login->getLoginData($dt);
			}
		} else if($this->session->userdata('logged_in')!="" && $this->session->userdata('stts')=="1"){
			header('location:'.base_url().'Welcome');
		} else {
			header('location:'.base_url().'Adminpegawai');
		}
	}
}