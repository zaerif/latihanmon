<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->library('datatables');
        $this->load->model('m_master');
    }
	public function index()
	{
        if($this->session->userdata('logged_in')!="" && $this->session->userdata('stts')=="1"){
            $d['mst_pegawai'] = $this->db->get('tbl_user');
            $d['mst_daerah'] = $this->db->get('master_gol');
            $this->load->view('home/master',$d);
        } else {
            header('location:'.base_url().'');
        }

	}

	public function get_master_json(){
        header('Content-Type: application/json');
        echo $this->m_master->master_list();

	}

    function get_master(){
        $kobar=$this->input->get('id');
        $data=$this->m_master->get_master_by_kode($kobar);
        echo json_encode($data);
    }

    function hapus_master(){
        $kobar=$this->input->post('kode');
        $data=$this->m_master->hapus_surat($kobar);
        echo json_encode($data);
    }

    function update_master(){
        $a2=$this->input->post('a1');
        $b2=$this->input->post('b1');
        $c2=$this->input->post('c1');
        $d2=$this->input->post('d1');
        $e2=$this->input->post('e1');
        $f2=$this->input->post('f1');
        $g2=$this->input->post('g1');
        $h2=$this->input->post('h1');
        $i2=$this->input->post('i1');
        $j2=$this->input->post('j1');
        $ko=$this->input->post('kode');
        $data=$this->m_master->update_surat($a2,$b2,$c2,$d2,$e2,$f2,$g2,$h2,$i2,$j2,$ko);
        echo json_encode($data);
    }

    function simpan_master(){
        $a2=$this->input->post('a1');
        $b2=$this->input->post('b1');
        $c2=$this->input->post('c1');
        $d2=$this->input->post('d1');
        $e2=$this->input->post('e1');
        $f2=$this->input->post('f1');
        $g2=$this->input->post('g1');
        $h2=$this->input->post('h1');
        $i2=$this->input->post('i1');
        $j2=$this->input->post('j1');
        $data=$this->m_master->simpan_surat($a2,$b2,$c2,$d2,$e2,$f2,$g2,$h2,$i2,$j2);
        echo json_encode($data);
    }

}
