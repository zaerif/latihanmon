<?php
class M_List extends CI_Model{
	private $master = 'list';

	function pegawai_list(){
		if($this->session->userdata('stts')=="1"){
	        $this->datatables->select('id,nama,no,dari');
	        $this->datatables->from("(select a.id,CONCAT(b.id,'##',b.nama,'##',a.id) nama,a.no,CONCAT(DATE_FORMAT(a.dari, '%d-%m-%Y'), ' s/d ' ,DATE_FORMAT(a.ke, '%d-%m-%Y')) as dari from list a left join tbl_user b on a.pegawai = b.id)z");
	        $this->datatables->add_column('view', '<a href="javascript:void(0);" data="$1" class="on-default lihat-row" style="color:blue;margin-right:15px;"><i class="fa fa-print"></i></a><a href="javascript:void(0);" data="$1" class="on-default edit-row" style="color:orange;margin-right:15px;"><i class="fa fa-pencil"></i></a><a href="javascript:void(0);" data="$1" class="on-default hapus-row" style="color:red;margin-right:15px;"><i class="fa fa-trash-o"></i></a>','id');
	        return $this->datatables->generate();
		} else {
			$id_user = $this->session->userdata('id_user');
	        $this->datatables->select('id,nama,no,dari');
	        $this->datatables->from("(select a.id,CONCAT(b.id,'##',b.nama,'##',a.id) nama,a.no,CONCAT(DATE_FORMAT(a.dari, '%d-%m-%Y'), ' s/d ' ,DATE_FORMAT(a.ke, '%d-%m-%Y')) as dari from list a left join tbl_user b on a.pegawai = b.id where a.pegawai = '$id_user')z");
	        $this->datatables->add_column('view', '<a href="javascript:void(0);" data="$1" class="on-default lihat-row" style="color:blue;margin-right:15px;"><i class="fa fa-print"></i></a></a>','id');
	        return $this->datatables->generate();
		}
	}

	function get_pegawai_by_kode($kobar){
		$hsl=$this->db->query("select * from list where id='$kobar'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'a1' => $data->pegawai,
					'b1' => $data->no,
					'c1' => $data->dari,
					'd1' => $data->ke,
					);
			}
		}
		return $hasil;
	}

	function get_pegawailist($kobar){
		$hasil = array();
		$hsl=$this->db->query("select id,nama from tbl_user where id not in (select b.pegawai from list b where b.ke >= '".$kobar."')");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil[]=array(
					'a1' => $data->id,
					'b1' => $data->nama,
					);
			}
		}
		return $hsl->result();
	}

	function hapus_pegawai($kobar){
		if($kobar != $this->session->userdata('id_user')){
			$this->db->query("DELETE FROM pembayaran  WHERE relate='$kobar'");
			$hasil=$this->db->query("DELETE FROM " . $this->master . "  WHERE id='$kobar'");
			return $hasil;
		} else {
			$arr = array('msg' => 'Tidak dapat Menghapus data', 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5); 
    		header('Content-Type: application/json');
			return json_encode($arr);
		}
	}

    function simpan_pegawai($a1,$b1,$c1,$d1){
        $dari = date_format(DateTime::createFromFormat('d/m/Y', $c1),'Y-m-d');
        $ke = date_format(DateTime::createFromFormat('d/m/Y', $d1),'Y-m-d');
        $sql = $sql = "INSERT INTO " . $this->master . "(pegawai,no,dari,ke)"
                . " VALUES(?,?,?,?)";
        $hasil = $this->db->query($sql, array($b1,$a1,$dari,$ke));
        $insert_id = $this->db->insert_id();
        $query = $this->db->query("select DATEDIFF(c.ke,c.dari)+1 as lama,a.harian,a.bandara,a.local,a.res1,a.res2,CASE WHEN d.golongan = 'IVeselon' THEN a.iveselon WHEN d.golongan = 'IVkaro' THEN a.ivkaro WHEN d.golongan = 'IVkabag' THEN a.ivkabag WHEN d.golongan = 'III' then a.iii WHEN d.golongan = 'II' THEN a.ii ELSE 'Tidak Ketemu' END as masterhotel from master_gol a left join surat_tugas b on a.nama = b.daerah left join list c on b.no = c.no left join tbl_user d on c.pegawai = d.id where b.no = '$a1' and c.pegawai = '$b1';");
        $row = $query->row();
        $sql1 = $sql = "INSERT INTO pembayaran (no,hotel,harian,pegawai,relate)"
                . " VALUES(?,?,?,?,?)";
        $harian = $row->lama*$row->harian;
        $hotel = ($row->lama - 1)*$row->masterhotel;
        $this->db->query($sql1, array($a1,$hotel,$harian,$b1,$insert_id));
        return $hasil;
    }

    function update_pegawai($a1,$b1,$c1,$d1,$kode){
        $dari = date_format(DateTime::createFromFormat('d/m/Y', $c1),'Y-m-d');
        $ke = date_format(DateTime::createFromFormat('d/m/Y', $d1),'Y-m-d');
		$this->db->query("UPDATE " . $this->master . " SET pegawai = '$b1',no = '$a1',dari='$dari',ke='$ke'  WHERE id='$kode'");
		$hasil= array('msg', 'Berhasil Mengubah Password');
		return $hasil;
    }
	
}