<?php
class M_Surattugas extends CI_Model{
	private $master = 'surat_tugas';

	function surat_list(){
	        $this->datatables->select('a.id,a.no,CONCAT(DATE_FORMAT(a.dari, "%d-%m-%Y"), " s/d " ,DATE_FORMAT(a.ke, "%d-%m-%Y")) as dari,DATE_FORMAT(a.ke, "%d-%m-%Y") as ke,a.daerah');
	        $this->datatables->from('surat_tugas a');
	        $this->datatables->add_column('view', '<a href="javascript:void(0);" data="$1" class="on-default lihat-row" style="color:blue;margin-right:15px;"><i class="fa fa-print"></i></a><a href="javascript:void(0);" data="$1" class="on-default edit-row" style="color:orange;margin-right:15px;"><i class="fa fa-pencil"></i></a><a href="javascript:void(0);" data="$1" class="on-default hapus-row" style="color:red;margin-right:15px;"><i class="fa fa-trash-o"></i></a>','id');
	        return $this->datatables->generate();
	}

	function get_surat_by_kode($kobar){
		$hsl=$this->db->query("SELECT * FROM surat_tugas WHERE id='$kobar'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'a1' => $data->no,
					'b1' => $data->dari,
					'c1' => $data->daerah,
					'd1' => isset($data->pegawai) ? $data->pegawai : '',
					'e1' => isset($data->ke) ? $data->ke : '',
					'f1' => isset($data->menimbang) ? $data->menimbang : '',
					'g1' => isset($data->dasar) ? $data->dasar : '',
					'h1' => isset($data->untuk) ? $data->untuk : '',
					'i1' => isset($data->notapenye) ? $data->notapenye : '',
					'j1' => isset($data->notano) ? $data->notano : '',
					'k1' => isset($data->perihal) ? $data->perihal : '',
					'l1' => isset($data->isian) ? $data->isian : '',
					);
			}
		}
		return $hasil;
	}

	function hapus_surat($kobar){
		if($kobar != $this->session->userdata('id_user')){
			$hasil=$this->db->query("DELETE FROM " . $this->master . "  WHERE id='$kobar'");
			return $hasil;
		} else {
			$arr = array('msg' => 'Tidak dapat Menghapus data', 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5); 
    		header('Content-Type: application/json');
			return json_encode($arr);
		}
	}

    function simpan_surat($a1,$b1,$c1,$d1,$e1,$f1,$g1,$h1,$i1,$j1,$k1,$l1){
        $user = $this->session->userdata('id_user');
        $dari = date_format(DateTime::createFromFormat('d/m/Y', $g1),'Y-m-d');
        $ke = date_format(DateTime::createFromFormat('d/m/Y', $h1),'Y-m-d');
        if($this->session->userdata('stts') == "4"){
            $a2 = $this->session->userdata('id_user');
        }
        $sql = $sql = "INSERT INTO " . $this->master . "(no,dari,ke,daerah,penyetuju,menimbang,dasar,untuk,notapenye,notano,perihal,isian)"
                . " VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        $hasil = $this->db->query($sql, array($a1,$dari,$ke,$b1,$c1,$d1,$e1,$f1,$i1,$j1,$k1,$l1));
        return $hasil;
    }

    function update_surat($a1,$b1,$c1,$d1,$e1,$f1,$g1,$h1,$i1,$j1,$k1,$l1,$kode){
        $user = $this->session->userdata('id_user');
        $dari = date_format(DateTime::createFromFormat('d/m/Y', $g1),'Y-m-d');
        $ke = date_format(DateTime::createFromFormat('d/m/Y', $h1),'Y-m-d');
        $sql = $sql = "UPDATE " . $this->master . " SET no=?,dari=?,ke=?,daerah=?,penyetuju=?,menimbang=?,dasar=?,untuk=?,notapenye = ?,notano = ?,perihal = ?,isian =? where id=?";
        $hasil = $this->db->query($sql, array($a1,$dari,$ke,$b1,$c1,$d1,$e1,$f1,$i1,$j1,$k1,$l1,$kode));
        return $hasil;
    }
	
}