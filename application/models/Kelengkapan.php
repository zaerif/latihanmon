<?php
class Kelengkapan extends CI_Model{
	private $master = 'kelengkapan';

	function surat_list(){
	        $this->datatables->select('no,IFNULL(nom_tiket,"0") nom_tiket,IFNULL(nom_transport,"0")nom_transport,IFNULL(nom_hotel,"0") nom_hotel,IFNULL(doc_tiket,"Belum") doc_tiket,doc_hotel,doc_transport,IFNULL(doc_kerja,"Belum") doc_kerja');
	        $this->datatables->from('(select a.no, b.nom_tiket,b.nom_transport,b.nom_hotel,b.doc_tiket,b.doc_transport,b.doc_hotel,b.doc_kerja from surat_tugas a left join kelengkapan b on a.no = b.no) c');
	        $this->datatables->add_column('view', '<a href="javascript:void(0);" data="$1" class="on-default edit-row" style="color:orange;margin-right:15px;"><i class="fa fa-pencil"></i></a>','no');
	        return $this->datatables->generate();
	}

	function get_surat_by_kode($kobar){
		$hsl=$this->db->query("select a.no, b.nom_tiket,b.nom_transport,b.nom_hotel,b.doc_tiket,b.doc_transport,b.doc_hotel,b.doc_kerja from surat_tugas a left join kelengkapan b on a.no = b.no WHERE a.no='$kobar'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'a1' => $data->no,
					'b1' => isset($data->nom_tiket) ? $data->nom_tiket : '0',
					'c1' => isset($data->nom_transport) ? $data->nom_transport : '0',
					'd1' => isset($data->nom_hotel) ? $data->nom_hotel : '0',
					'e1' => isset($data->doc_tiket) ? $data->doc_tiket : 'Belum',
					'f1' => isset($data->doc_hotel) ? $data->doc_hotel : 'Belum',
					'g1' => isset($data->doc_transport) ? $data->doc_transport : 'Belum',
					'h1' => isset($data->doc_kerja) ? $data->doc_kerja : 'Belum',
					);
			}
		}
		return $hasil;
	}

	function hapus_surat($kobar){
		if($kobar != $this->session->userdata('id_user')){
			$hasil=$this->db->query("DELETE FROM " . $this->master . "  WHERE id='$kobar'");
			return $hasil;
		} else {
			$arr = array('msg' => 'Tidak dapat Menghapus data', 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5); 
    		header('Content-Type: application/json');
			return json_encode($arr);
		}
	}

    function simpan_surat($a1,$b1,$c1,$d1,$e1,$f1,$g1,$h1,$i1){
        $upd='';
        $upd1='';
        $upd2='';
        $upd3='';
        if(!empty($_FILES['e1']['name']))
        {
            $acak=mt_rand(00000000000,mt_getrandmax());
            $bersih=$_FILES['e1']['name'];
            $nm=str_replace(" ","_","$bersih");
            $pisah=explode(".",$nm);
            $nama_murni_lama = preg_replace("/^(.+?);.*$/", "\\1",$pisah[0]);
            $nama_murni=date('Ymd-His');

            $ekstensi_ori = $_FILES['e1']['type'];
            $pisahekstensi=explode("/",$ekstensi_ori);
            $ekstensi_kotor = $pisahekstensi[1];
            
            $file_type = preg_replace("/^(.+?);.*$/", "\\1", $ekstensi_kotor);
            $file_type_baru = strtolower($file_type);
            
            $ubah=$acak.'-'.$nama_murni; //tanpa ekstensi
            $n_baru = $ubah.'.'.$file_type_baru;
            
            $config['upload_path']  = "./assets/sppd/";
            $config['allowed_types']= 'gif|jpg|png|jpeg|pdf|doc|xls|xlsx';
            $config['file_name'] = $n_baru;
            $config['max_size']     = '2500';
            $config['max_width']    = '3000';
            $config['max_height']   = '3000';
     
            $this->load->library('upload', $config);
     
            if ($this->upload->do_upload("e1")) {
                $data       = $this->upload->data();
     
                /* PATH */
                $source             = "./assets/sppd/".$data['file_name'] ;
     
                // Permission Configuration
                chmod($source, 0777) ;
                $upd = $data['file_name'];
            }
        }
        if(!empty($_FILES['f1']['name']))
        {
            $acak=mt_rand(00000000000,mt_getrandmax());
            $bersih=$_FILES['f1']['name'];
            $nm=str_replace(" ","_","$bersih");
            $pisah=explode(".",$nm);
            $nama_murni_lama = preg_replace("/^(.+?);.*$/", "\\1",$pisah[0]);
            $nama_murni=date('Ymd-His');

            $ekstensi_ori = $_FILES['f1']['type'];
            $pisahekstensi=explode("/",$ekstensi_ori);
            $ekstensi_kotor = $pisahekstensi[1];
            
            $file_type = preg_replace("/^(.+?);.*$/", "\\1", $ekstensi_kotor);
            $file_type_baru = strtolower($file_type);
            
            $ubah=$acak.'-'.$nama_murni; //tanpa ekstensi
            $n_baru = $ubah.'.'.$file_type_baru;
            
            $config['upload_path']  = "./assets/sppd/";
            $config['allowed_types']= 'gif|jpg|png|jpeg|pdf|doc|xls|xlsx';
            $config['file_name'] = $n_baru;
            $config['max_size']     = '2500';
            $config['max_width']    = '3000';
            $config['max_height']   = '3000';
     
            $this->load->library('upload', $config);
     
            if ($this->upload->do_upload("f1")) {
                $data       = $this->upload->data();
     
                /* PATH */
                $source             = "./assets/sppd/".$data['file_name'] ;
     
                // Permission Configuration
                chmod($source, 0777) ;
                $upd1 = $data['file_name'];
            }
        }
        if(!empty($_FILES['g1']['name']))
        {
            $acak=mt_rand(00000000000,mt_getrandmax());
            $bersih=$_FILES['g1']['name'];
            $nm=str_replace(" ","_","$bersih");
            $pisah=explode(".",$nm);
            $nama_murni_lama = preg_replace("/^(.+?);.*$/", "\\1",$pisah[0]);
            $nama_murni=date('Ymd-His');

            $ekstensi_ori = $_FILES['g1']['type'];
            $pisahekstensi=explode("/",$ekstensi_ori);
            $ekstensi_kotor = $pisahekstensi[1];
            
            $file_type = preg_replace("/^(.+?);.*$/", "\\1", $ekstensi_kotor);
            $file_type_baru = strtolower($file_type);
            
            $ubah=$acak.'-'.$nama_murni; //tanpa ekstensi
            $n_baru = $ubah.'.'.$file_type_baru;
            
            $config['upload_path']  = "./assets/sppd/";
            $config['allowed_types']= 'gif|jpg|png|jpeg|pdf|doc|xls|xlsx';
            $config['file_name'] = $n_baru;
            $config['max_size']     = '2500';
            $config['max_width']    = '3000';
            $config['max_height']   = '3000';
     
            $this->load->library('upload', $config);
     
            if ($this->upload->do_upload("g1")) {
                $data       = $this->upload->data();
     
                /* PATH */
                $source             = "./assets/sppd/".$data['file_name'] ;
     
                // Permission Configuration
                chmod($source, 0777) ;
                $upd2 = $data['file_name'];
            }
        }
        if(!empty($_FILES['h1']['name']))
        {
            $acak=mt_rand(00000000000,mt_getrandmax());
            $bersih=$_FILES['h1']['name'];
            $nm=str_replace(" ","_","$bersih");
            $pisah=explode(".",$nm);
            $nama_murni_lama = preg_replace("/^(.+?);.*$/", "\\1",$pisah[0]);
            $nama_murni=date('Ymd-His');

            $ekstensi_ori = $_FILES['h1']['type'];
            $pisahekstensi=explode("/",$ekstensi_ori);
            $ekstensi_kotor = $pisahekstensi[1];
            
            $file_type = preg_replace("/^(.+?);.*$/", "\\1", $ekstensi_kotor);
            $file_type_baru = strtolower($file_type);
            
            $ubah=$acak.'-'.$nama_murni; //tanpa ekstensi
            $n_baru = $ubah.'.'.$file_type_baru;
            
            $config['upload_path']  = "./assets/sppd/";
            $config['allowed_types']= 'gif|jpg|png|jpeg|pdf|doc|xls|xlsx';
            $config['file_name'] = $n_baru;
            $config['max_size']     = '2500';
            $config['max_width']    = '3000';
            $config['max_height']   = '3000';
     
            $this->load->library('upload', $config);
     
            if ($this->upload->do_upload("h1")) {
                $data       = $this->upload->data();
     
                /* PATH */
                $source             = "./assets/sppd/".$data['file_name'] ;
     
                // Permission Configuration
                chmod($source, 0777) ;
                $upd3 = $data['file_name'];
            }
        }
        $tot_keh = $this->db->query("select * from kelengkapan where no = '".$a1."' and pegawai = '".$i1."'");
        $jml = $tot_keh->num_rows();
        if($jml == 0){
	        $sql = $sql = "INSERT INTO " . $this->master . "(no,nom_tiket,nom_transport,nom_hotel,doc_tiket,doc_transport,doc_hotel,doc_kerja,pegawai)"
	                . " VALUES(?,?,?,?,?,?,?,?,?)";
	        $hasil = $this->db->query($sql, array($a1,$b1,$c1,$d1,$upd,$upd1,$upd2,$upd3,$i1));
        } else {
            $html='';
            if($upd != ''){
                $html .= ",doc_tiket='".$upd."'";
            }
            if($upd1 != ''){
                $html .= ",doc_transport='".$upd1."'";
            }
            if($upd2 != ''){
                $html .= ",doc_hotel='".$upd2."'";
            }
            if($upd3 != ''){
                $html .= ",doc_kerja='".$upd3."'";
            }
	        $sql = $sql = "UPDATE " . $this->master . " SET nom_tiket=?,nom_transport=?,nom_hotel=?".$html." where no=? and pegawai = ?";
	        $hasil = $this->db->query($sql, array($b1,$c1,$d1,$a1,$i1));
        }
        return $hasil;
    }

    function simpan_surat2($a1,$b1,$i1){
        $sql = $sql = "UPDATE " . $this->master . " SET belakang=? where no=? and pegawai = ?";
        $hasil = $this->db->query($sql, array($b1,$a1,$i1));
        return $hasil;
    }
	
}