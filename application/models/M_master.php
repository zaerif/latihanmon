<?php
class M_Master extends CI_Model{
	private $master = 'master_gol';

	function master_list(){
	        $this->datatables->select('a.id,a.nama,a.harian,a.iveselon,a.ivkaro,a.ivkabag,a.iii,a.ii,a.bandara,a.local,a.res1,a.res2');
	        $this->datatables->from('master_gol a');
	        $this->datatables->add_column('view', '<a href="javascript:void(0);" data="$1" class="on-default lihat-row" style="color:blue;margin-right:15px;"><i class="fa fa-eye"></i></a><a href="javascript:void(0);" data="$1" class="on-default edit-row" style="color:orange;margin-right:15px;"><i class="fa fa-pencil"></i></a><a href="javascript:void(0);" data="$1" class="on-default hapus-row" style="color:red;margin-right:15px;"><i class="fa fa-trash-o"></i></a>','id');
	        return $this->datatables->generate();
	}

	function get_master_by_kode($kobar){
		$hsl=$this->db->query("SELECT * FROM master_gol WHERE id='$kobar'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'a1' => $data->nama,
					'b1' => $data->iveselon,
					'c1' => $data->ivkaro,
					'd1' => isset($data->ivkabag) ? $data->ivkabag : '',
					'e1' => isset($data->iii) ? $data->iii : '',
					'f1' => isset($data->ii) ? $data->ii : '',
					'g1' => isset($data->bandara) ? $data->bandara : '',
					'h1' => isset($data->local) ? $data->local : '',
					'i1' => isset($data->res1) ? $data->res1 : '',
					'j1' => isset($data->res2) ? $data->res2 : '',
					);
			}
		}
		return $hasil;
	}

	function hapus_pegawai($kobar){
		$hasil=$this->db->query("DELETE FROM " . $this->master . "  WHERE id='$kobar'");
		return $hasil;
	}

    function simpan_master($a1,$b1,$c1,$d1,$e1,$f1,$g1,$h1,$i1,$j1){
        $user = $this->session->userdata('id_user');
        $sql = $sql = "INSERT INTO " . $this->master . "(nama,iveselon,ivkaro,ivkabag,iii,ii,bandara,local,res1,res2)"
                . " VALUES(?,?,?,?,?)";
        $hasil = $this->db->query($sql, array($a1,$b1,$c1,$d1,$e1,$f1,$g1,$h1,$i1,$j1));
        return $hasil;
    }

    function update_master($a1,$b1,$c1,$d1,$e1,$f1,$g1,$h1,$i1,$j1,$kode){
        $sql = $sql = "UPDATE " . $this->master . " SET nama=?,iveselon=?,ivkaro=?,ivkabag=?,iii=?,ii=?,bandara=?,local=?,res1=?,res2=? where id=?";
        $hasil = $this->db->query($sql, array($a1,$b1,$c1,$d1,$e1,$f1,$g1,$h1,$i1,$j1,$kode));
        return $hasil;
    }
	
}