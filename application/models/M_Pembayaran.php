<?php
class M_Pembayaran extends CI_Model{
	private $master = 'pembayaran';

    function __construct() {
        parent::__construct();
        $this->load->library('encryption');
    }

	function surat_list(){
	        $this->datatables->select('id,no,lama,daerah,nama');
	        $this->datatables->from("(select d.id,a.no,CONCAT('(',DATEDIFF(a.ke,a.dari)+1,') x ',FORMAT(d.harian/(DATEDIFF(a.ke,a.dari)+1),0)) as lama,b.daerah,c.nama from list a left join surat_tugas b on a.no = b.no left join tbl_user c on a.pegawai = c.id left join pembayaran d on a.no = d.no and c.id = d.pegawai) c");
	        $this->datatables->add_column('view', '<a href="javascript:void(0);" data="$1" class="on-default edit-row" style="color:orange;margin-right:15px;"><i class="fa fa-pencil"></i></a>','id');
	        return $this->datatables->generate();
	}

	function get_surat_by_kode($kobar){
		$hsl=$this->db->query("select a.id,a.no,((DATEDIFF(f.ke,f.dari)+1)*d.harian+d.bandara+d.local) as total,DATEDIFF(f.ke,f.dari)+1 as lama,e.daerah,c.golongan,c.nama,c.id namaid,d.harian,CASE WHEN c.golongan = 'IVeselon' THEN d.iveselon WHEN c.golongan = 'IVkaro' THEN d.ivkaro WHEN c.golongan = 'IVkabag' THEN d.ivkabag WHEN c.golongan = 'III' then d.iii WHEN c.golongan = 'II' THEN d.ii ELSE 'Tidak Ketemu' END as masterhotel,d.bandara,d.local,d.res1,d.res2, b.nom_tiket, b.nom_transport,b.nom_hotel from pembayaran a left join kelengkapan b on a.`no` = b.`no` and a.pegawai = b.pegawai left join tbl_user c on a.pegawai = c.id left join surat_tugas e on a.no = e.no left join master_gol d on e.daerah = d.nama left join list f on a.no = f.no and a.pegawai = f.pegawai WHERE a.id='$kobar';");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'a1' => $data->no,
					'b1' => isset($data->lama) ? $data->lama : '0',
					'c1' => isset($data->harian) ? $data->harian : '0',
					'd1' => isset($data->masterhotel) ? $data->masterhotel : '0',
					'e1' => isset($data->bandara) ? $data->bandara : '0',
					'f1' => isset($data->local) ? $data->local : '0',
					'g1' => isset($data->res1) ? $data->res1 : '0',
					'h1' => isset($data->res2) ? $data->res2 : '0',
                    'i1' => isset($data->nom_tiket) ? $data->nom_tiket : '0',
                    'j1' => isset($data->nom_transport) ? $data->nom_transport : '0',
                    'k1' => isset($data->nom_hotel) ? $data->nom_hotel : '0',
                    'l1' => isset($data->nama) ? $data->nama : '0',
                    'm1' => isset($data->daerah) ? $data->daerah : '0',
                    'n1' => isset($data->namaid) ? $data->namaid : '0',
                    'o1' => isset($data->id) ? $data->id : '0',
					);
			}
		}
		return $hasil;
	}

    function simpan_surat($a1,$b1,$c1,$d1,$e1,$f1,$g1,$h1,$i1,$j1,$k1){
        $tot_keh = $this->db->query("select * from pembayaran where id = '".$k1."'");
        $jml = $tot_keh->num_rows();
        if($jml == 0){
	        $sql = $sql = "INSERT INTO " . $this->master . "(no,harian,tiket,hotel,res,tanggal,pegawai,transport,penyetuju,bendahara)"
	                . " VALUES(?,?,?,?,?,?,?,?,?,?,?)";
	        $hasil = $this->db->query($sql, array($a1,$c1,$d1,$e1,$f1,date('Y-m-d'),$g1,$h1,$i1,$j1));
        } else {
	        $sql = $sql = "UPDATE " . $this->master . " SET harian=?,tiket=?,hotel=?,res=?,tanggal=?,pegawai=?,transport=?,penyetuju=?, bendahara=? where id=?";
	        $hasil = $this->db->query($sql, array($c1,$d1,$e1,$f1,date('Y-m-d'),$g1,$h1,$i1,$j1,$k1));
        }
        return $hasil;
    }
	
}