<?php
class M_Pegawai extends CI_Model{
	private $master = 'tbl_user';

	function pegawai_list(){
	        $this->datatables->select('a.id,a.nama,a.jabatan,a.golongan,a.nip');
	        $this->datatables->from('tbl_user a');
	        $this->datatables->add_column('view', '<a href="javascript:void(0);" data="$1" class="on-default lihat-row" style="color:blue;margin-right:15px;"><i class="fa fa-eye"></i></a><a href="javascript:void(0);" data="$1" class="on-default edit-row" style="color:orange;margin-right:15px;"><i class="fa fa-pencil"></i></a><a href="javascript:void(0);" data="$1" class="on-default hapus-row" style="color:red;margin-right:15px;"><i class="fa fa-trash-o"></i></a>','id');
	        return $this->datatables->generate();
	}

	function get_pegawai_by_kode($kobar){
		$hsl=$this->db->query("SELECT * FROM tbl_user WHERE id='$kobar'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'a1' => $data->nama,
					'b1' => $data->nip,
					'c1' => $data->jabatan,
					'd1' => isset($data->golongan) ? $data->golongan : '',
					);
			}
		}
		return $hasil;
	}

	function hapus_pegawai($kobar){
		if($kobar != $this->session->userdata('id_user')){
			$hasil=$this->db->query("DELETE FROM " . $this->master . "  WHERE id='$kobar'");
			return $hasil;
		} else {
			$arr = array('msg' => 'Tidak dapat Menghapus data', 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5); 
    		header('Content-Type: application/json');
			return json_encode($arr);
		}
	}

	function ubah_password($a1,$b1,$c1,$d1){
		if($b1 != $c1){
			$hasil= array('msg', 'Konfirmasi Password tidak Sesuai');
			return $hasil;
		} else {
			$id_user = $this->session->userdata('id_user');
			$login['id'] = $id_user;
			$login['password'] = md5($a1.'AppSimpeg32');
			$password =  md5($b1.'AppSimpeg32');
			$cek = $this->db->get_where('tbl_user_pegawai', $login);
			if($cek->num_rows()>0)
			{
				if($b1==$c1)
				{
					$this->db->query("UPDATE " . $this->master . " SET password = '$password'  WHERE id='$id_user'");
					$hasil= array('msg', 'Berhasil Mengubah Password');
					return $hasil;
				}
			}
			else
			{
				$hasil= array('msg', 'Password lama salah');
				return $hasil;
			}
		}
	}

    function simpan_pegawai($a1,$b1,$c1,$d1){
        $user = $this->session->userdata('id_user');
        $dari = date_format(DateTime::createFromFormat('d/m/Y', $e2),'Y-m-d H:i:s');
        $ke = date_format(DateTime::createFromFormat('d/m/Y', $f2),'Y-m-d H:i:s');
        if($this->session->userdata('stts') == "4"){
            $a2 = $this->session->userdata('id_user');
        }
        $password =  md5('admin123AppKPU32');
        $sql = $sql = "INSERT INTO " . $this->master . "(nama,nip,jabatan,golongan,password)"
                . " VALUES(?,?,?,?,?)";
        $hasil = $this->db->query($sql, array($a1,$b1,$c1,$d1,$password));
        return $hasil;
    }

    function update_pegawai($a1,$b1,$c1,$d1,$kode){
        $user = $this->session->userdata('id_user');
        $sql = $sql = "UPDATE " . $this->master . " SET nama=?,nip=?,jabatan=?,golongan=? where id=?";
        $hasil = $this->db->query($sql, array($a1,$b1,$c1,$d1,$kode));
        return $hasil;
    }
	
}