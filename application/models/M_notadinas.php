<?php
class M_Notadinas extends CI_Model{
	private $master = 'nota_dinas';

	function surat_list(){
	        $this->datatables->select('a.id,a.notano,DATE_FORMAT(a.tanggal, "%d-%m-%Y") as dari,a.perihal');
	        $this->datatables->from('nota_dinas a');
	        $this->datatables->add_column('view', '<a href="javascript:void(0);" data="$1" class="on-default lihat-row" style="color:blue;margin-right:15px;"><i class="fa fa-print"></i></a><a href="javascript:void(0);" data="$1" class="on-default edit-row" style="color:orange;margin-right:15px;"><i class="fa fa-pencil"></i></a><a href="javascript:void(0);" data="$1" class="on-default hapus-row" style="color:red;margin-right:15px;"><i class="fa fa-trash-o"></i></a>','id');
	        return $this->datatables->generate();
	}

	function get_surat_by_kode($kobar){
		$hsl=$this->db->query("SELECT * FROM nota_dinas WHERE id='$kobar'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'i1' => isset($data->notapenye) ? $data->notapenye : '',
					'j1' => isset($data->notano) ? $data->notano : '',
					'k1' => isset($data->perihal) ? $data->perihal : '',
					'l1' => isset($data->isian) ? $data->isian : '',
					'm1' => isset($data->tanggal) ? $data->tanggal : '',
					);
			}
		}
		return $hasil;
	}

	function hapus_surat($kobar){
		if($kobar != $this->session->userdata('id_user')){
			$hasil=$this->db->query("DELETE FROM " . $this->master . "  WHERE id='$kobar'");
			return $hasil;
		} else {
			$arr = array('msg' => 'Tidak dapat Menghapus data', 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5); 
    		header('Content-Type: application/json');
			return json_encode($arr);
		}
	}

    function simpan_surat($i1,$j1,$k1,$l1,$m1){
        $user = $this->session->userdata('id_user');
        $tanggal = date_format(DateTime::createFromFormat('d/m/Y', $m1),'Y-m-d');
        if($this->session->userdata('stts') == "4"){
            $a2 = $this->session->userdata('id_user');
        }
        $sql = $sql = "INSERT INTO " . $this->master . "(notapenye,notano,perihal,isian,tanggal)"
                . " VALUES(?,?,?,?,?)";
        $hasil = $this->db->query($sql, array($i1,$j1,$k1,$l1,$tanggal));
        return $hasil;
    }

    function update_surat($i1,$j1,$k1,$l1,$m1,$kode){
        $user = $this->session->userdata('id_user');
        $tanggal = date_format(DateTime::createFromFormat('d/m/Y', $m1),'Y-m-d');
        $sql = $sql = "UPDATE " . $this->master . " SET notapenye=?,notano=?,perihal=?,isian=?,tanggal=? where id=?";
        $hasil = $this->db->query($sql, array($i1,$j1,$k1,$l1,$tanggal,$kode));
        return $hasil;
    }
	
}