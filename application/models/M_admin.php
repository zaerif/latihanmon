<?php
class M_Admin extends CI_Model{
	private $master = 'tbl_user';

	function pegawai_list(){
		if($this->session->userdata('stts')=="1"){
	        $this->datatables->select('id,nama,roles');
	        $this->datatables->from('(select a.id,a.nama,CASE when a.role = 1 THEN "Admin" else "Pegawai" END as roles from tbl_user a)z');
	        $this->datatables->add_column('view', '<a href="javascript:void(0);" data="$1" class="on-default lihat-row" style="color:blue;margin-right:15px;"><i class="fa fa-eye"></i></a><a href="javascript:void(0);" data="$1" class="on-default edit-row" style="color:orange;margin-right:15px;"><i class="fa fa-pencil"></i></a>','id');
	        return $this->datatables->generate();
		} else {
			$id_user = $this->session->userdata('id_user');
	        $this->datatables->select('id,nama,roles');
	        $this->datatables->from('(select a.id,a.nama,CASE when a.role = 1 THEN "Admin" else "Pegawai" END as roles from tbl_user a where a.id = '.$id_user.')z');
	        $this->datatables->add_column('view', '<a href="javascript:void(0);" data="$1" class="on-default lihat-row" style="color:blue;margin-right:15px;"><i class="fa fa-eye"></i></a><a href="javascript:void(0);" data="$1" class="on-default edit-row" style="color:orange;margin-right:15px;"><i class="fa fa-pencil"></i></a>','id');
	        return $this->datatables->generate();
		}
	}

	function get_pegawai_by_kode($kobar){
		$hsl=$this->db->query("SELECT * FROM tbl_user WHERE id='$kobar'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'a1' => $data->nama,
					'b1' => $data->role,
					);
			}
		}
		return $hasil;
	}

    function simpan_pegawai($a1,$b1,$c1,$d1){
    	if($b1 != $c1){
			$hasil= array('msg', 'Konfirmasi Password tidak Sesuai');
			return $hasil;
		} else {
			$login['password'] = md5($a1.'AppKPU32');
			$password =  md5($b1.'AppKPU32');
			$cek = $this->db->get_where('tbl_user_pegawai', $login);
			if($cek->num_rows()>0)
			{
				if($b1==$c1)
				{
					$this->db->query("UPDATE " . $this->master . " SET password = '$password',role = '$d1'  WHERE id='$id_user'");
					$hasil= array('msg', 'Berhasil Mengubah Password');
					return $hasil;
				}
			}
			else
			{
				$hasil= array('msg', 'Password lama salah');
				return $hasil;
			}
		}
        $user = $this->session->userdata('id_user');
        $password =  md5('admin123AppKPU32');
        $sql = $sql = "INSERT INTO " . $this->master . "(nama,nip,jabatan,golongan,password)"
                . " VALUES(?,?,?,?,?)";
        $hasil = $this->db->query($sql, array($a1,$b1,$c1,$d1,$password));
        return $hasil;
    }

    function update_pegawai($a1,$b1,$c1,$d1,$kode){
        $user = $this->session->userdata('id_user');
    	if($b1 != $c1){
			$hasil= array('msg', 'Konfirmasi Password tidak Sesuai');
			return $hasil;
		} else {
			if($b1==$c1)
			{
				$this->db->query("UPDATE " . $this->master . " SET password = '$password',role = '$d1'  WHERE id='$kode'");
				$hasil= array('msg', 'Berhasil Mengubah Password');
				return $hasil;
			}
		}
    }
	
}