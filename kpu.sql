-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 10.1.8-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Membuang struktur basisdata untuk msadi_kpu
DROP DATABASE IF EXISTS `msadi_kpu`;
CREATE DATABASE IF NOT EXISTS `msadi_kpu` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `msadi_kpu`;

-- membuang struktur untuk table msadi_kpu.kelengkapan
DROP TABLE IF EXISTS `kelengkapan`;
CREATE TABLE IF NOT EXISTS `kelengkapan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pegawai` int(11) DEFAULT NULL,
  `no` varchar(200) NOT NULL DEFAULT '0',
  `nom_tiket` int(11) DEFAULT NULL,
  `nom_transport` int(11) DEFAULT NULL,
  `nom_hotel` int(11) DEFAULT NULL,
  `doc_tiket` varchar(100) DEFAULT NULL,
  `doc_transport` varchar(100) DEFAULT NULL,
  `doc_hotel` varchar(100) DEFAULT NULL,
  `doc_kerja` varchar(100) DEFAULT NULL,
  `belakang` longtext,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel msadi_kpu.kelengkapan: ~2 rows (lebih kurang)
DELETE FROM `kelengkapan`;
/*!40000 ALTER TABLE `kelengkapan` DISABLE KEYS */;
INSERT INTO `kelengkapan` (`id`, `pegawai`, `no`, `nom_tiket`, `nom_transport`, `nom_hotel`, `doc_tiket`, `doc_transport`, `doc_hotel`, `doc_kerja`, `belakang`, `tanggal`) VALUES
	(4, 1, 'demo1/sppd/2018', 1000000, 200000, 300000, '1139918131-20180826-223006.pdf', '1139918131-20180826-2230061.pdf', '1139918131-20180826-2230062.pdf', '1199205175-20180826-224031.png', 'Jakarta##Demoke##26/08/2018##Demonama####Demo1##26/08/2018##Demo1kepala##demo1nama####Demo2##Demo2##26/08/2018####demo2nama####Demo3##26/08/2018##Demo3##Demo3nama####Demo4##Demo4##26/08/2018####Demo4nama####Demo5##26/08/2018##Demo3##Demo5nama####Demo6##Demo6##26/08/2018####Demo6nama####Demo7##Demo7##26/08/2018##Demo7nama####Demo8nama', NULL),
	(7, 4, 'demo1/sppd/2018', 2000000, 1000000, 500000, '', '', '', '', NULL, NULL);
/*!40000 ALTER TABLE `kelengkapan` ENABLE KEYS */;

-- membuang struktur untuk table msadi_kpu.list
DROP TABLE IF EXISTS `list`;
CREATE TABLE IF NOT EXISTS `list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pegawai` varchar(100) DEFAULT NULL,
  `no` varchar(200) DEFAULT NULL,
  `dari` date DEFAULT NULL,
  `ke` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel msadi_kpu.list: ~1 rows (lebih kurang)
DELETE FROM `list`;
/*!40000 ALTER TABLE `list` DISABLE KEYS */;
INSERT INTO `list` (`id`, `pegawai`, `no`, `dari`, `ke`) VALUES
	(18, '1', 'demo1/sppd/2018', '2018-08-23', '2018-08-25'),
	(19, '4', 'demo1/sppd/2018', '2018-08-23', '2018-08-23');
/*!40000 ALTER TABLE `list` ENABLE KEYS */;

-- membuang struktur untuk table msadi_kpu.master_gol
DROP TABLE IF EXISTS `master_gol`;
CREATE TABLE IF NOT EXISTS `master_gol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `harian` int(11) DEFAULT NULL,
  `iveselon` int(11) DEFAULT NULL,
  `ivkaro` int(11) DEFAULT NULL,
  `ivkabag` int(11) DEFAULT NULL,
  `iii` int(11) DEFAULT NULL,
  `ii` int(11) DEFAULT NULL,
  `bandara` int(11) DEFAULT NULL,
  `local` int(11) DEFAULT NULL,
  `res1` int(11) DEFAULT NULL,
  `res2` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel msadi_kpu.master_gol: ~34 rows (lebih kurang)
DELETE FROM `master_gol`;
/*!40000 ALTER TABLE `master_gol` DISABLE KEYS */;
INSERT INTO `master_gol` (`id`, `nama`, `harian`, `iveselon`, `ivkaro`, `ivkabag`, `iii`, `ii`, `bandara`, `local`, `res1`, `res2`) VALUES
	(1, 'ACEH/ BANDA ACEH', 360000, 4420000, 3526000, 1294000, 556000, 556000, 512000, 246000, 200000, 150000),
	(2, 'SUMATERA UTARA/ MEDAN', 370000, 4960000, 1518000, 1100000, 530000, 530000, 512000, 464000, 200000, 150000),
	(3, 'RIAU/ PEKAN BARU', 370000, 3820000, 3119000, 1650000, 852000, 852000, 512000, 188000, 200000, 150000),
	(4, 'KEPULAUAN RIAU/ TANJUNG PINANG', 370000, 4275000, 1854000, 1037000, 792000, 792000, 512000, 274000, 200000, 150000),
	(5, 'JAMBI/ JAMBI', 370000, 4000000, 3337000, 1212000, 520000, 520000, 512000, 294000, 200000, 150000),
	(6, 'SUMATERA BARAT/ PADANG', 380000, 5236000, 3332000, 1353000, 650000, 650000, 512000, 380000, 200000, 150000),
	(7, 'SUMATERA SELATAN/ PALEMBANG', 380000, 8447000, 3083000, 1571000, 861000, 861000, 512000, 256000, 200000, 150000),
	(8, 'LAMPUNG/ BANDAR LAMPUNG', 380000, 4491000, 2067000, 1140000, 400000, 400000, 512000, 334000, 200000, 150000),
	(9, 'BENGKULU/ BENGKULU', 380000, 2071000, 1628000, 1546000, 572000, 572000, 512000, 218000, 200000, 150000),
	(10, 'BANGKA BELITUNG/ PANGKAL PINANG', 410000, 3827000, 2838000, 1957000, 622000, 622000, 512000, 180000, 200000, 150000),
	(11, 'BANTEN/ SERANG', 370000, 5725000, 2373000, 1000000, 718000, 718000, 512000, 892000, 200000, 150000),
	(12, 'JAWA BARAT/ BANDUNG', 430000, 5381000, 2755000, 1006000, 570000, 570000, 512000, 332000, 200000, 150000),
	(13, 'DKI JAKARTA/ JAKARTA', 530000, 8720000, 1490000, 992000, 610000, 610000, 512000, 512000, 200000, 150000),
	(14, 'JAWA TENGAH/ SEMARANG', 370000, 4242000, 1480000, 954000, 486000, 486000, 512000, 150000, 200000, 150000),
	(15, 'D.I. YOGYAKARTA/ YOGYAKARTA', 420000, 5017000, 2695000, 1384000, 845000, 845000, 512000, 236000, 200000, 150000),
	(16, 'JAWA TIMUR/ SURABAYA', 410000, 4400000, 1605000, 1076000, 664000, 664000, 512000, 388000, 200000, 150000),
	(17, 'BALI/ DENPASAR', 480000, 4890000, 1946000, 990000, 910000, 910000, 512000, 318000, 200000, 150000),
	(18, 'NUSA TENGGARA BARAT/ MATARAM', 440000, 3500000, 2648000, 1418000, 580000, 580000, 512000, 462000, 200000, 150000),
	(19, 'NUSA TENGGARA TIMUR/ KUPANG', 430000, 3000000, 1493000, 1355000, 550000, 550000, 512000, 216000, 200000, 150000),
	(20, 'KALIMANTAN BARAT/ PONTIANAK', 380000, 2654000, 1538000, 1125000, 538000, 538000, 512000, 270000, 200000, 150000),
	(21, 'KALIMANTAN TENGAH/ PALANGKARAYA', 360000, 4901000, 3391000, 1160000, 659000, 659000, 512000, 222000, 200000, 150000),
	(22, 'KALIMANTAN SELATAN/ BANJARMASIN', 380000, 4797000, 3316000, 1500000, 540000, 540000, 512000, 300000, 200000, 150000),
	(23, 'KALIMANTAN TIMUR/ BALIKPAPAN', 430000, 4000000, 2188000, 1507000, 804000, 804000, 512000, 900000, 200000, 150000),
	(24, 'KALIMANTAN UTARA/ TARAKAN', 430000, 4000000, 2188000, 1507000, 804000, 804000, 512000, 204000, 200000, 150000),
	(25, 'SULAWESI UTARA/ MANADO', 370000, 4919000, 2290000, 924000, 782000, 782000, 512000, 276000, 200000, 150000),
	(26, 'GORONTALO/ GORONTALO', 370000, 4168000, 2549000, 1909000, 764000, 764000, 512000, 480000, 200000, 150000),
	(27, 'SULAWESI BARAT/ MAMUJU', 410000, 4076000, 2581000, 1075000, 704000, 704000, 512000, 626000, 200000, 150000),
	(28, 'SULAWESI SELATAN/ MAKASSAR', 430000, 4820000, 1550000, 1020000, 665000, 665000, 512000, 290000, 200000, 150000),
	(29, 'SULAWESI TENGAH/ PALU', 370000, 2309000, 2027000, 1567000, 951000, 951000, 512000, 330000, 200000, 150000),
	(30, 'SULAWESI TENGGARA/ KENDARI', 380000, 2475000, 2059000, 1297000, 786000, 786000, 512000, 342000, 200000, 150000),
	(31, 'MALUKU/ AMBON', 380000, 3467000, 3240000, 1048000, 667000, 667000, 512000, 480000, 200000, 150000),
	(32, 'MALUKU UTARA/ TERNATE', 430000, 3440000, 3175000, 1073000, 480000, 480000, 512000, 430000, 200000, 150000),
	(33, 'PAPUA/ JAYAPURA', 580000, 3859000, 3318000, 2521000, 829000, 829000, 512000, 862000, 200000, 150000),
	(34, 'PAPUA BARAT/ MANOKWARI', 480000, 3872000, 3212000, 2056000, 600000, 600000, 512000, 364000, 200000, 150000);
/*!40000 ALTER TABLE `master_gol` ENABLE KEYS */;

-- membuang struktur untuk table msadi_kpu.pembayaran
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE IF NOT EXISTS `pembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no` varchar(200) DEFAULT NULL,
  `pegawai` int(11) DEFAULT NULL,
  `relate` int(11) DEFAULT NULL,
  `hotel` int(11) DEFAULT NULL,
  `transport` int(11) DEFAULT NULL,
  `tiket` int(11) DEFAULT NULL,
  `harian` int(11) DEFAULT NULL,
  `res` int(11) DEFAULT NULL,
  `penyetuju` int(11) DEFAULT '5',
  `bendahara` int(11) DEFAULT '6',
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel msadi_kpu.pembayaran: ~2 rows (lebih kurang)
DELETE FROM `pembayaran`;
/*!40000 ALTER TABLE `pembayaran` DISABLE KEYS */;
INSERT INTO `pembayaran` (`id`, `no`, `pegawai`, `relate`, `hotel`, `transport`, `tiket`, `harian`, `res`, `penyetuju`, `bendahara`, `tanggal`) VALUES
	(11, 'demo1/sppd/2018', 1, 18, 8840000, 0, 1000000, 1080000, 0, 5, 6, '2018-08-27'),
	(12, 'demo1/sppd/2018', 4, 19, 0, NULL, NULL, 360000, NULL, 5, 6, NULL);
/*!40000 ALTER TABLE `pembayaran` ENABLE KEYS */;

-- membuang struktur untuk table msadi_kpu.surat_tugas
DROP TABLE IF EXISTS `surat_tugas`;
CREATE TABLE IF NOT EXISTS `surat_tugas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no` varchar(200) DEFAULT NULL,
  `dari` date DEFAULT NULL,
  `ke` date DEFAULT NULL,
  `daerah` varchar(100) DEFAULT NULL,
  `pegawai` int(11) DEFAULT NULL,
  `penyetuju` int(11) DEFAULT NULL,
  `menimbang` longtext,
  `dasar` longtext,
  `untuk` longtext,
  `notapenye` int(11) DEFAULT NULL,
  `perihal` varchar(200) DEFAULT NULL,
  `notano` varchar(200) DEFAULT NULL,
  `isian` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel msadi_kpu.surat_tugas: ~4 rows (lebih kurang)
DELETE FROM `surat_tugas`;
/*!40000 ALTER TABLE `surat_tugas` DISABLE KEYS */;
INSERT INTO `surat_tugas` (`id`, `no`, `dari`, `ke`, `daerah`, `pegawai`, `penyetuju`, `menimbang`, `dasar`, `untuk`, `notapenye`, `perihal`, `notano`, `isian`) VALUES
	(3, 'demo1/sppd/2018', '2018-08-23', '2018-08-25', 'ACEH/ BANDA ACEH', 1, 1, 'a##b##c##d##e', '1##2##3##4##5##6##7##8##9##10', 'untuk', 7, 'Fasilitas Keamananan', 'notadinas/nota/2018', 'Demo'),
	(4, 'demo2/sppd/2018', '2018-08-30', '2018-09-03', 'SUMATERA UTARA/ MEDAN', NULL, 8, 'aa##aaaa##aaaaa##aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa##aaaaaaaaaaa', 'a222222222222222222222##2222222222222222##33333333333333##111111111111111##45##########', 'Mendampingi dan Memfasilitasi Anggota KPU Melakukan Supervisi dan Monitoring pelaksanaan verifikasi faktual terhadap sampel dukungan perseorangan calon Peserta Pemilu Anggota DPD yang telah memenuhi syarat berdasarkan penelitian administrasi, di Banda Aceh pada tanggal 7 s/d 8 Juni 2018 selama 2 ( dua ) hari.', NULL, NULL, NULL, NULL),
	(5, 'demo3/sppd/2018', '2018-08-31', '2018-09-01', 'KEPULAUAN RIAU/ TANJUNG PINANG', NULL, 8, 'demo1##demo2##demo3##demo4##demo5', '1##2##3##4##5##6##7##8##9##10', 'Mendampingi dan Memfasilitasi Anggota KPU Melakukan Supervisi dan Monitoring pelaksanaan verifikasi faktual terhadap sampel dukungan perseorangan calon Peserta Pemilu Anggota DPD yang telah memenuhi syarat berdasarkan penelitian administrasi, di Banda Aceh pada tanggal 30 s/d 1 Sept 2018 selama 2 ( dua ) hari.', NULL, NULL, NULL, NULL),
	(6, 'sppd4/spd/42018', '2018-08-26', '2018-08-26', 'JAMBI/ JAMBI', NULL, 8, 'Bahwa dalam rangka memastikan pelaksanaan verifikasi faktual terhadap sampel dukungan perseorangan calon Peserta Pemilu Anggota DPD yang telah memenuhi syarat berdasarkan penelitian administrasi yang dilaksanakan oleh KPU/ KIP Kabupaten/Kota dilakukan sesuai dengan ketentuan Peraturan KPU Nomor 14 Tahun 2018 tentang Pencalonan Perseorangan Peserta Pemilihan Umum Anggota Dewan Perwakilan Daerah;##Bahwa dalam rangka Mendampingi dan Memfasilitasi Anggota KPU Melakukan Supervisi dan Monitoring pelaksanaan verifikasi faktual terhadap sampel  dukungan perseorangan calon Peserta Pemilu Anggota DPD yang telah memenuhi syarat berdasarkan penelitian administrasi;##Bahwa untuk melaksanakan kegiatan pada point a dan b dipandang perlu pembentukan Surat Tugas dimaksud.####', 'Undang-Undang Nomor 7 Tahun 2017 tentang Pemilihan Umum ( Lembaran Negara Republik Indonesia Tahun 2011 Nomor 182, Tambahan Lembaran Negara Republik Indonesia Nomor 6109 );##################', 'Mendampingi dan Memfasilitasi Anggota KPU Melakukan Supervisi dan Monitoring pelaksanaan verifikasi faktual terhadap sampel dukungan perseorangan calon Peserta Pemilu Anggota DPD yang telah memenuhi syarat berdasarkan penelitian administrasi, di Banda Aceh pada tanggal 7 s/d 8 Juni 2018 selama 2 ( dua ) hari.', NULL, NULL, NULL, NULL),
	(7, '001/testlagi', '2018-08-27', '2018-08-30', 'ACEH/ BANDA ACEH', NULL, 6, 'Bahwa dalam rangka memastikan pelaksanaan verifikasi faktual terhadap sampel dukungan perseorangan calon Peserta Pemilu Anggota DPD yang telah memenuhi syarat berdasarkan penelitian administrasi yang dilaksanakan oleh KPU/ KIP Kabupaten/Kota dilakukan sesuai dengan ketentuan Peraturan KPU Nomor 14 Tahun 2018 tentang Pencalonan Perseorangan Peserta Pemilihan Umum Anggota Dewan Perwakilan Daerah;##Bahwa dalam rangka Mendampingi dan Memfasilitasi Anggota KPU Melakukan Supervisi dan Monitoring pelaksanaan verifikasi faktual terhadap sampel  dukungan perseorangan calon Peserta Pemilu Anggota DPD yang telah memenuhi syarat berdasarkan penelitian administrasi;##Bahwa untuk melaksanakan kegiatan pada point a dan b dipandang perlu pembentukan Surat Tugas dimaksud.####', 'Undang-Undang Nomor 7 Tahun 2017 tentang Pemilihan Umum ( Lembaran Negara Republik Indonesia Tahun 2011 Nomor 182, Tambahan Lembaran Negara Republik Indonesia Nomor 6109 );##Peraturan Komisi Pemilihan Umum Nomor 05 Tahun 2008 Tentang Tata Kerja Komisi Pemilihan Umum, Komisi Pemilihan Umum Provinsi dan Komisi Pemilihan Umum Kabupaten/ Kota sebagaimana di ubah dengan Peraturan Komisi Pemilihan Umum Nomor 21 Tahun 2008, Peraturan Komisi Pemilihan Umum Nomor 37 Tahun 2008 dan Peraturan Komisi Pemilihan Nomor 01 Tahun 2010;##Peraturan Komisi Pemilihan Umum Nomor 06 Tahun 2008 tentang Susunan Organisasi dan Tata Kerja Sekretariat Jenderal KPU, Sekretariat KPU Provinsi dan Sekretariat KPU Kabupaten/ Kota sebagaimana diubah dengan Peraturan Komisi Pemilihan Umum Nomor 22 Tahun 2008;##Peraturan Menteri Keuangan Nomor. 49/PMK.02/2017 tentang Standar Biaya Masukan Tahun Anggaran 2018;##SK. Sekretaris Jenderal KPU Nomor. 442 / Kpts / Setjen / TAHUN 2015 tentang Pelimpahan Wewenang Penandatangan Surat Tugas.##DIPA KPU BA. 076 Tahun Anggaran 2018.########', 'Mendampingi dan Memfasilitasi Anggota KPU Melakukan Supervisi dan Monitoring pelaksanaan verifikasi faktual terhadap sampel dukungan perseorangan calon Peserta Pemilu Anggota DPD yang telah memenuhi syarat berdasarkan penelitian administrasi, di Banda Aceh pada tanggal 7 s/d 8 Juni 2018 selama 2 ( dua ) hari.', 8, 'Tes kunker', '001/test', 'Bersama ini dengan hormat dilaporkan, dalam rangka Fasilitas Pengamanan Acara Pelantikan Timsel, Provinsi/Kabupaten/Kota dan Pelantikan Komisioner KPU Provinsi Kabupaten/Kota Periode 2018-2023 di Hotel Shantika Jakarta tanggal 05 s/d 07 Juni 2018, maka dipandang perlu menugaskan personil Satuan Pengamanan KPU yang dianggap cakap dan sigap serta bertanggung jawab dalam melaksanakan tugas.');
/*!40000 ALTER TABLE `surat_tugas` ENABLE KEYS */;

-- membuang struktur untuk table msadi_kpu.tbl_user
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `golongan` varchar(100) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `nip` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  `aktif` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel msadi_kpu.tbl_user: ~4 rows (lebih kurang)
DELETE FROM `tbl_user`;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`id`, `nama`, `golongan`, `jabatan`, `nip`, `password`, `role`, `aktif`) VALUES
	(1, 'admin', 'IVeselon', 'admin', NULL, 'd8752be4a0f66f7dd43bfded78469e6c', '1', 1),
	(4, 'demo', 'III', 'demo', 'demo', 'd8752be4a0f66f7dd43bfded78469e6c', NULL, NULL),
	(5, 'ACHMAD SYAIFUDIN RAHADHIAN', 'IVeselon', 'PEJABAT PEMBUAT KOMITMEN \r\nBAGIAN ANGGARAN 076 BIRO UMUM', '197210102000121001', 'd8752be4a0f66f7dd43bfded78469e6c', NULL, NULL),
	(6, 'ARIF GUNAWAN A.Md', 'IVeselon', 'Bendahara', '', 'd8752be4a0f66f7dd43bfded78469e6c', NULL, NULL),
	(7, 'Rr. Endang Pujiastuti Secapawati', 'IVkabag', 'Kepala Bagian', '', 'd8752be4a0f66f7dd43bfded78469e6c', NULL, NULL),
	(8, 'Yayu Yulianti', 'IVeselon', 'Kepala Biro Umum', '', 'd8752be4a0f66f7dd43bfded78469e6c', NULL, NULL);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
